#ifndef MATERIALLOADER_H
#define MATERIALLOADER_H

#include <QTextStream>

#include <source/Objects/Material/material.h>



class MaterialLoader
{
public:
    MaterialLoader();

    Material *parse(QTextStream &inputStream, float newParameter = 1.f);

    static QVector3D parseVector3D(const QString &source, bool &status);
};

#endif // MATERIALLOADER_H
