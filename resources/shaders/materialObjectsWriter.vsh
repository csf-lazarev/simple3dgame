#version 450 core
in vec3 aPos;
in vec3 aNormal;

struct minMaterial{
    vec3 diffuse;
    float shininess;
};


out vec3 FragPos;
out vec4 AlbedoSpec;
out vec3 Normal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform minMaterial material;

void main()
{
    vec4 worldPos = model * vec4(aPos, 1.0);
    FragPos = worldPos.xyz;

    mat3 normalMatrix = transpose(inverse(mat3(model)));
    Normal = normalMatrix * aNormal;

    AlbedoSpec = vec4(material.diffuse, material.shininess);

    gl_Position = projection * view * worldPos;
}
