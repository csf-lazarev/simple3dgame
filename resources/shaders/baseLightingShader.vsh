#version 450 core

struct Light {
    vec3 position;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float constant;
    float linear;
    float quadratic;
};

in vec3 position;
in vec3 normal;

uniform Light iLight;
out Light light;

out vec3 outNormal;
out vec3 outFragPos;

uniform mat4 projection;
uniform mat4 model;
uniform mat4 view;

uniform vec3 inLightPos;


void main()
{
  mat4 view_model = view * model;
  gl_Position = projection * view_model * vec4(position, 1.0f);
  outFragPos = vec3(view_model * vec4(position, 1.0f));
  outNormal = mat3(transpose(inverse(view_model))) * normal;
  light = iLight;
  light.position = vec3(view * vec4(iLight.position, 1.0f));
}
