#version 450 core

in vec3 pos;
in vec2 texCoords;

out vec2 TexCoords;

void main(){
    gl_Position = vec4(pos, 1.0);
    TexCoords = texCoords;
}
