#version 330 core
layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec4 gAlbedoSpec;
layout (location = 2) out vec3 gNormal;


in vec3 FragPos;
in vec3 Normal;
in vec4 AlbedoSpec;

void main()
{
    // store the fragment position vector in the first gbuffer texture
    gPosition = FragPos;
    // also store the per-fragment normals into the gbuffer
    gNormal = Normal;
    // and the diffuse per-fragment color
    gAlbedoSpec = AlbedoSpec;
    // store specular intensity in gAlbedoSpec's alpha component
}
