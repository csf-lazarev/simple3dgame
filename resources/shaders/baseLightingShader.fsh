#version 450 core
struct Light {
    vec3 position;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float constant;
    float linear;
    float quadratic;
};

struct Material{
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
};

out vec4 color;
in Light light;

in vec3 outNormal;
in vec3 outFragPos;

uniform Material material;

void main()
{
    // Ambient
    vec3 ambient = material.ambient * light.ambient;

    // Diffuse
    vec3 normal = normalize(outNormal);
    vec3 lightDir = normalize(light.position - outFragPos);
    float diffuseFactor = max(dot(normal, lightDir), 0.0);
    vec3 diffuse = diffuseFactor * material.diffuse * light.diffuse;

    // Specular
    vec3 viewDir = normalize(-outFragPos);
    vec3 reflectDir = reflect(-lightDir, normal);
    float specularFactor = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    vec3 specular = (material.specular * specularFactor) * light.specular;

    float distance    = length(light.position - outFragPos);
    float attenuation = 1.0 / (light.constant + light.linear * distance +
                        light.quadratic * (distance * distance));

    vec3 resultColor = (ambient + diffuse + specular) * attenuation;
    color = vec4(resultColor, 1.0f);
}
