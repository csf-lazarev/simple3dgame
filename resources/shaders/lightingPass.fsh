#version 450 core
struct Light {
    vec3 position;
    vec3 color;

    float constant;
    float linear;
    float quadratic;
};

in vec2 TexCoords;
out vec4 FragColor;

layout(location = 0) uniform sampler2D gPosition;
layout(location = 1) uniform sampler2D gAlbedoSpec;
layout(location = 2) uniform sampler2D gNormal;


uniform vec3 viewPos;

const int NR_LIGHTS = 32;
uniform Light lights[NR_LIGHTS];
uniform int realLightCount;


void main()
{

    vec4 gPos_sample = texture2D(gPosition, TexCoords);
    vec4 gColor_sample = texture2D(gAlbedoSpec, TexCoords);
    vec4 gNormal_sample = texture2D(gNormal, TexCoords);

    vec3 FragPos = gPos_sample.rgb;

    vec3 Normal = gNormal_sample.rgb;

    vec3 Albedo = gColor_sample.rgb;

    float Specular = gColor_sample.a;
    vec3 viewDir = normalize(viewPos - FragPos);
    float ambientFactor = 0.01;

    int activeLights = min(NR_LIGHTS, realLightCount);

    vec3 normal = normalize(Normal);
    vec3 lighting = ambientFactor * Albedo * length(normal);

    for(int i = 0; i < activeLights; ++i){
        // Diffuse
        vec3 lightDir = normalize(lights[i].position - FragPos);
        float diffuseFactor = max(dot(normal, lightDir), 0.0);
        vec3 diffuse = diffuseFactor * Albedo * lights[i].color;

        // Specular
        vec3 halfWayDir = normalize(lightDir + viewDir);
        float specularFactor = pow(max(dot(normal, halfWayDir), 0.0), 256.0);
        vec3 specular = specularFactor * lights[i].color * Specular;

        float distance = length(lights[i].position - FragPos);

        // Attenuation
        float attenuation = 1.0 / (lights[i].constant + lights[i].linear * distance +
                            lights[i].quadratic * (distance * distance));

        vec3 resultColor = (diffuse + specular) * attenuation;
        lighting += resultColor;
    }

    FragColor = vec4(lighting, 1.0f);
}
