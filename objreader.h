#ifndef OBJREADER_H
#define OBJREADER_H

#include <QString>
#include <QVector4D>
#include <QVector3D>
#include <QVector2D>
#include <QFile>

#include <QTextStream>

#include <source/Buffer/geometrybuffer.h>


class ObjReader
{
public:
    ObjReader();
    GeometryBuffer* parse(const QString &source) const;
    GeometryBuffer* parseFromFile(const QString &firePath) const;
    GeometryBuffer* parse(QFile &file) const;
    GeometryBuffer* parse(QTextStream &input_stream) const;


    static QVector4D parseVector4D(const QString &source, bool &status);

    static QVector3D parseVector3D(const QString &source, bool &status);

    static QVector2D parseVector2D(const QString &source, bool &status);

    static bool parseFaceCondition(const QString &source, QVector<int> &vertexIndices, QVector<int> &textureIndices, QVector<int> &normalIndices);

};

#endif // OBJREADER_H
