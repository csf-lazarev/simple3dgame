#ifndef KEYEVENTREGISTER_H
#define KEYEVENTREGISTER_H

#include <QVector>
#include <QKeyEvent>

class KeyEventRegister
{
public:
    KeyEventRegister();
    void registerKeyEvent(QKeyEvent* event);
    bool isPresed(int key);
    QList<QKeyEvent*> getEvents();

private:
    bool keyMap[1024];
    QMap<int, QKeyEvent*> eventmap;
};

#endif // KEYEVENTREGISTER_H
