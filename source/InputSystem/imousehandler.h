#ifndef IMOUSEHANDLER_H
#define IMOUSEHANDLER_H

#include <QMouseEvent>



class IMouseHandler
{
public:
    IMouseHandler();
    virtual ~IMouseHandler();

    virtual void mouseMoved(float xOffset, float yOffset) = 0;
    virtual void wheelEvent(QWheelEvent *event) = 0;
};

#endif // IMOUSEHANDLER_H
