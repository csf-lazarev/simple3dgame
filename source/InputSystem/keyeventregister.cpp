#include "keyeventregister.h"
#include <QDebug>

KeyEventRegister::KeyEventRegister()
{

}

void KeyEventRegister::registerKeyEvent(QKeyEvent *event)
{
    QKeyEvent::Type type = event->type();
    if (event->key() > 1023)
        return;

    if (type == QKeyEvent::Type::KeyPress && !keyMap[event->key()]){
        eventmap.insert(event->key(), new QKeyEvent(*event));
        keyMap[event->key()] = true;
    }
    else if (type == QKeyEvent::Type::KeyRelease && keyMap[event->key()]){
        eventmap.remove(event->key());
        keyMap[event->key()] = false;
    }
}

bool KeyEventRegister::isPresed(int key)
{
    if (key > 1023)
        return false;
    return keyMap[key];
}

QList<QKeyEvent *> KeyEventRegister::getEvents()
{
    return eventmap.values();
}


