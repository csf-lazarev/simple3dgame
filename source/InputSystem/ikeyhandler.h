#ifndef IKEYHANDLER_H
#define IKEYHANDLER_H

#include <QKeyEvent>




class IKeyHandler
{
public:
    IKeyHandler();
    virtual ~IKeyHandler();

    virtual void onKeyPress(QKeyEvent *event) = 0;
};

#endif // IKEYHANDLER_H
