#include "geometrybuffer.h"
#include <QMap>

#include <source/Objects/Intersection/aabbcreator.h>

GeometryBuffer::GeometryBuffer(VertexBuffer *linkBuffer) : vertexIndices(), textureIndices(), normalIndices(),
    indicesStarts{0},linkedBuffer(linkBuffer)
{

}

GeometryBuffer::~GeometryBuffer()
{
}

void GeometryBuffer::addFace(const QVector<int> *vertexIndices, const QVector<int> *textureIndices,
                          const QVector<int> *normalIndices)
{
    this->vertexIndices.append(*vertexIndices);
    if(textureIndices && textureIndices->size())
        this->textureIndices.append(*textureIndices);
    if(normalIndices && normalIndices->size())
        this->normalIndices.append(*normalIndices);

    indicesStarts.append(indicesStarts.last() + vertexIndices->size());
}

void GeometryBuffer::setLinkedBuffer(VertexBuffer *linkedBuffer)
{
    this->linkedBuffer = linkedBuffer;
}

void GeometryBuffer::setVertexIndices(QVector<int> &vertexIndices)
{
    this->vertexIndices = vertexIndices;
}

void GeometryBuffer::setTextureIndices(QVector<int> &textureIndices)
{
    this->textureIndices = textureIndices;
}

void GeometryBuffer::setNormalIndices(QVector<int> &normalIndices)
{
    this->normalIndices = normalIndices;
}

void GeometryBuffer::setIndicesStarts(QVector<int> &indicesStarts)
{
    this->indicesStarts = indicesStarts;
}

GeometryBuffer *GeometryBuffer::getTriangulatedGeometryBuffer()
{
    GeometryBuffer* newBuffer = new GeometryBuffer(linkedBuffer);
    QVector<int> &starts = indicesStarts;
    trinangulateBuffer(starts, vertexIndices, newBuffer->vertexIndices);

//    if(this->hasTextureIndices())
//        trinangulateBuffer(starts, textureIndices, newBuffer->textureIndices);
    if(this->hasNormalIndices())
        trinangulateBuffer(starts, normalIndices, newBuffer->normalIndices);

    makeTrinagulateStarts(newBuffer->vertexIndices.size() / 3, newBuffer->indicesStarts);
    return newBuffer;
}

VertexBuffer *GeometryBuffer::getLinkedBuffer() const
{
    return linkedBuffer;
}

const QVector<int> &GeometryBuffer::getIndicesStarts() const
{
    return indicesStarts;
}

const QVector<int> &GeometryBuffer::getVertexIndices() const
{
    return vertexIndices;
}

const QVector<int> &GeometryBuffer::getNormalIndices() const
{
    return normalIndices;
}

void GeometryBuffer::getVertexListByVertexIndices(QVector<QVector3D> &list)
{
     QVector<QVector3D> uniqueList = linkedBuffer->getVertexList();
    list.resize(vertexIndices.size());

    for (int vertexIndex = 0; vertexIndex < vertexIndices.size(); ++vertexIndex)
        list[vertexIndex] = uniqueList[vertexIndices[vertexIndex]];
}

void GeometryBuffer::getNormalListByNormalIndices(QVector<QVector3D> &list)
{
    const QVector<QVector3D> uniqueList = linkedBuffer->getNormalsList();
    list.resize(vertexIndices.size());

    for (int normalIndex = 0; normalIndex < vertexIndices.size(); ++normalIndex)
        list[normalIndex] = uniqueList[normalIndices[normalIndex]];
}

TriangulatedGeometryBuffer *GeometryBuffer::toTriangulatedGeometryBuffer()
{
    QVector<int> tVerticesIndices, tNormalsIndices;
    GeometryBuffer::trinangulateBuffer(indicesStarts, vertexIndices, tVerticesIndices);
    GeometryBuffer::trinangulateBuffer(indicesStarts, normalIndices, tNormalsIndices);


    Q_ASSERT(tVerticesIndices.size() == tNormalsIndices.size() && "invalid buffer data");
    int size = tVerticesIndices.size();
    Q_ASSERT(size % 3 == 0);
    QVector<QVector3D> vertices = linkedBuffer->getVertexList();
    QVector<QVector3D> normals = linkedBuffer->getNormalsList();

    TriangulatedGeometryBuffer *tBuffer = new TriangulatedGeometryBuffer();


    for (int tIndex = 0; tIndex < size - 2; tIndex += 3){


        tBuffer->addTriangle(new GTriangle(vertices[tVerticesIndices[tIndex]],
                                            vertices[tVerticesIndices[tIndex + 1]],
                                            vertices[tVerticesIndices[tIndex + 2]],
                                            normals[tNormalsIndices[tIndex]],
                                            normals[tNormalsIndices[tIndex + 1]],
                                            normals[tNormalsIndices[tIndex + 2]],
                                            tIndex));


    }

    return tBuffer;
}

void GeometryBuffer::trinangulateBuffer(const QVector<int> &indicesStarts, const QVector<int> &indicesBuffer,
                                       QVector<int> &triangulatedBuffer)
{

    Q_ASSERT(!indicesStarts.empty());
    Q_ASSERT(indicesBuffer.size() == indicesStarts.last());

    triangulatedBuffer.resize(getCountTriangles(indicesStarts) * 3);

    int currentTriangulatedBufferIndex = 0;
    for (int polygonIndex = 1; polygonIndex < indicesStarts.size(); ++polygonIndex){

        int indexEnd = indicesStarts[polygonIndex];

        int indexStart = indicesStarts[polygonIndex - 1];

        Q_ASSERT(indexStart < indexEnd);

        for (int polygonVertexIndex = indexStart; polygonVertexIndex < indexEnd - 2; ++polygonVertexIndex){

            triangulatedBuffer[currentTriangulatedBufferIndex++] = indicesBuffer[indexStart];
            triangulatedBuffer[currentTriangulatedBufferIndex++] = indicesBuffer[polygonVertexIndex + 1];
            triangulatedBuffer[currentTriangulatedBufferIndex++] = indicesBuffer[polygonVertexIndex + 2];
        }

    }
}

int GeometryBuffer::getCountTriangles(const QVector<int> &indicesStarts)
{
    int countTrinagles = 0;
    for (int startIndex = 1; startIndex < indicesStarts.size(); ++startIndex)
        countTrinagles += indicesStarts[startIndex] - indicesStarts[startIndex - 1] - 2;
    return countTrinagles;
}

void GeometryBuffer::makeTrinagulateStarts(int countTriangles, QVector<int> &oldIndicesStarts)
{
    oldIndicesStarts.resize(countTriangles + 1);

    for (int numberTrinagle = 0; numberTrinagle <= countTriangles; ++numberTrinagle)
        oldIndicesStarts[numberTrinagle] = numberTrinagle * 3;
}

QVector<IndexedTriangle *> *GeometryBuffer::getTriangleData(AABB *res) const
{    
    QVector<IndexedTriangle *> *triangles = new QVector<IndexedTriangle *>();
    triangles->resize(indicesStarts.size() - 1);
    QVector<QVector3D> vertices = linkedBuffer->getVertexList();
    QVector<QVector3D> normals = linkedBuffer->getNormalsList();

    if (res){
        AABBCreator c;
        for (int indexStart = 0; indexStart < indicesStarts.size() - 1; ++indexStart){
            int vertexStartIndex = indicesStarts[indexStart];
            vector3 v1 = vertices[vertexIndices[vertexStartIndex]];
            vector3 v2 = vertices[vertexIndices[vertexStartIndex + 1]];
            vector3 v3 = vertices[vertexIndices[vertexStartIndex + 2]];

            triangles->operator[](indexStart) = new IndexedTriangle(v1,v2,v3, indexStart);
            c.addPoint(v1);
            c.addPoint(v2);
            c.addPoint(v3);
        }
        *res = *c.getAABB();
    }else
    {
        for (int indexStart = 0; indexStart < indicesStarts.size() - 1; ++indexStart){
            int vertexStartIndex = indicesStarts[indexStart];
            vector3 v1 = vertices[vertexIndices[vertexStartIndex]];
            vector3 v2 = vertices[vertexIndices[vertexStartIndex + 1]];
            vector3 v3 = vertices[vertexIndices[vertexStartIndex + 2]];
            triangles->operator[](indexStart) = new IndexedTriangle(v1,v2,v3, indexStart);
        }
    }
    return triangles;
}

bool GeometryBuffer::hasTextureIndices()
{
    return textureIndices.size();
}

bool GeometryBuffer::hasNormalIndices()
{
    return normalIndices.size();
}

void GeometryBuffer::recalculateNormals()
{
    QVector<QVector3D> vertices = linkedBuffer->getVertexList();
    normalIndices.clear();
    normalIndices.resize(vertexIndices.size());
    linkedBuffer->resetNormals();

    QHash<QVector3D, int> normalsMap;
    int nextNormalIndex = 0;

    for (int polygonIndex = 1; polygonIndex < indicesStarts.size(); ++polygonIndex){

        int indexEnd = indicesStarts[polygonIndex];

        int indexStart = indicesStarts[polygonIndex - 1];

        for (int polygonVertexIndex = indexStart; polygonVertexIndex < indexEnd - 2; ++polygonVertexIndex){

            int index0 = polygonVertexIndex;
            int index1 = polygonVertexIndex + 1;
            int index2 = polygonVertexIndex + 2;

            QVector3D v0 = vertices[vertexIndices[index0]];
            QVector3D v1 = vertices[vertexIndices[index1]];
            QVector3D v2 = vertices[vertexIndices[index2]];

            QVector3D normal = QVector3D::crossProduct(v1 - v0, v2 - v1).normalized();

            int normalIndex;

            if (!normalsMap.contains(normal)){
                normalIndex = nextNormalIndex++;
                normalsMap.insert(normal, normalIndex);
            }
            else{
                normalIndex = normalsMap.value(normal);
            }

            normalIndices[index0] = normalIndex;
            normalIndices[index1] = normalIndex;
            normalIndices[index2] = normalIndex;

        }

    }

    QVector<QVector3D> &normals = linkedBuffer->getNormals();
    normals.resize(normalsMap.keys().size());

    for (auto kv = normalsMap.keyValueBegin(); kv != normalsMap.keyValueEnd(); kv++){
        auto it = kv.base();
        normals[it.value()] = it.key();
    }

}

