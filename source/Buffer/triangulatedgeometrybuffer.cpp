#include "triangulatedgeometrybuffer.h"

TriangulatedGeometryBuffer::TriangulatedGeometryBuffer(bool enableUV) : uvAttached(enableUV)
{

}

void TriangulatedGeometryBuffer::addTriangle(GTriangle *triangle)
{
    triangles.append(triangle);
}

void TriangulatedGeometryBuffer::addUVCoordinate(TriangleUV *tUV)
{
    triangleUVs.append(tUV);
}

bool TriangulatedGeometryBuffer::isValid() const
{
    return !uvAttached || triangles.size() == triangleUVs.size();
}

bool TriangulatedGeometryBuffer::isUVAttached() const
{
    return uvAttached;
}

void TriangulatedGeometryBuffer::enableUV(bool enabled)
{
    uvAttached = enabled;
}

const GTriangle *TriangulatedGeometryBuffer::getTriangle(int index) const
{
    return triangles[index];
}

GTriangle *TriangulatedGeometryBuffer::getTriangle(int index)
{
    return triangles[index];
}

const TriangleUV *TriangulatedGeometryBuffer::getTriangleUV(int index) const
{
    return triangleUVs[index];
}

TriangleUV *TriangulatedGeometryBuffer::getTriangleUV(int index)
{
    return triangleUVs[index];
}

const QVector<GTriangle *> &TriangulatedGeometryBuffer::getTrianglesData() const
{
    return triangles;
}

const QVector<TriangleUV *> &TriangulatedGeometryBuffer::getUVData() const
{
    return triangleUVs;
}

int TriangulatedGeometryBuffer::countTriangles() const
{
    return triangles.size();
}

TriangleUV::TriangleUV(const QVector2D &aUV, const QVector2D &bUV, const QVector2D &cUV) :
    aUV(aUV), bUV(bUV), cUV(cUV)
{

}

QVector2D TriangleUV::getAUV() const
{
    return aUV;
}

void TriangleUV::setAUV(const QVector2D &value)
{
    aUV = value;
}

QVector2D TriangleUV::getBUV() const
{
    return bUV;
}

void TriangleUV::setBUV(const QVector2D &value)
{
    bUV = value;
}

QVector2D TriangleUV::getCUV() const
{
    return cUV;
}

void TriangleUV::setCUV(const QVector2D &value)
{
    cUV = value;
}
