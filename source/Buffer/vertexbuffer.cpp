#include "vertexbuffer.h"

#include <source/Objects/Intersection/aabbcreator.h>

VertexBuffer::VertexBuffer() : vertexList(), textureList(), normalsList()
{

}

void VertexBuffer::addVertex(const QVector3D &vertex)
{
    vertexList.append(vertex);
}

void VertexBuffer::addTextureVertex(const QVector2D &textureVertex)
{
    textureList.append(textureVertex);
}

void VertexBuffer::addNormal(const QVector3D &normal)
{
    normalsList.append(normal);
}


const QVector<QVector3D> &VertexBuffer::getVertexList() const
{
    return vertexList;
}

const QVector<QVector2D> &VertexBuffer::getTextureList() const
{
    return textureList;
}

const QVector<QVector3D> &VertexBuffer::getNormalsList() const
{
    return normalsList;
}

QVector<QVector3D> &VertexBuffer::getNormals()
{
    return normalsList;
}

AABB *VertexBuffer::getAABB() const
{
    Point3D p = vertexList[0];
    float minX = p.x();
    float minY = p.y();
    float minZ = p.z();

    float maxX = p.x();
    float maxY = p.y();
    float maxZ = p.z();

    for (int vertexIndex = 1; vertexIndex < vertexList.size(); ++vertexIndex){

        Point3D vertex = vertexList[vertexIndex];

        if (vertex.x() < minX)
            minX = vertex[0];
        else if (vertex.x() > maxX)
            maxX = vertex[0];

        if (vertex.y() < minY)
            minY = vertex[1];
        else if (vertex.y() > maxY)
            maxY = vertex[1];

        if (vertex.z() < minZ)
            minZ = vertex[2];
        else if (vertex.z() > maxZ)
            maxZ = vertex[2];

    }

    return new AABB(Point3D(minX, minY, minZ), Point3D(maxX, maxY, maxZ));
}

void VertexBuffer::resetNormals()
{
    normalsList.clear();
}


