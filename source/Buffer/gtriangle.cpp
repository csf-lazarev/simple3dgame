#include "gtriangle.h"


GTriangle::GTriangle(const QVector3D &a, const QVector3D &b, const QVector3D &c, const QVector3D &na, const QVector3D &nb, const QVector3D &nc, unsigned index) :
    Triangle(a,b,c), na(na), nb(nb), nc(nc), index(index)
{

}


QVector3D GTriangle::getNormalA() const
{
    return na;
}

void GTriangle::setNormalA(const QVector3D &value)
{
    na = value;
}

QVector3D GTriangle::getNormalB() const
{
    return nb;
}

void GTriangle::setNormalB(const QVector3D &value)
{
    nb = value;
}

QVector3D GTriangle::getNormalC() const
{
    return nc;
}

void GTriangle::setNormalC(const QVector3D &value)
{
    nc = value;
}

unsigned GTriangle::getIndex() const
{
    return index;
}
