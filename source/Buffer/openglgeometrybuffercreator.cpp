#include "openglgeometrybuffercreator.h"
#include <QVector>
#include <QVector3D>

OpenGLGeometryBufferCreator::OpenGLGeometryBufferCreator()
{

}

OpenGLGeometryBuffer *OpenGLGeometryBufferCreator::createFor(const TriangulatedGeometryBuffer &buffer)
{

    QVector<QVector3D> vertices, normals;

    int countTriangles = buffer.countTriangles();

    vertices.resize(countTriangles * 3);
    normals.resize(countTriangles * 3);

    for (int triangleIndex = 0; triangleIndex < countTriangles; ++triangleIndex){

        const GTriangle *triangle = buffer.getTriangle(triangleIndex);
        int indexPointer = triangleIndex * 3;

        vertices[indexPointer] = triangle->getA();
        vertices[indexPointer + 1] = triangle->getB();
        vertices[indexPointer + 2] = triangle->getC();

        normals[indexPointer] = triangle->getNormalA();
        normals[indexPointer + 1] = triangle->getNormalB();
        normals[indexPointer + 2] = triangle->getNormalC();

    }
    QOpenGLBuffer *vertexBuffer = new QOpenGLBuffer();
    QOpenGLBuffer *normalBuffer = new QOpenGLBuffer();

    int dataSize = countTriangles * 3 * 3 * sizeof (float);

    vertexBuffer->create();
    vertexBuffer->bind();
    vertexBuffer->allocate(vertices.data(), dataSize);
    vertexBuffer->release();

    normalBuffer->create();
    normalBuffer->bind();
    normalBuffer->allocate(normals.data(), dataSize);

    OpenGLGeometryBuffer *glBuffer = new OpenGLGeometryBuffer(vertexBuffer,  normalBuffer);

    if(!buffer.isUVAttached())
        return glBuffer;

    QVector<QVector2D> uvs;
    uvs.resize(countTriangles * 3);

    for (int triangleIndex = 0; triangleIndex < countTriangles; ++triangleIndex){

        const TriangleUV *tUV = buffer.getTriangleUV(triangleIndex);
        int indexPointer = triangleIndex * 3;

        uvs[indexPointer] = tUV->getAUV();
        uvs[indexPointer + 1] = tUV->getBUV();
        uvs[indexPointer + 2] = tUV->getCUV();
    }

    QOpenGLBuffer *uvBuffer = new QOpenGLBuffer();

    uvBuffer->create();
    uvBuffer->bind();
    uvBuffer->allocate(uvs.data(), countTriangles * 3 * 2 * sizeof (float));
    uvBuffer->release();

    glBuffer->attachUVBuffer(uvBuffer);

    return glBuffer;
}
