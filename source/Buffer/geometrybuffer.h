#ifndef GeometryBuffer_H
#define GeometryBuffer_H

#include "triangulatedgeometrybuffer.h"
#include "vertexbuffer.h"

#include <QVector>

#include <source/Objects/Intersection/Primitives/indexedtriangle.h>



class GeometryBuffer
{
public:
    explicit GeometryBuffer(VertexBuffer *linkBuffer = nullptr);
    ~GeometryBuffer();

    void addFace(const QVector<int> *vertexIndices, const QVector<int> *textureIndices = nullptr,
                 const QVector<int> *normalIndices = nullptr);


    void setLinkedBuffer(VertexBuffer *linkedBuffer);
    void setVertexIndices(QVector<int> &vertexIndices);
    void setTextureIndices(QVector<int> &textureIndices);
    void setNormalIndices(QVector<int> &normalIndices);
    void setIndicesStarts(QVector<int> &indicesStarts);

    GeometryBuffer* getTriangulatedGeometryBuffer();

    VertexBuffer* getLinkedBuffer() const;

    const QVector<int> &getIndicesStarts() const;
    const QVector<int> &getVertexIndices() const;
    const QVector<int> &getNormalIndices() const;

    void getVertexListByVertexIndices(QVector<QVector3D> &list);
    void getNormalListByNormalIndices(QVector<QVector3D> &list);

    TriangulatedGeometryBuffer *toTriangulatedGeometryBuffer();

    static void trinangulateBuffer(const QVector<int> &indicesStarts, const QVector<int> &sourceBuffer,
                                     QVector<int> &resultBuffer);
    static int getCountTriangles(const QVector<int> &indicesStarts);

    static void makeTrinagulateStarts(int countTriangles, QVector<int> &oldIndicesStarts);

    QVector<IndexedTriangle *> *getTriangleData(AABB * res) const;

    bool hasTextureIndices();
    bool hasNormalIndices();

    void recalculateNormals();

private:
    QVector<int> vertexIndices;
    QVector<int> textureIndices;
    QVector<int> normalIndices;

    QVector<int> indicesStarts;

    VertexBuffer *linkedBuffer;
};

inline uint qHash(const QVector3D &key, uint seed){
    return qHash(key.x(), seed) * qHash(key.y(), seed) +
                                       qHash(key.y(), seed) * qHash(key.z(), seed);
}

#endif // GeometryBuffer_H
