#ifndef GTRIANGLE_H
#define GTRIANGLE_H
#include <QVector3D>
#include <source/Objects/Intersection/Primitives/triangle.h>

class GTriangle : public Triangle
{
public:
    GTriangle(const QVector3D &a,  const QVector3D &b,  const QVector3D &c,
              const QVector3D &na, const QVector3D &nb, const QVector3D &nc,
              unsigned index);

    QVector3D getNormalA() const;
    void setNormalA(const QVector3D &value);

    QVector3D getNormalB() const;
    void setNormalB(const QVector3D &value);

    QVector3D getNormalC() const;
    void setNormalC(const QVector3D &value);

    unsigned getIndex() const;

private:
    //Normals
    QVector3D na, nb, nc;

    unsigned index;
};

#endif // GTRIANGLE_H
