#ifndef TRINAGULATEDGEOMETRYBUFFER_H
#define TRINAGULATEDGEOMETRYBUFFER_H
#include <QVector>
#include <QVector2D>
#include "gtriangle.h"


struct TriangleUV{

public:
    TriangleUV(const QVector2D &aUV, const QVector2D &bUV, const QVector2D &cUV);

    QVector2D getAUV() const;
    void setAUV(const QVector2D &value);

    QVector2D getBUV() const;
    void setBUV(const QVector2D &value);

    QVector2D getCUV() const;
    void setCUV(const QVector2D &value);

private:
    QVector2D aUV, bUV, cUV;
};

class TriangulatedGeometryBuffer
{
public:
    explicit TriangulatedGeometryBuffer(bool enableUV = false);
    void addTriangle(GTriangle * triangle);
    void addUVCoordinate(TriangleUV *tUV);
    bool isValid() const;
    bool isUVAttached() const;
    void enableUV(bool enabled = true);

    const GTriangle * getTriangle(int index) const;
    GTriangle *getTriangle(int index);

    const TriangleUV *getTriangleUV(int index) const;
    TriangleUV *getTriangleUV(int index);

    const QVector<GTriangle *> &getTrianglesData() const;
    const QVector<TriangleUV *> &getUVData() const;

    int countTriangles() const;
    QVector<GTriangle *> const &getCopyTriangles();

private:
    QVector<GTriangle *> triangles;
    QVector<TriangleUV *> triangleUVs;
    bool uvAttached;
};

#endif // TRINAGULATEDGEOMETRYBUFFER_H
