#ifndef OPENGLGEOMETRYBUFFERCREATOR_H
#define OPENGLGEOMETRYBUFFERCREATOR_H
//#include <source/Buffer/geometrybuffer.h>
#include <source/Buffer/triangulatedgeometrybuffer.h>
#include <source/OpenGL/openglgeometrybuffer.h>

class OpenGLGeometryBufferCreator
{
public:
    OpenGLGeometryBufferCreator();

    static OpenGLGeometryBuffer *createFor(const TriangulatedGeometryBuffer &buffer);
};

#endif // OPENGLGEOMETRYBUFFERCREATOR_H
