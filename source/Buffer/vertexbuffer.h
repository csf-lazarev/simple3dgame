#ifndef VERTEXBUFFER_H
#define VERTEXBUFFER_H

#include <QVector>
#include <QVector2D>
#include <QVector3D>

#include <source/Objects/Intersection/aabb.h>
#include <source/Objects/Intersection/Primitives/triangle.h>



class VertexBuffer
{
public:
    VertexBuffer();
    void addVertex(const QVector3D &vertex);
    void addTextureVertex(const QVector2D &textureVertex);
    void addNormal(const QVector3D &normal);


    const QVector<QVector3D>& getVertexList() const;
    const QVector<QVector2D>& getTextureList() const;
    const QVector<QVector3D>& getNormalsList() const;
    QVector<QVector3D> &getNormals();

    AABB *getAABB() const;

    void resetNormals();

private:
    QVector<QVector3D> vertexList;
    QVector<QVector2D> textureList;
    QVector<QVector3D> normalsList;
};

#endif // VERTEXBUFFER_H
