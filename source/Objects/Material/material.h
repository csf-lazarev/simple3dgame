#ifndef MATERIAL_H
#define MATERIAL_H

#include <types.h>

class Material
{
public:
    Material(vector3 diffuse, float shininess);

    vector3 getDiffuse() const;
    void setDiffuse(const vector3 &value);

    float getShininess() const;
    void setShininess(float value);

private:
    vector3 diffuse;
    float shininess;
};

#endif // MATERIAL_H
