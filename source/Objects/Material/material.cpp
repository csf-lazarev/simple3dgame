#include "material.h"

Material::Material(vector3 diffuse, float shininess) :
    diffuse(diffuse), shininess(shininess)
{

}

vector3 Material::getDiffuse() const
{
    return diffuse;
}

void Material::setDiffuse(const vector3 &value)
{
    diffuse = value;
}

float Material::getShininess() const
{
    return shininess;
}

void Material::setShininess(float value)
{
    shininess = value;
}
