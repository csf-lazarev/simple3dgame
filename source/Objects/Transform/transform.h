#ifndef TRANSFORM_H
#define TRANSFORM_H

#include <types.h>
class Transform
{
public:
    Transform();
    virtual ~Transform();
    virtual const matrix4 &getTransformMatrix() const = 0;
};

#endif // TRANSFORM_H
