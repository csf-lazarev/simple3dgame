#ifndef TRANSFORM3D_H
#define TRANSFORM3D_H
#include "transform.h"
#include <qmath.h>
#include <QQuaternion>
#include <types.h>

class Transform3D : public Transform
{
public:
    Transform3D(const vector3 &translate, const vector3 &scale, const QVector3D& rotationAxis, float angle);
    ~Transform3D() override;
    const matrix4 & getTransformMatrix() const  override;

    static Transform3D createIdentity();

private:
    void updateTransformMatrix() const;
private:
    vector3 translate;
    vector3 scale;
    QQuaternion rotationQuaternion;
    mutable matrix4 transformMatrix;
    mutable bool needUpdateMatrix = false;
};

#endif // TRANSFORM3D_H
