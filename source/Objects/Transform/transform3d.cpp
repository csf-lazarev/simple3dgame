#include "transform3d.h"

Transform3D::Transform3D(const vector3 &translate, const vector3 &scale, const QVector3D &rotationAxis, float angle) :
    translate(translate), scale(scale),
    rotationQuaternion(qCos(qDegreesToRadians(angle)), rotationAxis)
{
    updateTransformMatrix();
}

Transform3D::~Transform3D()
{

}

const matrix4 &Transform3D::getTransformMatrix() const
{
    if (needUpdateMatrix){
        needUpdateMatrix = false;
    }
    return transformMatrix;
}

Transform3D Transform3D::createIdentity()
{
    return Transform3D(QVector3D(), QVector3D(1,1,1), QVector3D(0,0,0), 0);
}

void Transform3D::updateTransformMatrix() const
{
    transformMatrix.setToIdentity();
    transformMatrix.translate(translate);
    transformMatrix.scale(scale);
    transformMatrix.rotate(rotationQuaternion.normalized());
}
