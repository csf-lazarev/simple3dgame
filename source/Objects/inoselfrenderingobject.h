#ifndef INOSELFRENDERINGOBJECT_H
#define INOSELFRENDERINGOBJECT_H

#include "baserenderingobject.h"



class INoSelfRenderingObject : public BaseRenderingObject
{
public:
    INoSelfRenderingObject();
    virtual ~INoSelfRenderingObject();
};

#endif // INOSELFRENDERINGOBJECT_H
