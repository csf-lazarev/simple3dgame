#ifndef SCENEOBJECT_H
#define SCENEOBJECT_H

#include "baserenderingobject.h"
#include "inoselfrenderingobject.h"
#include "transformableobject.h"

#include <types.h>
#include <source/typeid.h>
#include <source/Objects/Geometry/gridgeometry.h>
#include <qmath.h>
#include <source/ResourceSystem/geomertybuffercontainer.h>
#include <source/Buffer/triangulatedgeometrybuffer.h>
#include <source/Objects/Transform/transform3d.h>
#include <source/Objects/Material/material.h>

using TGContainer = GeomertyBufferContainer<TriangulatedGeometryBuffer>;
class SceneObject : public INoSelfRenderingObject, public TransformableObject
{
public:
    SceneObject(TGContainer * bufferContainer, Transform3D *transform);

    Transform *getTransform() const override;

    GeomertyBufferContainer<TriangulatedGeometryBuffer> *getGeometryBufferContainer() const;

private:
    GeomertyBufferContainer<TriangulatedGeometryBuffer> *geometryBufferContainer;
    Transform3D *transform;
};

#endif // SCENEOBJECT_H
