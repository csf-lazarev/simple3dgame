#ifndef BASESCENEOBJECT_H
#define BASESCENEOBJECT_H
#include <source/typeid.h>

class BaseRenderingObject
{
public:
    BaseRenderingObject();
    virtual ~BaseRenderingObject();
    TypeID getObjectType();
    size_t getObjectTypeHash();
};

#endif // BASESCENEOBJECT_H
