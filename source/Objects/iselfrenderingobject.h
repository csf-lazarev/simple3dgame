#ifndef ISELFRENDERINGOBJECT_H
#define ISELFRENDERINGOBJECT_H

#include "baserenderingobject.h"

#include <source/Render/RenderHelpers/irenderhelper.h>



class ISelfRenderingObject : public BaseRenderingObject
{
public:
    ISelfRenderingObject();
    virtual ~ISelfRenderingObject();
    virtual void render() = 0;
};

#endif // ISELFRENDERINGOBJECT_H
