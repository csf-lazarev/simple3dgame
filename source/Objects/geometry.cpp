#include "geometry.h"

Geometry::Geometry() : geometryBuffer(nullptr), geometryType(Mesh)
{

}

Geometry::Geometry(GeometryBuffer *GeometryBuffer, Type geometryType) : geometryBuffer(GeometryBuffer),
    geometryType(geometryType)
{

}

Geometry::~Geometry()
{

}

void Geometry::draw()
{
    Q_ASSERT(geometryBuffer);

    if (geometryType == Type::Lines)
        drawLines();
    else if (geometryType == Type::Mesh)
        drawMesh();
    else
        drawTriangles();
}

void Geometry::drawLines()
{
    QVector<int> indicesStarts = geometryBuffer->getIndicesStarts();
    QVector<int> vertexIndices = geometryBuffer->getVertexIndices();
    QVector<QVector3D> vertexData = geometryBuffer->getLinkedBuffer()->getVertexList();

    for (int indexEnd = 1; indexEnd < indicesStarts.size(); ++indexEnd){
        glBegin(GL_LINES);
        for (int vertexIndex = indicesStarts[indexEnd - 1]; vertexIndex < indicesStarts[indexEnd];
             ++vertexIndex){

            QVector3D vertex = vertexData[vertexIndices[vertexIndex] - 1]; // -1 because numeration indices starts with 1
            glVertex3f(vertex.x(), vertex.y(), vertex.z());

        }
        glEnd();
    }
}

void Geometry::drawMesh()
{
    QVector<int> indicesStarts = geometryBuffer->getIndicesStarts();
    QVector<int> vertexIndices = geometryBuffer->getVertexIndices();
    QVector<QVector3D> vertexData = geometryBuffer->getLinkedBuffer()->getVertexList();

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    for (int indexEnd = 1; indexEnd < indicesStarts.size(); ++indexEnd){
        glBegin(GL_POLYGON);
        for (int vertexIndex = indicesStarts[indexEnd - 1]; vertexIndex < indicesStarts[indexEnd];
             ++vertexIndex){

            QVector3D vertex = vertexData[vertexIndices[vertexIndex] - 1]; // -1 because numeration indices starts with 1
            glVertex3f(vertex.x(), vertex.y(), vertex.z());

        }
        glEnd();
    }
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void Geometry::drawTriangles()
{
    QVector<int> indicesStarts = geometryBuffer->getIndicesStarts();
    QVector<int> vertexIndices = geometryBuffer->getVertexIndices();
    QVector<QVector3D> vertexData = geometryBuffer->getLinkedBuffer()->getVertexList();

    bool hasTextureCoords = geometryBuffer->hasTextureIndices();
    bool hasNormals = geometryBuffer->hasNormalIndices();

    for (int indexEnd = 1; indexEnd < indicesStarts.size(); ++indexEnd){

        bool polygonSizeEqualsThree = indicesStarts[indexEnd] - indicesStarts[indexEnd - 1] == 3;
        Q_ASSERT(polygonSizeEqualsThree);
        glBegin(GL_TRIANGLES);
        for (int vertexIndex = indicesStarts[indexEnd - 1]; vertexIndex < indicesStarts[indexEnd];
             ++vertexIndex){

            QVector3D vertex = vertexData[vertexIndices[vertexIndex] - 1]; // -1 because numeration indices starts with 1
            glVertex3f(vertex.x(), vertex.y(), vertex.z());

        }
        glEnd();
    }
}
