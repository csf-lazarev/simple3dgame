#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <source/Buffer/geometrybuffer.h>
#include <GL/gl.h>

class Geometry
{
public:
    enum Type{
        Triangles = 1, Lines = 2, Mesh = 3
    };
public:

    Geometry();
    Geometry(GeometryBuffer *GeometryBuffer, Type geometryType);
    virtual ~Geometry();
    virtual void draw();
protected:

    virtual void drawLines();
    virtual void drawMesh();
    virtual void drawTriangles();

    GeometryBuffer* geometryBuffer;
    Type geometryType;

};

#endif // GEOMETRY_H
