#ifndef LINEGEOMETRY_H
#define LINEGEOMETRY_H

#include "basegeometry.h"

#include <source/Buffer/geometrybuffer.h>

#include <QOpenGLShaderProgram>



class LineGeometry : BaseGeometry
{
public:
    LineGeometry(GeometryBuffer *geometryBuffer, QOpenGLShaderProgram *shader);
    void draw() const override;
private:
    GeometryBuffer *geometrySource;
    QOpenGLShaderProgram *shader;
};

#endif // LINEGEOMETRY_H
