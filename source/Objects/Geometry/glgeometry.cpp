#include "glgeometry.h"



GLGeometry::GLGeometry(QOpenGLBuffer *vertexVbo, QOpenGLBuffer *normalVbo, size_t countVertex) :
    vertexVbo(vertexVbo), normalVbo(normalVbo), countVertex(countVertex)
{

}


size_t GLGeometry::getCountVertex() const
{
    return countVertex;
}

QOpenGLBuffer *GLGeometry::getVertexVbo() const
{
    return vertexVbo;
}

QOpenGLBuffer *GLGeometry::getNormalVbo() const
{
    return normalVbo;
}
