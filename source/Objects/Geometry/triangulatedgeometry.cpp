#include "triangulatedgeometry.h"
#include <QGLFunctions>

TriangulatedGeometry::TriangulatedGeometry(QOpenGLBuffer *vertexVBO, QOpenGLBuffer *vertexIBO, QOpenGLBuffer *normalVBO, QOpenGLBuffer *normalIBO, int vertexCount, int normalCount)
    : vertexVBO(vertexVBO), vertexIBO(vertexIBO), normalVBO(normalVBO), normalIBO(normalIBO), vertexCount(vertexCount), normalCount(normalCount)
{

}

QOpenGLBuffer *TriangulatedGeometry::getVertexVBO() const
{
    return vertexVBO;
}

QOpenGLBuffer *TriangulatedGeometry::getVertexIBO() const
{
    return vertexIBO;
}

QOpenGLBuffer *TriangulatedGeometry::getNormalVBO() const
{
    return normalVBO;
}

QOpenGLBuffer *TriangulatedGeometry::getNormalIBO() const
{
    return normalIBO;
}

int TriangulatedGeometry::getVertexCount() const
{
    return vertexCount;
}

int TriangulatedGeometry::getNormalCount() const
{
    return normalCount;
}
