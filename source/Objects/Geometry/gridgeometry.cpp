#include "gridgeometry.h"
#include <QGLFunctions>
#include <QDebug>
GridGeometry::GridGeometry(GeometryBuffer *geometrySource) : geometrySource(geometrySource)
{

}

void GridGeometry::draw() const
{
    QVector<int> indicesStarts = geometrySource->getIndicesStarts();
    QVector<int> vertexIndices = geometrySource->getVertexIndices();
    QVector<QVector3D> vertexData = geometrySource->getLinkedBuffer()->getVertexList();

    for (int indexEnd = 1; indexEnd < indicesStarts.size(); ++indexEnd){
        glBegin(GL_POLYGON);
        for (int vertexIndex = indicesStarts[indexEnd - 1]; vertexIndex < indicesStarts[indexEnd];
             ++vertexIndex){

            QVector3D vertex = vertexData[vertexIndices[vertexIndex]];
            glVertex3f(vertex.x(), vertex.y(), vertex.z());

        }
        glEnd();
    }
}
