#ifndef MESHGEOMETRY_H
#define MESHGEOMETRY_H

#include "basegeometry.h"

#include <source/Buffer/geometrybuffer.h>



class GridGeometry : public BaseGeometry
{
public:
    GridGeometry(GeometryBuffer *geometrySource);

    void draw() const override;

private:
    GeometryBuffer *geometrySource;
};

#endif // MESHGEOMETRY_H
