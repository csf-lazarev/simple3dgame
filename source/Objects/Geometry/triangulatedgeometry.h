#ifndef TRIANGULATEDGEOMETRY_H
#define TRIANGULATEDGEOMETRY_H

#include "basegeometry.h"

#include <source/Buffer/geometrybuffer.h>
#include <QOpenGLBuffer>



class TriangulatedGeometry
{
public:
    TriangulatedGeometry(QOpenGLBuffer *vertexVBO,
                         QOpenGLBuffer *vertexIBO,
                         QOpenGLBuffer *normalVBO,
                         QOpenGLBuffer *normalIBO,
                         int vertexCount,
                         int normalCount);
    QOpenGLBuffer *getVertexVBO() const;

    QOpenGLBuffer *getVertexIBO() const;

    QOpenGLBuffer *getNormalVBO() const;

    QOpenGLBuffer *getNormalIBO() const;

    int getVertexCount() const;

    int getNormalCount() const;

private:
    QOpenGLBuffer *vertexVBO;
    QOpenGLBuffer *vertexIBO;
    QOpenGLBuffer *normalVBO;
    QOpenGLBuffer *normalIBO;
    int vertexCount;
    int normalCount;
};

#endif // TRIANGULATEDGEOMETRY_H
