#include "linegeometry.h"
#include <QOpenGLFunctions>

LineGeometry::LineGeometry(GeometryBuffer *geometryBuffer, QOpenGLShaderProgram *shader) :
    geometrySource(geometryBuffer), shader(shader)
{

}

void LineGeometry::draw() const
{
    QVector<int> indicesStarts = geometrySource->getIndicesStarts();
    QVector<int> vertexIndices = geometrySource->getVertexIndices();
    QVector<QVector3D> vertexData = geometrySource->getLinkedBuffer()->getVertexList();

    for (int indexEnd = 1; indexEnd < indicesStarts.size(); ++indexEnd){
        glBegin(GL_LINE);
        for (int vertexIndex = indicesStarts[indexEnd - 1]; vertexIndex < indicesStarts[indexEnd];
             ++vertexIndex){

            QVector3D vertex = vertexData[vertexIndices[vertexIndex]];
            glVertex3f(vertex.x(), vertex.y(), vertex.z());

        }
        glEnd();
    }
}
