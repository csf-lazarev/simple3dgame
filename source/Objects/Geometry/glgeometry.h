#ifndef GLGEOMETRY_H
#define GLGEOMETRY_H

#include <source/OpenGL/glbuffer.h>
#include <QOpenGLVertexArrayObject>


class GLGeometry
{
public:
    GLGeometry(QOpenGLBuffer *vertexVbo,
    QOpenGLBuffer *normalVbo,
    size_t countVertex);

    size_t getCountVertex() const;

    QOpenGLBuffer *getVertexVbo() const;

    QOpenGLBuffer *getNormalVbo() const;

private:
    QOpenGLBuffer *vertexVbo;
    QOpenGLBuffer *normalVbo;
    size_t countVertex;
};

#endif // GLGEOMETRY_H
