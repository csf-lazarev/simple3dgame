#ifndef BASEGEOMETRY_H
#define BASEGEOMETRY_H


class BaseGeometry
{
public:
    BaseGeometry();
    virtual ~BaseGeometry();

    virtual void draw() const = 0;
};

#endif // BASEGEOMETRY_H
