#include "materialobject.h"

MaterialObject::MaterialObject(TGContainer *bufferContainer, Transform3D *transform, Material *material) :
    SceneObject(bufferContainer, transform), material(material)
{

}

Material *MaterialObject::getMaterial() const
{
    return material;
}

void MaterialObject::setMaterial(Material *value)
{
    material = value;
}
