#include "planegrid.h"


PlaneGrid::PlaneGrid(SimpleRenderHelper *renderHelper, const QVector3D &center, float width, float height, unsigned rows, unsigned columns) :
    ISelfRenderingObject(), renderHelper(renderHelper), center(center), width(width), height(height), rows(rows), columns(columns)
{

}

void PlaneGrid::render()
{
    ICamera *sceneCamera = renderHelper->getSceneEnvironment()->getActiveCamera();

    glMatrixMode(GL_PROJECTION);
    glLoadMatrixf((sceneCamera->getProjectionMatrix() * sceneCamera->getViewMatrix()).data());
    drawGrid();
    glLoadIdentity();
}

void PlaneGrid::drawGrid()
{

    QVector3D leftTop = center + QVector3D(width/2, 0, height/2);
    glColor3f(0.5, 0.5, 0.5);
    float strideRow = height / rows;
    float strideCol = width / columns;
    float leftX = leftTop.x();
    float rightX = leftTop.x() - strideCol * (columns - 1);
    float y = leftTop.y();
    float topZ = leftTop.z();
    float bottomZ = leftTop.z() - strideRow * (rows - 1);

    for (unsigned col = 0; col < columns; ++col){
        glBegin(GL_LINES);
        float z = topZ - strideRow * col;
        glVertex3f(leftX, y, z);
        glVertex3f(rightX, y, z);
        glEnd();
    }

    for (unsigned row = 0; row < rows; ++row){
        glBegin(GL_LINES);
        float x = leftX - strideCol * row;
        glVertex3f(x, y, topZ);
        glVertex3f(x, y, bottomZ);
        glEnd();
    }


}
