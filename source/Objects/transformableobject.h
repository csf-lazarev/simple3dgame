#ifndef TRANSFORMABLEOBJECT_H
#define TRANSFORMABLEOBJECT_H

#include "baserenderingobject.h"
#include "Transform/transform.h"


class TransformableObject
{
public:
    TransformableObject();
    virtual ~TransformableObject();

    virtual Transform *getTransform() const = 0;
};

#endif // TRANSFORMABLEOBJECT_H
