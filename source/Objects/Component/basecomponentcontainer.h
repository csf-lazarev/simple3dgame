#ifndef BASECOMPONENTCONTAINER_H
#define BASECOMPONENTCONTAINER_H

#include <source/typeid.h>
class BaseComponentContainer
{
public:
    BaseComponentContainer();
    virtual ~BaseComponentContainer();
    virtual const  TypeID getComponentTypeID() const = 0;
};

#endif // BASECOMPONENTCONTAINER_H
