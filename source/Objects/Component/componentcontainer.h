#ifndef COMPONENTCOMTAINER_H
#define COMPONENTCOMTAINER_H
#include "basecomponentcontainer.h"

#include <source/typeid.h>

template<class ComponentType>
class ComponentContainer : public BaseComponentContainer{
public:
    ComponentContainer(bool isHandlerResource) :
        componentSource(nullptr), isHandlerResource(isHandlerResource)
    {

    }

    ComponentContainer(const ComponentType* componentSource, bool isHandlerResource)
        : componentSource(componentSource), isHandlerResource(isHandlerResource){

    }

    ~ComponentContainer(){
        if (isHandlerResource)
            delete componentSource;
    }

    const ComponentType *getComponentSource() const
    {
        return componentSource;
    }

    ComponentType *getComponentSource(){
        return componentSource;
    }

    const TypeID getComponentTypeID() const override{
        return getTypeID<ComponentType>();
    }

private:
    ComponentType *componentSource;
    bool isHandlerResource;
};

#endif // COMPONENTCOMTAINER_H
