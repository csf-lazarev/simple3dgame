#include "objectmanager.h"

QVector<TypeID> ObjectManager::SRTypes
{

};

ObjectManager::ObjectManager()
{

}

ObjectManager::~ObjectManager()
{
   selfRenderingObjects.clear();
   noSelfRenderingObjects.clear();
}

void ObjectManager::registerObject(BaseRenderingObject *object)
{
    ISelfRenderingObject *casted = dynamic_cast<ISelfRenderingObject *>(object);
    if (casted)
        registerObject(casted);
    else
        registerObject(dynamic_cast<INoSelfRenderingObject *>(object));
}

void ObjectManager::registerObject(ISelfRenderingObject *object)
{
    selfRenderingObjects.append(object);
}

void ObjectManager::registerObject(INoSelfRenderingObject *object)
{
    noSelfRenderingObjects.append(object);
}

const QVector<ISelfRenderingObject *> ObjectManager::getSelfRenderingObjects() const
{
    return selfRenderingObjects;
}

const QVector<INoSelfRenderingObject *> &ObjectManager::getNoSelfRenderingObjects() const
{
    return noSelfRenderingObjects;
}
