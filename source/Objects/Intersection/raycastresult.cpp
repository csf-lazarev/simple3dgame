#include "raycastresult.h"

RaycastResult::RaycastResult(GTriangle *triangle, Point3D point, float rayDistance, GlobalBVHTreeItem *item) :
    triangle(triangle),point(point), rayDistance(rayDistance), item(item)
{

}


int RaycastResult::getTriangleIndex() const
{
    return triangle->getIndex();
}

Point3D RaycastResult::getPoint() const
{
    return point;
}

float RaycastResult::getRayDistance() const
{
    return rayDistance;
}

GTriangle *RaycastResult::getTriangle() const
{
    return triangle;
}

GlobalBVHTreeItem *RaycastResult::getItem() const
{
    return item;
}

void RaycastResult::setItem(GlobalBVHTreeItem *value)
{
    item = value;
}

