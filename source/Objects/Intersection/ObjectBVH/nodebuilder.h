#ifndef NODEBUILDER_H
#define NODEBUILDER_H

#include <source/Objects/Intersection/aabb.h>

#include <source/Buffer/gtriangle.h>
#include <source/Objects/Intersection/intersections.h>



class NodeBuilder
{
public:
    NodeBuilder(AABB *aabb, QVector<GTriangle *> *sourceTriangles);
    ~NodeBuilder();

    NodeBuilder *getLeft() const;

    NodeBuilder *getRight() const;

    NodeBuilder *getCenter() const;

    AABB *getVolumeBound() const;

    QVector<GTriangle *> *getSourceTriangles() const;
    bool divide(int limit);

    bool hasLeftPart();
    bool hasRightPart();
    bool hasCenterPart();

private:
    AABB * volumeBound;
    QVector<GTriangle *> *sourceTriangles;

    NodeBuilder *left;
    NodeBuilder *right;
    NodeBuilder *center;
};

#endif // NODEBUILDER_H
