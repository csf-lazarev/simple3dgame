#include "nodebuilder.h"

#include <source/Objects/Intersection/aabbcreator.h>

NodeBuilder::NodeBuilder(AABB *aabb, QVector<GTriangle *> *sourceTriangles) :
    volumeBound(aabb), sourceTriangles(sourceTriangles), left(nullptr), right(nullptr), center(nullptr)
{

}

NodeBuilder::~NodeBuilder()
{
    delete left;
    delete right;
    delete center;
}

bool NodeBuilder::divide(int limit)
{

    if (limit >= sourceTriangles->size())
        return false;

    QVector<GTriangle *> *leftTriangles = new QVector<GTriangle *>();
    QVector<GTriangle *> *rightTriangles = new QVector<GTriangle *>();
    QVector<GTriangle *> *centerTriangles = new QVector<GTriangle *>();

    int leftCounter = 0, rightCounter = 0, centerCouter = 0;
    leftTriangles->resize(sourceTriangles->size());
    rightTriangles->resize(sourceTriangles->size());
    centerTriangles->resize(sourceTriangles->size());

    AABBCreator leftBoxCreator, rightBoxCreator, centerBoxCreator;

    Plane *plane = volumeBound->getDivisionPlane();

    for (GTriangle * const triangle : *sourceTriangles){

        int pos = intersections::triangleAndPlane(*plane, triangle);

        if (pos < 0){
            leftTriangles->operator[](leftCounter++) = triangle;
            leftBoxCreator.addTriangle(*triangle);
        }
        else if (pos > 0){
            rightTriangles->operator[](rightCounter++) = triangle;
            rightBoxCreator.addTriangle(*triangle);
        }
        else {
            centerTriangles->operator[](centerCouter++) = triangle;
            centerBoxCreator.addTriangle(*triangle);
        }

    }

    if (centerTriangles->size() == sourceTriangles->size())
        return false;

    if (leftBoxCreator.created()){
        leftTriangles->resize(leftCounter);
        left = new NodeBuilder(leftBoxCreator.getAABB(), leftTriangles);
    }
    if (rightBoxCreator.created()){
        rightTriangles->resize(rightCounter);
        right = new NodeBuilder(rightBoxCreator.getAABB(), rightTriangles);
    }
    if (centerBoxCreator.created()){
        centerTriangles->resize(centerCouter);
        center = new NodeBuilder(centerBoxCreator.getAABB(), centerTriangles);
    }


    sourceTriangles->clear();
    return true;

}

bool NodeBuilder::hasLeftPart()
{
    return left;
}

bool NodeBuilder::hasRightPart()
{
    return right;
}

bool NodeBuilder::hasCenterPart()
{
    return center;
}

NodeBuilder *NodeBuilder::getCenter() const
{
    return center;
}

AABB *NodeBuilder::getVolumeBound() const
{
    return volumeBound;
}

QVector<GTriangle *> *NodeBuilder::getSourceTriangles() const
{
    return sourceTriangles;
}

NodeBuilder *NodeBuilder::getRight() const
{
    return right;
}

NodeBuilder *NodeBuilder::getLeft() const
{
    return left;
}
