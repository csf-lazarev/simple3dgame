#ifndef IOBJECTBVHTREENODE_H
#define IOBJECTBVHTREENODE_H

#include <source/Objects/Intersection/ray.h>
#include <source/Objects/Intersection/raycastresult.h>
#include <QVector>


class IObjectBVHTreeNode
{
public:
    IObjectBVHTreeNode();
    virtual ~IObjectBVHTreeNode();

    virtual bool traceRay(const Ray &ray, QVector<RaycastResult *> &res) const = 0;
};

#endif // IOBJECTBVHTREENODE_H
