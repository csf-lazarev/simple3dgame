#ifndef OBJECTBVHTREELEAFNODE_H
#define OBJECTBVHTREELEAFNODE_H

#include "iobjectbvhtreenode.h"

#include <QVector>

#include <source/Buffer/gtriangle.h>
#include <source/Objects/Intersection/intersections.h>



class ObjectBVHTreeLeafNode : public IObjectBVHTreeNode
{
public:
    ObjectBVHTreeLeafNode(AABB *volumeBound, QVector<GTriangle *> *triangles);
    ~ObjectBVHTreeLeafNode() override;
    bool traceRay(const Ray &ray, QVector<RaycastResult *> &res) const override;
private:
    AABB *volumeBound;
    QVector<GTriangle *> *triangles;
};

#endif // OBJECTBVHTREELEAFNODE_H
