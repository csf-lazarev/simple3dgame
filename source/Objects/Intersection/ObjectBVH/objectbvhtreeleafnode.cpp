#include "objectbvhtreeleafnode.h"

ObjectBVHTreeLeafNode::ObjectBVHTreeLeafNode(AABB *volumeBound, QVector<GTriangle *> *triangles) :
    volumeBound(volumeBound), triangles(triangles)
{

}

ObjectBVHTreeLeafNode::~ObjectBVHTreeLeafNode()
{
    delete volumeBound;
    triangles->clear();
    delete triangles;
}

bool ObjectBVHTreeLeafNode::traceRay(const Ray &ray, QVector<RaycastResult *> &res) const
{
    bool status = false;
    if (!intersections::intersect(*volumeBound, ray))
        return false;

    for (GTriangle * triangle : *triangles){

        float rc = intersections::raycast(*triangle, ray);
        if (rc > 1e-8f){
            res.append(new RaycastResult(triangle, ray.getOrigin() + rc * ray.getDirection(), rc));
            status = true;
        }

    }

    return status;
}
