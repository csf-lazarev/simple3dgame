#include "objectbvhtree.h"

ObjectBVHTree::ObjectBVHTree(TriangulatedGeometryBuffer * const modelTringulatedBuffer, unsigned trianglesLimit) : linkedGeometryBuffer(modelTringulatedBuffer),
    trianglesLimit(trianglesLimit)
{
    root = new ObjectBVHTreeNode();
}

ObjectBVHTree::~ObjectBVHTree()
{
    delete root;
}

void ObjectBVHTree::compute()
{
    AABBCreator creator;
    QVector<GTriangle *> triangles = linkedGeometryBuffer->getTrianglesData();

    QVector<GTriangle *> *copy = new QVector<GTriangle *>();
    copy->resize(triangles.size());

    for (int i = 0; i < triangles.size(); ++i){
        GTriangle *t = triangles[i];
        copy->operator[](i) = t;
        creator.addTriangle(*t);
    }

    NodeBuilder *builder = new NodeBuilder(creator.getAABB(), copy);
    root->create(builder, trianglesLimit);
    delete builder;
}

AABB *ObjectBVHTree::getVolumeBound() const
{
    return root->getVolumeBound();
}

bool ObjectBVHTree::traceRay(const Ray &ray, QVector<RaycastResult *> &results)
{
    return root->traceRay(ray, results);
}

TriangulatedGeometryBuffer *ObjectBVHTree::getLinkedGeometryBuffer() const
{
    return linkedGeometryBuffer;
}
