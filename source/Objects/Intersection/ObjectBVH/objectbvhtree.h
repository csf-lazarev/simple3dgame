#ifndef OBJECTBVHTREE_H
#define OBJECTBVHTREE_H

#include "objectbvhtreenode.h"

#include <source/Buffer/triangulatedgeometrybuffer.h>
#include <source/Objects/Intersection/aabb.h>
#include <source/Objects/Intersection/aabbcreator.h>
#include <source/Objects/Intersection/intersections.h>
#include <source/Objects/Intersection/raycastresult.h>
#include <QVector>



class ObjectBVHTree
{
public:
    ObjectBVHTree(TriangulatedGeometryBuffer * const modelTringulatedBuffer,unsigned trianglesLimit);
    ~ObjectBVHTree();
    void compute();
    AABB *getVolumeBound() const;
    bool traceRay(const Ray &ray, QVector<RaycastResult *> &results);
    TriangulatedGeometryBuffer *getLinkedGeometryBuffer() const;

private:
    TriangulatedGeometryBuffer * const linkedGeometryBuffer;
    ObjectBVHTreeNode *root;
    unsigned trianglesLimit;
};

#endif // OBJECTBVHTREE_H
