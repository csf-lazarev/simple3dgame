#include "objectbvhtreeleafnode.h"
#include "objectbvhtreenode.h"


ObjectBVHTreeNode::~ObjectBVHTreeNode()
{
    delete leftNode;
    delete rightNode;
    delete centerNode;
}

void ObjectBVHTreeNode::create(NodeBuilder *builder, int limit)
{
    volumeBound = builder->getVolumeBound();
    if (builder->divide(limit)){

        if (builder->hasLeftPart()){
            ObjectBVHTreeNode *node = new ObjectBVHTreeNode();
            leftNode = node;
            node->create(builder->getLeft(), limit);
        }

        if (builder->hasRightPart()){
            ObjectBVHTreeNode *node = new ObjectBVHTreeNode();
            rightNode = node;
            node->create(builder->getRight(), limit);
        }

        if (builder->hasCenterPart()){
            ObjectBVHTreeNode *node = new ObjectBVHTreeNode();
            centerNode = node;
            node->create(builder->getCenter(), limit);
        }

    }
    else
    {
        centerNode = new ObjectBVHTreeLeafNode(builder->getVolumeBound(), builder->getSourceTriangles());
    }
}

bool ObjectBVHTreeNode::traceRay(const Ray &ray, QVector<RaycastResult *> &res) const
{
    if (!intersections::intersect(*volumeBound, ray))
        return false;
    bool status = false;
    if (leftNode)
        status |= leftNode->traceRay(ray, res);
    if (rightNode)
        status |= rightNode->traceRay(ray, res);
    if (centerNode)
        status |= centerNode->traceRay(ray, res);
    return status;
}

AABB *ObjectBVHTreeNode::getVolumeBound() const
{
    return volumeBound;
}

