#ifndef OBJECTBVHTREENODE_H
#define OBJECTBVHTREENODE_H

#include "iobjectbvhtreenode.h"
#include "nodebuilder.h"

#include <source/Objects/Intersection/aabb.h>
#include <source/Objects/Intersection/aabbcreator.h>
#include <source/Objects/Intersection/intersections.h>
#include <QVector>
#include <QDebug>


class ObjectBVHTreeNode : public IObjectBVHTreeNode
{
public:
    ~ObjectBVHTreeNode() override;
    void create(NodeBuilder * builder, int limit);

    bool traceRay(const Ray &ray, QVector<RaycastResult *> &res) const override;

    AABB *getVolumeBound() const;

private:



private:
    AABB *volumeBound;
    IObjectBVHTreeNode *leftNode;
    IObjectBVHTreeNode *centerNode;
    IObjectBVHTreeNode *rightNode;

};

#endif // OBJECTBVHTREENODE_H
