#ifndef GLOBALBVHTREEITEM_H
#define GLOBALBVHTREEITEM_H

#include "aabb.h"

#include <source/Objects/Transform/transform3d.h>
#include <source/Objects/Intersection/ObjectBVH/objectbvhtree.h>



class GlobalBVHTreeItem
{
public:
    GlobalBVHTreeItem(Transform *transform, ObjectBVHTree *objectTree);
    ~GlobalBVHTreeItem();
    AABB *getObjectAABB() const;

    Transform *getObjectTransform() const;

    ObjectBVHTree *getObjectTree() const;
    int getId() const;

private:
    static int idCounter;
private:
    const int id;
    Transform *objectTransform;
    ObjectBVHTree *objectTree;
    AABB *objectAABB;
};

#endif // GLOBALBVHTREEITEM_H
