#ifndef BVHTREE_H
#define BVHTREE_H

#include "iglobalbvhtreenode.h"

#include <QVector>


class BVHTree
{
public:
    BVHTree(IBVHTreeNode *root);
    ~BVHTree();

    bool traceRay(const Ray &ray, QMap<int, GlobalBVHTreeItem *> &itemStore);
    void addItem(GlobalBVHTreeItem *item);
    void acsept(IBVHTreeVisitor *visitor);
private:
    IBVHTreeNode *root;

};

#endif // BVHTREE_H
