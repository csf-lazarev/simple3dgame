#ifndef IBVHTREENODE_H
#define IBVHTREENODE_H

#include "aabb.h"
#include "ibvhtreevisitor.h"

#include <QVector>

class Ray;
class GlobalBVHTreeItem;


class IBVHTreeNode
{
public:
    IBVHTreeNode(AABB *volumeBound);
    virtual ~IBVHTreeNode();
    virtual void insertItem(GlobalBVHTreeItem *item) = 0;
    virtual bool checkIntersection(const Ray &ray, QMap<int, GlobalBVHTreeItem*> &itemStore) = 0;
    virtual void acsept(IBVHTreeVisitor *visitor) = 0;

    AABB *getVolumeBound() const;

protected:
    AABB *volumeBound;
};

#endif // IBVHTREENODE_H
