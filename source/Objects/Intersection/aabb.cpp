#include "aabb.h"
#include "aabbcreator.h"
#include <source/Objects/Intersection/intersections.h>


AABB::AABB(const Point3D &center, float hXExtent, float hYExtent, float hZExtent) :
    center(center), halfExtents(hXExtent , hYExtent, hZExtent), minPoint(center - halfExtents), maxPoint(center + halfExtents)
{
    //optimizedExtents();
}

AABB::AABB(const Point3D &minPoint, const Point3D &maxPoint) :
    center((minPoint + maxPoint) / 2), halfExtents((maxPoint - minPoint) / 2), minPoint(minPoint), maxPoint(maxPoint)
{
    //optimizedExtents();
}

bool AABB::containsPoint(const Point3D &point)
{
    bool pointHigestThatMin = point.x() >= minPoint.x() && point.y() >= minPoint.y() &&
            point.z() >= minPoint.z();

    bool pointLowerThatMax = point.x() <= maxPoint.x() && point.y() <= maxPoint.y() &&
            point.z() <= maxPoint.z();

    return pointLowerThatMax && pointHigestThatMin;
}

Point3D AABB::getMinPoint() const
{
    return minPoint;
}

Point3D AABB::getMaxPoint() const
{
    return maxPoint;
}

Point3D AABB::getCenter() const
{
    return center;
}

QVector3D AABB::getHalfExtents() const
{
    return halfExtents;
}

AABB *AABB::transformed(const Transform &t)
{
    AABBCreator creator;
    QMatrix4x4 transform =t.getTransformMatrix();
    for (QVector4D p : getBoundPoints4D()){
        QVector4D transformedPoint = transform * p;
        creator.addPoint(transformedPoint);
    }
    return creator.getAABB();
}

QVector<QVector3D> AABB::getBoundPoints3D()
{
    return QVector<QVector3D>
    {
        QVector3D(minPoint),
        QVector3D(maxPoint),
        QVector3D(minPoint[0] + halfExtents[0], minPoint[1], minPoint[2]),
        QVector3D(minPoint[0], minPoint[1] + halfExtents[1], minPoint[2]),
        QVector3D(minPoint[0], minPoint[1], minPoint[2] + halfExtents[2]),
        QVector3D(maxPoint[0] - halfExtents[0], maxPoint[1], maxPoint[2]),
        QVector3D(maxPoint[0], maxPoint[1] - halfExtents[1], maxPoint[2]),
        QVector3D(maxPoint[0], maxPoint[1], maxPoint[2] - halfExtents[2]),
    };
}

QVector<QVector4D> AABB::getBoundPoints4D()
{
    return QVector<QVector4D>
    {
        QVector4D(minPoint, 1),
        QVector4D(maxPoint, 1),
        QVector4D(minPoint[0] + halfExtents[0], minPoint[1], minPoint[2], 1),
        QVector4D(minPoint[0], minPoint[1] + halfExtents[1], minPoint[2], 1),
        QVector4D(minPoint[0], minPoint[1], minPoint[2] + halfExtents[2], 1),
        QVector4D(maxPoint[0] - halfExtents[0], maxPoint[1], maxPoint[2], 1),
        QVector4D(maxPoint[0], maxPoint[1] - halfExtents[1], maxPoint[2], 1),
        QVector4D(maxPoint[0], maxPoint[1], maxPoint[2] - halfExtents[2], 1),
    };
}

QPair<AABB *, AABB *> AABB::halfDivideByLongestSide() const
{
    int longExtentIndex = 0;

    if (halfExtents.x() >= halfExtents.y() && halfExtents.x() >= halfExtents.z())
        longExtentIndex = 0;
    else if (halfExtents.y() >= halfExtents.z()) {
        longExtentIndex = 1;
    }
    else{
        longExtentIndex = 2;
    }

    Point3D leftCenter = Point3D(center);
    Point3D rightCenter = Point3D(center);
    QVector3D hExt = QVector3D(halfExtents);
    hExt[longExtentIndex] /= 2;

    leftCenter[longExtentIndex] -= hExt[longExtentIndex];
    rightCenter[longExtentIndex] += hExt[longExtentIndex];

    return QPair<AABB *, AABB *>(new AABB(leftCenter, hExt.x(), hExt.y(), hExt.z()),
                                 new AABB(rightCenter, hExt.x(), hExt.y(), hExt.z()));
}

Plane *AABB::getDivisionPlane()
{
    int longExtentIndex = 0;

    if (halfExtents.x() >= halfExtents.y() && halfExtents.x() >= halfExtents.z())
        longExtentIndex = 0;
    else if (halfExtents.y() >= halfExtents.z()) {
        longExtentIndex = 1;
    }
    else{
        longExtentIndex = 2;
    }

    QVector3D normal(0,0,0);
    normal[longExtentIndex] = 1;

    return new Plane(normal, center[longExtentIndex]);

}

AABB *AABB::getIntersectionBox(AABB *first, AABB *second)
{
    bool intesect = intersections::intersect(*first, *second);
    if (!intesect)
        return new AABB(QVector3D(0,0,0), QVector3D(0,0,0));
    Point3D iMin = first->minPoint, iMax = first->maxPoint;

    Point3D aMin = first->minPoint, aMax = first->maxPoint;
    Point3D bMin = second->minPoint, bMax = second->maxPoint;

    for (int i = 0; i < 3; ++i){

        if (iMin[i] < aMin[i])
            iMin[i] = aMin[i];
        if (iMin[i] < bMin[i])
            iMin[i] = bMin[i];

        if (iMax[i] > aMax[i])
            iMax[i] = aMax[i];
        if (iMax[i] > bMax[i])
            iMax[i] = bMax[i];
    }

    return new AABB(iMin, iMax);
}

void AABB::optimizedExtents()
{
    bool needRecalc = false;
    for (int i = 0; i < 3; ++i)
        if (halfExtents[i] < 1e-6f){
            needRecalc = true;
            halfExtents[i] = 1e-6f;
        }
    if (needRecalc)
        recalcMinMax();
}

void AABB::recalcMinMax()
{
    minPoint = QVector3D(center - halfExtents);
    maxPoint = QVector3D(center + halfExtents);
}
