#ifndef AABB_H
#define AABB_H

#include "plane.h"
#include "ray.h"
#include <QPair>
#include <source/Objects/Transform/transform.h>


using Point3D = QVector3D;

class AABB
{
public:
    AABB(const Point3D &center, float hXExtent, float hYExtent, float hZExtent);
    AABB (const Point3D &minPoint, const Point3D &maxPoint);
    bool containsPoint(const Point3D &point);

    Point3D getMinPoint() const;

    Point3D getMaxPoint() const;

    Point3D getCenter() const;

    QVector3D getHalfExtents() const;

    /**
     * @brief transformed - apply transform matrix from t by even\n
     * point this AABB, then recalcilated AABB by trasformed points
     * @param t trasformation
     * @return transformed AABB
     */
    AABB *transformed(const Transform &t);

    QVector<QVector3D> getBoundPoints3D();
    QVector<QVector4D> getBoundPoints4D();

    QPair<AABB *, AABB *> halfDivideByLongestSide() const;

    Plane *getDivisionPlane();

    static AABB *getIntersectionBox(AABB *first, AABB *second);

private:
    void optimizedExtents();
    void recalcMinMax();
private:
    Point3D center;
    QVector3D halfExtents;
    Point3D minPoint;
    Point3D maxPoint;
};

#endif // AABB_H
