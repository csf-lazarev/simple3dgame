#include "globalbvhtreeleafnode.h"

BVHTreeLeafNode::BVHTreeLeafNode(AABB *volumeBound) : IBVHTreeNode(volumeBound)
{

}

BVHTreeLeafNode::~BVHTreeLeafNode()
{
    delete volumeBound;
}

void BVHTreeLeafNode::insertItem(GlobalBVHTreeItem *item)
{
    if (intersections::intersect(*volumeBound, *item->getObjectAABB()))
        items.append(item);
}

bool BVHTreeLeafNode::checkIntersection(const Ray &ray, QMap<int, GlobalBVHTreeItem*> &itemStore)
{
    bool intersect = intersections::intersect(*volumeBound, ray);

    if (!intersect)
        return false;

    for (GlobalBVHTreeItem * item : items){
//        Ray *transRay = ray.getTransformed(item->getObjectTransform()->getTransformMatrix().inverted());
        if (intersections::intersect(*item->getObjectAABB(), ray)){
            intersect = true;
           if (!itemStore.contains(item->getId()))
               itemStore.insert(item->getId(), item);
        }
    }
    return intersect;

}

void BVHTreeLeafNode::acsept(IBVHTreeVisitor *visitor)
{
    visitor->visit(this);
}
