#ifndef BVHTREELEAFNODE_H
#define BVHTREELEAFNODE_H

#include "globalbvhtreeitem.h"
#include "iglobalbvhtreenode.h"
#include <source/Objects/Intersection/intersections.h>

#include <QVector>



class BVHTreeLeafNode : public IBVHTreeNode
{
public:
    BVHTreeLeafNode(AABB *volumeBound);
    ~BVHTreeLeafNode() override;
    void insertItem(GlobalBVHTreeItem *item) override;
    bool checkIntersection(const Ray &ray, QMap<int, GlobalBVHTreeItem *> &itemStore) override;
    void acsept(IBVHTreeVisitor *visitor) override;
private:
    QVector<GlobalBVHTreeItem *> items;
};

#endif // BVHTREELEAFNODE_H
