#ifndef RAYCASTRESULT_H
#define RAYCASTRESULT_H

#include <source/Buffer/gtriangle.h>

#include <QVector>

class GlobalBVHTreeItem;

class RaycastResult
{
public:
    RaycastResult(GTriangle * triangle, Point3D point, float rayDistance, GlobalBVHTreeItem *item = nullptr);

    int getTriangleIndex() const;

    Point3D getPoint() const;

    float getRayDistance() const;

    GTriangle *getTriangle() const;

    GlobalBVHTreeItem *getItem() const;
    void setItem(GlobalBVHTreeItem *value);

private:
    GTriangle * triangle;
    Point3D point;
    float rayDistance;
    GlobalBVHTreeItem *item;
};

#endif // RAYCASTRESULT_H
