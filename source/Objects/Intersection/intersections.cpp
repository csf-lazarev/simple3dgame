#include "intersections.h"

float intersections::raycast(const AABB &aabb, const Ray &ray)
{
    Point3D min = aabb.getMinPoint();
    Point3D max = aabb.getMaxPoint();

    QVector3D rayDir = ray.getNormalizedDirection();
    Point3D rayOrig = ray.getOrigin();
    float t1 = (min.x() - rayOrig.x()) / (rayDir.x() == 0.f ? 1e-7f : rayDir.x());
    float t2 = (max.x() - rayOrig.x()) / (rayDir.x() == 0.f ? 1e-7f : rayDir.x());

    float t3 = (min.y() - rayOrig.y()) / (rayDir.y() == 0.f ? 1e-7f : rayDir.y());
    float t4 = (max.y() - rayOrig.y()) / (rayDir.y() == 0.f ? 1e-7f : rayDir.y());

    float t5 = (min.z() - rayOrig.z()) / (rayDir.z() == 0.f ? 1e-7f : rayDir.z());
    float t6 = (max.z() - rayOrig.z()) / (rayDir.z() == 0.f ? 1e-7f : rayDir.z());

    float tMin = qMax(qMax(qMin(t1,t2), qMin(t3,t4)), qMin(t5,t6));
    float tMax = qMin(qMin(qMax(t1,t2), qMax(t3,t4)), qMax(t5,t6));

    bool noIntersects = tMax < 0.f || tMin > tMax;

    if (noIntersects)
        return -1.f;
    return tMin < 0.f ? tMax : tMin;
}


bool intersections::intersect(const AABB &aabb, const Ray &ray)
{
    return raycast(aabb, ray) >= 0.f;
}

bool intersections::intersect(const AABB &first, const AABB &second)
{
    QVector3D sumExtens = first.getHalfExtents() + second.getHalfExtents();

    Point3D fCenter = first.getCenter();
    Point3D sCenter = second.getCenter();

    float xDistance = qAbs(fCenter.x() - sCenter.x());
    float yDistance = qAbs(fCenter.y() - sCenter.y());
    float zDistance = qAbs(fCenter.z() - sCenter.z());

    return xDistance <= sumExtens.x() && yDistance <= sumExtens.y() && zDistance <= sumExtens.z();

}

float intersections::raycast2(const Triangle &triangle, const Ray &ray)
{
    Plane plane(triangle);
    if (!plane.isValid()){
        float d1 = ray.distanceToPoint(triangle.getA());
        float d2 = ray.distanceToPoint(triangle.getB());
        float d3 = ray.distanceToPoint(triangle.getC());
        return qMax(qMax(d1, d2), d3);
    }
    float rc = raycast(plane, ray);
    if (rc < 1e-7f)
        return -1;
    Point3D testPoint = ray.getOrigin();
    if (rc > 1e-7f)
        testPoint += rc * ray.getNormalizedDirection();
    return triangle.containsPoint(testPoint) ? rc : -1;
}

float intersections::raycast(const Plane &plane, const Ray &ray)
{
    float h1 = plane.signedDistanceToPoint(ray.getOrigin());
    if (qAbs(h1) < 1e-7f)
        return 0;
    float delta = plane.signedDistanceToPoint(ray.getOrigin() + ray.getNormalizedDirection()) - h1;

    if (h1 * delta >= 0.f)
        return -1;

    float absCos = qAbs(QVector3D::dotProduct(ray.getNormalizedDirection(), plane.getNormal()));

    return qAbs(h1) / (absCos * ray.getDirection().length());
}

bool intersections::intersect(const Triangle &triangle, const Ray &ray)
{
    return raycast(triangle, ray) > 1e-8f;
}

int intersections::triangleAndPlane(const Plane &plane, const Triangle *triangle)
{
    float p1 = plane.signedDistanceToPoint(triangle->getA());
    float p2 = plane.signedDistanceToPoint(triangle->getB());
    float p3 = plane.signedDistanceToPoint(triangle->getC());

    if (p1 < 0 && p2 < 0 && p3 < 0)
        return -1;
    else if (p1 >= 0 && p2 >= 0 && p3 >= 0)
        return 1;
    else return 0;

}

float intersections::raycast(const QVector3D &point, const Ray &ray)
{
    return ray.distanceToPoint(point);
}

float intersections::raycast(const Triangle &triangle, const Ray &ray)
{
    vector3 e1 = triangle.getB() - triangle.getA();
    vector3 e2 = triangle.getC() - triangle.getA();
        // Вычисление вектора нормали к плоскости
    vector3 pvec = QVector3D::crossProduct(ray.getNormalizedDirection(), e2);
    float det = QVector3D::dotProduct(e1, pvec);

    // Луч параллелен плоскости
    if (det < 1e-8f && det > -1e-8f) {
        return -1;
    }

    float inv_det = 1 / det;
    vector3 tvec = ray.getOrigin() - triangle.getA();
    float u = QVector3D::dotProduct(tvec, pvec) * inv_det;
    if (u < 0 || u > 1) {
        return -1;
    }

    vector3 qvec = QVector3D::crossProduct(tvec, e1);
    float v = QVector3D::dotProduct(ray.getNormalizedDirection(), qvec) * inv_det;
    if (v < 0 || u + v > 1) {
        return -1;
    }
    return QVector3D::dotProduct(e2, qvec) * inv_det / ray.getDirection().length();
}
