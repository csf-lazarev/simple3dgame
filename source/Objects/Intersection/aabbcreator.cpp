#include "aabbcreator.h"

AABBCreator::AABBCreator()
{

}

void AABBCreator::addPoint(const Point3D &point)
{
    if (!create){
        create = true;
        minPoint = point;
        maxPoint = point;
    }
    else
    {
     for (int i = 0; i < 3; ++i){
         if(minPoint[i] > point[i])
             minPoint[i] = point[i];
         else if (maxPoint[i] < point[i])
             maxPoint[i] = point[i];
     }
    }
}

void AABBCreator::addPoint(const QVector4D &point)
{
    addPoint(point.toVector3D());
}

void AABBCreator::addTriangle(const Triangle &triangle)
{
    addPoint(triangle.getA());
    addPoint(triangle.getB());
    addPoint(triangle.getC());
}

void AABBCreator::reset()
{
    create = false;
}

AABB *AABBCreator::getAABB() const
{
    if (!create)
        return nullptr;
    return new AABB(minPoint, maxPoint);
}

bool AABBCreator::created() const
{
    return create;
}
