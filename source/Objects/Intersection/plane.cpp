#include "plane.h"

Plane::Plane(const QVector3D &point1, const QVector3D &point2, const QVector3D &point3) :
    normal(QVector3D::crossProduct(point2 - point1, point3 - point2).normalized()),
    distance(-QVector3D::dotProduct(normal, point1))
{

}

Plane::Plane(const Triangle &triangle) : Plane(triangle.getA(), triangle.getB(), triangle.getC())
{

}

Plane::Plane(const QVector3D &normal, float distance) : normal(normal), distance(distance)
{

}

QVector3D Plane::getNormal() const
{
    return normal;
}

bool Plane::isValid()
{
    return normal.length() != 0.f;
}

float Plane::getDistance() const
{
    return distance;
}

float Plane::distanceToPoint(const Point3D &point) const
{
    return qAbs(signedDistanceToPoint(point));
}

float Plane::signedDistanceToPoint(const Point3D &point) const
{
    return QVector3D::dotProduct(normal, point) + distance;
}
