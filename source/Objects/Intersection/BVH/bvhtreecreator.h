#ifndef BVHTREECREATOR_H
#define BVHTREECREATOR_H

#include <source/Objects/Intersection/aabb.h>
#include <source/Objects/Intersection/globalbvhtreenode.h>
#include <source/Objects/Intersection/globalbvhtreeleafnode.h>
#include <source/Objects/Intersection/bvhtree.h>
#include <QtGlobal>
#include <QPair>


class BVHTreeCreator
{
public:
    BVHTreeCreator();

    static BVHTree *createTree(AABB *startVolume, unsigned countLevels);
    static QPair<AABB*, AABB*> halfDivideAABBByXAxis(AABB *parent);
    static QPair<AABB*, AABB*> halfDivideAABBByYAxis(AABB *parent);
    static QPair<AABB*, AABB*> halfDivideAABBByZAxis(AABB *parent);
};

#endif // BVHTREECREATOR_H
