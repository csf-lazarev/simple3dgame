#include "bvhtreecreator.h"

#include <QQueue>

BVHTreeCreator::BVHTreeCreator()
{

}

BVHTree *BVHTreeCreator::createTree(AABB *startVolume, unsigned countLevels)
{
    Q_ASSERT(countLevels > 0);
    BVHTreeNode *root = new BVHTreeNode(startVolume);
    QQueue<BVHTreeNode *> parentQueue;
    QQueue<BVHTreeNode *> childQueue;
    parentQueue.push_back(root);

    unsigned divAxis = 0;

    for (unsigned level = 0; level < countLevels - 1; ++level){

        QVector3D hExt = parentQueue.first()->getVolumeBound()->getHalfExtents();

//        if (hExt[0] >= hExt[1] && hExt[0] >= hExt[2])
//            divAxis = 0;
//        else if (hExt[1] >= hExt[2])
//            divAxis = 1;
//        else
//            divAxis = 2;

        while (!parentQueue.isEmpty()){

            BVHTreeNode* parentNode = parentQueue.dequeue();

            QPair<AABB *, AABB *> childBounds = parentNode->getVolumeBound()->halfDivideByLongestSide();

//            if (divAxis == 0)
//                childBounds = halfDivideAABBByXAxis(parentNode->getVolumeBound());
//            else if (divAxis == 1)
//                childBounds = halfDivideAABBByYAxis(parentNode->getVolumeBound());
//            else
//                childBounds = halfDivideAABBByZAxis(parentNode->getVolumeBound());

            BVHTreeNode *leftNode = new BVHTreeNode(childBounds.first);
            BVHTreeNode *rightNode = new BVHTreeNode(childBounds.second);

            parentNode->setLeftNode(leftNode);
            parentNode->setRightNode(rightNode);

            childQueue.push_back(leftNode);
            childQueue.push_back(rightNode);

        }

        parentQueue.swap(childQueue);

    }

    QVector3D hExt = parentQueue.first()->getVolumeBound()->getHalfExtents();
//    if (hExt[0] >= hExt[1] && hExt[0] >= hExt[2])
//        divAxis = 0;
//    else if (hExt[1] >= hExt[2])
//        divAxis = 1;
//    else
//        divAxis = 2;

    while (!parentQueue.isEmpty()){

        BVHTreeNode* parentNode = parentQueue.dequeue();

        QPair<AABB *, AABB *> childBounds = parentNode->getVolumeBound()->halfDivideByLongestSide();

//        if (divAxis == 0)
//            childBounds = halfDivideAABBByXAxis(parentNode->getVolumeBound());
//        else if (divAxis == 1)
//            childBounds = halfDivideAABBByYAxis(parentNode->getVolumeBound());
//        else
//            childBounds = halfDivideAABBByZAxis(parentNode->getVolumeBound());

        BVHTreeLeafNode *leftNode = new BVHTreeLeafNode(childBounds.first);
        BVHTreeLeafNode *rightNode = new BVHTreeLeafNode(childBounds.second);

        parentNode->setLeftNode(leftNode);
        parentNode->setRightNode(rightNode);
    }

    return new BVHTree(root);
}

QPair<AABB *, AABB *> BVHTreeCreator::halfDivideAABBByXAxis(AABB *parent)
{
    const QVector3D hExt = parent->getHalfExtents();
    const Point3D parentCenter = parent->getCenter();
    float xCenterDelta = hExt.x() / 2;
    AABB *left = new AABB(QVector3D(parentCenter.x() - xCenterDelta, parentCenter.y(), parentCenter.z()),
                          hExt.x() / 2, hExt.y(), hExt.z());
    AABB *right = new AABB(QVector3D(parentCenter.x() + xCenterDelta, parentCenter.y(), parentCenter.z()),
                           hExt.x() / 2, hExt.y(), hExt.z());

    return QPair<AABB *, AABB *>(left, right);
}

QPair<AABB *, AABB *> BVHTreeCreator::halfDivideAABBByYAxis(AABB *parent)
{
    const QVector3D hExt = parent->getHalfExtents();
    const Point3D parentCenter = parent->getCenter();
    float yCenterDelta = hExt.y() / 2;
    AABB *left = new AABB(QVector3D(parentCenter.x(), parentCenter.y() - yCenterDelta, parentCenter.z()),
                          hExt.x(), hExt.y() / 2, hExt.z());
    AABB *right = new AABB(QVector3D(parentCenter.x(), parentCenter.y() + yCenterDelta, parentCenter.z()),
                           hExt.x(), hExt.y() / 2, hExt.z());

    return QPair<AABB *, AABB *>(left, right);
}

QPair<AABB *, AABB *> BVHTreeCreator::halfDivideAABBByZAxis(AABB *parent)
{
    const QVector3D hExt = parent->getHalfExtents();
    const Point3D parentCenter = parent->getCenter();
    float zCenterDelta = hExt.z() / 2;
    AABB *left = new AABB(QVector3D(parentCenter.x(), parentCenter.y(), parentCenter.z() - zCenterDelta),
                          hExt.x(), hExt.y(), hExt.z() / 2);
    AABB *right = new AABB(QVector3D(parentCenter.x(), parentCenter.y(), parentCenter.z() + zCenterDelta),
                           hExt.x(), hExt.y(), hExt.z() / 2);

    return QPair<AABB *, AABB *>(left, right);
}
