#ifndef RAYTRACER_H
#define RAYTRACER_H

#include "raycastresult.h"
#include <source/Objects/Intersection/globalbvhtreeitem.h>
#include <source/Objects/Intersection/bvhtree.h>
#include <source/Objects/Intersection/RayTracer/raytracingmode.h>
#include <source/Objects/Intersection/RayTracer/raycastresultstore.h>

class RayTracer
{
public :
    RayTracer(BVHTree *globalTree, RayTracingMode defaultMode = RayTracingMode(RayTracingMode::DetectMode::NEAREST, RayTracingMode::FaceMode::FRONT));
    const QVector<RaycastResult *> &trace(const Ray &ray, bool *status = nullptr);
    const RaycastResultStore &getResultStore();
private:
    BVHTree *globalTree;
    RayTracingMode defaultMode;
    RaycastResultStore resultStore;
};

#endif // RAYTRACER_H
