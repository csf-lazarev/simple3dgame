#ifndef INDEXEDTRIANGLE_H
#define INDEXEDTRIANGLE_H

#include "triangle.h"


class IndexedTriangle : public Triangle
{
public:
    IndexedTriangle(const Point3D &a, const Point3D &b, const Point3D &c, int index);
    IndexedTriangle(const Point3D &a, const Point3D &b, const Point3D &c, int index, QVector3D normal);
    int getIndex() const;
    void setIndex(int value);

    QVector3D getNormal() const;

private:
    int index;
    QVector3D normal;
};

#endif // INDEXEDTRIANGLE_H
