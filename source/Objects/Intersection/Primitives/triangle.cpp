#include "triangle.h"

Triangle::Triangle(const Point3D &a, const Point3D &b, const Point3D &c):
    BaseTriangle(a,b,c)
{

}

Triangle::~Triangle()
{

}

QVector3D Triangle::barycentric(const QVector3D &testPoint) const
{
    QVector3D v0 = b - a;
    QVector3D v1 = c - a;
    QVector3D v2 = testPoint - a;

    float d00 = QVector3D::dotProduct(v0, v0);
    float d01 = QVector3D::dotProduct(v0, v1);
    float d11 = QVector3D::dotProduct(v1,v1);
    float d20 = QVector3D::dotProduct(v2, v0);
    float d21 = QVector3D::dotProduct(v2, v1);

    float denom = d00 * d11 - d01 * d01;

    if (denom < 1e-7f)
        return QVector3D();
    QVector3D res;
    res[1] = (d11 * d20 - d01 * d21) / denom;
    res[2] = (d00 * d21 - d01 * d20) / denom;
    res[0] = 1 - res[1] - res[2];
    return res;
}


bool Triangle::containsPoint(const Point3D &point) const
{
    Point3D bar = barycentric(point);
    return bar.x() >= 0 && bar.y() >= 0 && bar.z() >= 0 &&
            bar.x() <= 1 && bar.y() <= 1 && bar.z() <= 1;
}
