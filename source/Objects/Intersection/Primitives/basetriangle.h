#ifndef BASETRIANGLE_H
#define BASETRIANGLE_H
#include <QVector3D>

class BaseTriangle
{
public:
    BaseTriangle(const QVector3D &a, const QVector3D &b, const QVector3D &c);
    virtual ~BaseTriangle();

    virtual QVector3D getA() const;
    virtual void setA(const QVector3D &value);

    virtual QVector3D getB() const;
    virtual void setB(const QVector3D &value);

    virtual QVector3D getC() const;
    virtual void setC(const QVector3D &value);

    virtual QVector3D getNormal();

protected:
    QVector3D a,b,c;
};

#endif // BASETRIANGLE_H
