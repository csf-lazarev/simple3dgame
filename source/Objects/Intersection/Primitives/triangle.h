#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <QVector3D>
#include "basetriangle.h"

using Point3D = QVector3D;

class Triangle : public BaseTriangle
{
public:
    Triangle(const Point3D &a, const Point3D &b, const Point3D &c);
    virtual ~Triangle();
    bool containsPoint(const Point3D &point) const;
    QVector3D barycentric(const QVector3D &testPoint) const;
};

#endif // TRIANGLE_H
