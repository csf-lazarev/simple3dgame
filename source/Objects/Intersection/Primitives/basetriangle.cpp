#include "basetriangle.h"

BaseTriangle::BaseTriangle(const QVector3D &a, const QVector3D &b, const QVector3D &c) :
    a(a), b(b), c(c)
{

}

BaseTriangle::~BaseTriangle()
{

}

QVector3D BaseTriangle::getA() const
{
    return a;
}

void BaseTriangle::setA(const QVector3D &value)
{
    a = value;
}

QVector3D BaseTriangle::getB() const
{
    return b;
}

void BaseTriangle::setB(const QVector3D &value)
{
    b = value;
}

QVector3D BaseTriangle::getC() const
{
    return c;
}

void BaseTriangle::setC(const QVector3D &value)
{
    c = value;
}

QVector3D BaseTriangle::getNormal()
{
    return QVector3D::crossProduct(b - a, c - b).normalized();
}
