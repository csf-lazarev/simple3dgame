#include "indexedtriangle.h"

IndexedTriangle::IndexedTriangle(const Point3D &a, const Point3D &b, const Point3D &c, int index) :
    Triangle(a,b,c), index(index), normal(QVector3D::crossProduct(b-a, c-b).normalized())
{

}

IndexedTriangle::IndexedTriangle(const Point3D &a, const Point3D &b, const Point3D &c, int index,  QVector3D normal) :
    Triangle(a, b, c), index(index), normal(normal)
{

}

int IndexedTriangle::getIndex() const
{
    return index;
}

void IndexedTriangle::setIndex(int value)
{
    index = value;
}

QVector3D IndexedTriangle::getNormal() const
{
    return normal;
}
