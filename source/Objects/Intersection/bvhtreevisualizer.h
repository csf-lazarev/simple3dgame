#ifndef BVHTREEVISUALIZER_H
#define BVHTREEVISUALIZER_H

#include "aabb.h"
#include <source/Objects/Intersection/globalbvhtreenode.h>
#include <source/Objects/Intersection/globalbvhtreeleafnode.h>
#include "ibvhtreevisitor.h"

#include <source/Render/RenderHelpers/simplerenderhelper.h>



class BVHTreeVisualizer : IBVHTreeVisitor
{
public:
    BVHTreeVisualizer();
    void visit(BVHTreeNode *node) const override;
    void visit(BVHTreeLeafNode *node) const override;
private:
    void drawAABB(AABB *aabb) const;
private:
    SimpleRenderHelper *renderHelper;
};

#endif // BVHTREEVISUALIZER_H
