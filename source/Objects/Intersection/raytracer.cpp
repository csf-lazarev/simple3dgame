#include "raytracer.h"


RayTracer::RayTracer(BVHTree *globalTree, RayTracingMode defaultMode) : globalTree(globalTree),
    defaultMode(defaultMode), resultStore(defaultMode)
{

}

const QVector<RaycastResult *> &RayTracer::trace(const Ray &ray, bool *status)
{
    bool intercest = false;
    QMap<int, GlobalBVHTreeItem*> itemMap;
    globalTree->traceRay(ray, itemMap);

    resultStore.resetStore();
    resultStore.setRay(ray);

    for (GlobalBVHTreeItem* item : itemMap.values()){

        QVector<RaycastResult *> localStore;

        QMatrix4x4 inverseTransform = item->getObjectTransform()->getTransformMatrix().inverted();
        Ray *transRay = ray.getTransformed(inverseTransform);
        intercest |= item->getObjectTree()->traceRay(*transRay, localStore);
        resultStore.setRay(*transRay);
        for (RaycastResult *res : localStore){
            if (resultStore.addIfMatch(res))
                res->setItem(item);
        }
    }
    if (status)
        *status = intercest;
    return resultStore.getStore();
}

const RaycastResultStore &RayTracer::getResultStore()
{
    return resultStore;
}

