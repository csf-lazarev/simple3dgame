#include "bvhtreevisualizer.h"

BVHTreeVisualizer::BVHTreeVisualizer()
{

}

void BVHTreeVisualizer::visit(BVHTreeNode *node) const
{
    drawAABB(node->getVolumeBound());
}

void BVHTreeVisualizer::visit(BVHTreeLeafNode *node) const
{
    drawAABB(node->getVolumeBound());
}

void BVHTreeVisualizer::drawAABB(AABB *aabb) const
{
    Q_ASSERT(aabb);
    Point3D minPoint = aabb->getMinPoint();
    Point3D maxPoint = aabb->getMaxPoint();
    QVector3D hExt = aabb->getHalfExtents();
    Point3D highAdvPoint = Point3D(minPoint.x(),
                                   minPoint.y() + 2 * hExt.y(), minPoint.z());

    Point3D lowAdvPoint = Point3D(maxPoint.x(),
                                  maxPoint.y() - 2 * hExt.y(), maxPoint.z());

    glBegin(GL_LINES);
        glVertex3f(minPoint.x(), minPoint.y(), minPoint.z());
        glVertex3f(minPoint.x() + 2 * hExt.x(), minPoint.y(), minPoint.z());
    glEnd();

    glBegin(GL_LINES);
        glVertex3f(minPoint.x(), minPoint.y(), minPoint.z());
        glVertex3f(minPoint.x(), minPoint.y() + 2 * hExt.y(), minPoint.z());
    glEnd();

    glBegin(GL_LINES);
        glVertex3f(minPoint.x(), minPoint.y(), minPoint.z());
        glVertex3f(minPoint.x(), minPoint.y(), minPoint.z() + 2 * hExt.z());
    glEnd();

    glBegin(GL_LINES);
        glVertex3f(maxPoint.x(), maxPoint.y(), maxPoint.z());
        glVertex3f(maxPoint.x() - 2 * hExt.x(), maxPoint.y(), maxPoint.z());
    glEnd();

    glBegin(GL_LINES);
        glVertex3f(maxPoint.x(), maxPoint.y(), maxPoint.z());
        glVertex3f(maxPoint.x(), maxPoint.y() - 2 * hExt.y(), maxPoint.z());
    glEnd();

    glBegin(GL_LINES);
        glVertex3f(maxPoint.x(), maxPoint.y(), maxPoint.z());
        glVertex3f(maxPoint.x(), maxPoint.y(), maxPoint.z() - 2 * hExt.z());
    glEnd();


    glBegin(GL_LINES);
        glVertex3f(highAdvPoint.x(), highAdvPoint.y(), highAdvPoint.z());
        glVertex3f(highAdvPoint.x() + 2 * hExt.x(), highAdvPoint.y(), highAdvPoint.z());
    glEnd();

    glBegin(GL_LINES);
        glVertex3f(highAdvPoint.x(), highAdvPoint.y(), highAdvPoint.z());
        glVertex3f(highAdvPoint.x(), highAdvPoint.y(), highAdvPoint.z() + 2 * hExt.z());
    glEnd();

    glBegin(GL_LINES);
        glVertex3f(lowAdvPoint.x(), lowAdvPoint.y(), lowAdvPoint.z());
        glVertex3f(lowAdvPoint.x() - 2 * hExt.x(), lowAdvPoint.y(), lowAdvPoint.z());
    glEnd();

    glBegin(GL_LINES);
        glVertex3f(lowAdvPoint.x(), lowAdvPoint.y(), lowAdvPoint.z());
        glVertex3f(lowAdvPoint.x(), lowAdvPoint.y(), lowAdvPoint.z() - 2 * hExt.z());
    glEnd();

    glBegin(GL_LINES);
        glVertex3f(minPoint.x() + 2 * hExt.x(), minPoint.y(), minPoint.z());
        glVertex3f(maxPoint.x(), maxPoint.y(), maxPoint.z() - 2 * hExt.z());
    glEnd();

    glBegin(GL_LINES);
        glVertex3f(minPoint.x(), minPoint.y(), minPoint.z() + 2 * hExt.z());
        glVertex3f(maxPoint.x() - 2 * hExt.x(), maxPoint.y(), maxPoint.z());
    glEnd();

}
