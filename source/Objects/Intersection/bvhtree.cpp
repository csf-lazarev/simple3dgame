#include "bvhtree.h"

BVHTree::BVHTree(IBVHTreeNode *root) : root(root)
{

}

BVHTree::~BVHTree()
{
    delete root;
}

bool BVHTree::traceRay(const Ray &ray, QMap<int, GlobalBVHTreeItem*> &itemStore)
{
    return root->checkIntersection(ray, itemStore);
}

void BVHTree::addItem(GlobalBVHTreeItem *item)
{
    root->insertItem(item);
}

void BVHTree::acsept(IBVHTreeVisitor *visitor)
{
    root->acsept(visitor);
}
