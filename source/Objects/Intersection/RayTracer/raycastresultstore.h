#ifndef RAYCASTRESULTSTORE_H
#define RAYCASTRESULTSTORE_H

#include "raytracingmode.h"

#include <source/Objects/Intersection/ray.h>
#include <source/Objects/Intersection/raycastresult.h>



class RaycastResultStore
{
public:
    RaycastResultStore(RayTracingMode mode, Ray ray);
    explicit RaycastResultStore(RayTracingMode mode);

    bool addIfMatch(RaycastResult * result);

    Ray getRay() const;
    void setRay(const Ray &value);

    const QVector<RaycastResult *> &getStore() const;

    void resetStore();

private:
    void rewrite(RaycastResult * result);
    void add(RaycastResult * result);
    bool match(RaycastResult * result, bool &isSingle);
    bool compare(RaycastResult * result);

private:
    RayTracingMode mode;
    Ray ray;
    QVector<RaycastResult *> store;
};

#endif // RAYCASTRESULTSTORE_H
