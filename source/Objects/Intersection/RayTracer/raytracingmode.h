#ifndef RAYTRACINGMODE_H
#define RAYTRACINGMODE_H


class RayTracingMode
{
public:

    enum class DetectMode{
        NEAREST, FAREST, ALL
    };

    enum class FaceMode{
        FRONT, BACK, FRONT_AND_BACK
    };

    RayTracingMode(DetectMode detectMode, FaceMode faceMode);
    DetectMode getDetectMode() const;
    void setDetectMode(const DetectMode &value);

    FaceMode getFaceMode() const;
    void setFaceMode(const FaceMode &value);

private:
    DetectMode detectMode;
    FaceMode faceMode;
};

#endif // RAYTRACINGMODE_H
