#include "raycastresultstore.h"

RaycastResultStore::RaycastResultStore(RayTracingMode mode, Ray ray) : mode(mode), ray(ray)
{

}

RaycastResultStore::RaycastResultStore(RayTracingMode mode) : mode(mode), ray(QVector3D(0,0,0),
                                                                              QVector3D(0,0,0))
{

}

bool RaycastResultStore::addIfMatch(RaycastResult *result)
{
    bool isSingle = false;
    bool isMatch = match(result, isSingle);
    if (isMatch)
        isSingle? rewrite(result) : add(result);
    return isMatch;
}

void RaycastResultStore::rewrite(RaycastResult *result)
{
    store.clear();
    store.append(result);
}

void RaycastResultStore::add(RaycastResult *result)
{
    store.append(result);
}

bool RaycastResultStore::match(RaycastResult *result, bool &isSingle)
{
    isSingle = mode.getDetectMode() != RayTracingMode::DetectMode::ALL;
    GTriangle *tr = result->getTriangle();
    bool matchFace = true;
    if (mode.getFaceMode() == RayTracingMode::FaceMode::FRONT)
        matchFace =  QVector3D::dotProduct(tr->getNormal(), ray.getDirection()) < 1e-8f;
    else if (mode.getFaceMode() == RayTracingMode::FaceMode::BACK)
        matchFace =  QVector3D::dotProduct(tr->getNormal(), ray.getDirection()) > 1e-8f;

    if (!matchFace)
        return false;

    if (store.isEmpty() || !isSingle)
        return true;

    RaycastResult *priority = store[0];

    if (mode.getDetectMode() == RayTracingMode::DetectMode::NEAREST){
        return  result->getRayDistance() < priority->getRayDistance();
    }
    if (mode.getDetectMode() == RayTracingMode::DetectMode::FAREST) {
        return result->getRayDistance() > priority->getRayDistance();
    }

}

const QVector<RaycastResult *> &RaycastResultStore::getStore() const
{
    return store;
}

void RaycastResultStore::resetStore()
{
    while (!store.isEmpty()) {
        RaycastResult *r = store.takeFirst();
        delete r;
    }
}

Ray RaycastResultStore::getRay() const
{
    return ray;
}

void RaycastResultStore::setRay(const Ray &value)
{
    ray = value;
}
