#include "raytracingmode.h"

RayTracingMode::RayTracingMode(DetectMode detectMode, FaceMode faceMode) : detectMode(detectMode), faceMode(faceMode)
{

}

RayTracingMode::DetectMode RayTracingMode::getDetectMode() const
{
    return detectMode;
}

void RayTracingMode::setDetectMode(const DetectMode &value)
{
    detectMode = value;
}

RayTracingMode::FaceMode RayTracingMode::getFaceMode() const
{
    return faceMode;
}

void RayTracingMode::setFaceMode(const FaceMode &value)
{
    faceMode = value;
}
