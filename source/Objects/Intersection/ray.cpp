#include "ray.h"

Ray::Ray(const QVector3D &origin, const QVector3D &direction) :
    origin(origin), direction(direction)
{

}

QVector3D Ray::getOrigin() const
{
    return origin;
}

QVector3D Ray::getDirection() const
{
    return direction;
}

QVector3D Ray::getNormalizedDirection() const
{
    return direction.normalized();
}

Ray *Ray::getTransformed(const QMatrix4x4 &transform) const
{
    QVector4D orig(origin, 1.0);
    QVector4D dir(direction, 0.f);
    orig = transform * orig;
    dir = transform * dir;
    return new Ray(orig.toVector3D(), dir.toVector3D());
}

bool Ray::pointInRay(const QVector3D &point) const
{
    return distanceToPoint(point) >= 0;
}

float Ray::distanceToPoint(const QVector3D &point) const
{
    QVector3D dir = point - origin;
    if (QVector3D::dotProduct(dir.normalized(), direction) == 1.f)
        return dir.length();
    else return -1;
}
