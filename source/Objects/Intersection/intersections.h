#ifndef INTERSECTIONS_H
#define INTERSECTIONS_H

#include "aabb.h"
#include "plane.h"

#include <source/Objects/Intersection/Primitives/triangle.h>

#include <QtMath>
namespace intersections {

    float raycast(const AABB &aabb, const Ray &ray);
    float raycast(const Plane &plane, const Ray &ray);
    float raycast(const Triangle &triangle, const Ray &ray);
    float raycast2(const Triangle &triangle, const Ray &ray);
    float raycast(const QVector3D &point, const Ray &ray);

    bool intersect(const AABB &aabb, const Ray &ray);

    int triangleAndPlane(const Plane &plane, const Triangle* triangle);

    bool intersect(const AABB &first, const AABB &second);
    bool intersect(const Triangle & triangle, const Ray &ray);


}

#endif // INTERSECTIONS_H
