#include "globalbvhtreeitem.h"

int GlobalBVHTreeItem::idCounter = 1;

GlobalBVHTreeItem::GlobalBVHTreeItem(Transform *transform, ObjectBVHTree *objectTree) :
    id(idCounter++),objectTransform(transform), objectTree(objectTree), objectAABB(objectTree->getVolumeBound()->transformed(*transform))
{

}

GlobalBVHTreeItem::~GlobalBVHTreeItem()
{

}

AABB *GlobalBVHTreeItem::getObjectAABB() const
{
    return objectAABB;
}

Transform *GlobalBVHTreeItem::getObjectTransform() const
{
    return objectTransform;
}

ObjectBVHTree *GlobalBVHTreeItem::getObjectTree() const
{
    return objectTree;
}

int GlobalBVHTreeItem::getId() const
{
    return id;
}
