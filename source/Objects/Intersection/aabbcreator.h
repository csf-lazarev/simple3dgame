#ifndef AABBCREATOR_H
#define AABBCREATOR_H

#include "aabb.h"




class AABBCreator
{
public:
    AABBCreator();

    void addPoint(const Point3D &point);
    void addPoint(const QVector4D &point);
    void addTriangle(const Triangle &triangle);
    void reset();
    AABB *getAABB() const;
    bool created() const;
private:
    Point3D minPoint;
    Point3D maxPoint;
    bool create = false;
};

#endif // AABBCREATOR_H
