#include "globalbvhtreenode.h"

BVHTreeNode::BVHTreeNode(AABB *volumeBound) : IBVHTreeNode(volumeBound), leftNode(nullptr), rightNode(nullptr)
{

}

BVHTreeNode::~BVHTreeNode()
{
    delete leftNode;
    delete rightNode;
}

void BVHTreeNode::insertItem(GlobalBVHTreeItem *item)
{
    if (intersections::intersect(*volumeBound, *item->getObjectAABB())){
        leftNode->insertItem(item);
        rightNode->insertItem(item);
    }
}

bool BVHTreeNode::checkIntersection(const Ray &ray, QMap<int, GlobalBVHTreeItem*> &itemStore)
{
    bool intersect = intersections::intersect(*volumeBound, ray);

    if (!intersect)
        return false;

    intersect = leftNode->checkIntersection(ray, itemStore);
    return rightNode->checkIntersection(ray, itemStore) || intersect;
}

void BVHTreeNode::acsept(IBVHTreeVisitor *visitor)
{
   visitor->visit(this);
   leftNode->acsept(visitor);
   rightNode->acsept(visitor);
}

IBVHTreeNode *BVHTreeNode::getLeftNode() const
{
    return leftNode;
}

void BVHTreeNode::setLeftNode(IBVHTreeNode *value)
{
    delete leftNode;
    leftNode = value;
}

IBVHTreeNode *BVHTreeNode::getRightNode() const
{
    return rightNode;
}

void BVHTreeNode::setRightNode(IBVHTreeNode *value)
{
    delete rightNode;
    rightNode = value;
}

