#ifndef BVHTREENODE_H
#define BVHTREENODE_H

#include "iglobalbvhtreenode.h"
#include "globalbvhtreeitem.h"
#include <source/Objects/Intersection/intersections.h>

class BVHTreeNode : public IBVHTreeNode
{
public:
    BVHTreeNode(AABB *volumeBound);
    ~BVHTreeNode() override;
    virtual void insertItem(GlobalBVHTreeItem *item) override;
    virtual bool checkIntersection(const Ray &ray, QMap<int, GlobalBVHTreeItem *> &itemStore) override;
    void acsept(IBVHTreeVisitor *visitor) override;



    IBVHTreeNode *getLeftNode() const;
    void setLeftNode(IBVHTreeNode *value);

    IBVHTreeNode *getRightNode() const;
    void setRightNode(IBVHTreeNode *value);

protected:
    IBVHTreeNode *leftNode;
    IBVHTreeNode *rightNode;
};

#endif // BVHTREENODE_H
