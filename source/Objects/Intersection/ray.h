#ifndef RAY_H
#define RAY_H

#include <QMatrix4x4>
#include <QVector3D>
#include <QVector4D>


class Ray
{
public:
    Ray(const QVector3D &origin, const QVector3D &direction);

    QVector3D getOrigin() const;


    QVector3D getDirection() const;
    QVector3D getNormalizedDirection() const;
    Ray *getTransformed(const QMatrix4x4 &transform) const;

    bool pointInRay(const QVector3D &point) const;

    float distanceToPoint(const QVector3D &point) const;

private:
    QVector3D origin;
    QVector3D direction;
};

#endif // RAY_H
