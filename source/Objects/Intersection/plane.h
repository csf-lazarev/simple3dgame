#ifndef PLANE_H
#define PLANE_H

#include <QVector3D>

#include <source/Objects/Intersection/Primitives/triangle.h>

class Plane
{
public:
    Plane(const QVector3D& point1, const QVector3D &point2, const QVector3D &point3);
    explicit Plane(const Triangle &triangle);

    Plane(const QVector3D &normal, float distance);

    QVector3D getNormal() const;
    bool isValid();

    float getDistance() const;

    float distanceToPoint(const Point3D &point) const;
    float signedDistanceToPoint(const Point3D &point) const;



private:
    QVector3D normal;
    float distance;
};

#endif // PLANE_H
