#include "iglobalbvhtreenode.h"

IBVHTreeNode::IBVHTreeNode(AABB *volumeBound) : volumeBound(volumeBound)
{

}

IBVHTreeNode::~IBVHTreeNode()
{

}

AABB *IBVHTreeNode::getVolumeBound() const
{
    return volumeBound;
}


