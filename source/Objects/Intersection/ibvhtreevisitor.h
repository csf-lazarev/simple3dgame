#ifndef IBVHTREEVISITOR_H
#define IBVHTREEVISITOR_H

class BVHTreeNode;
class BVHTreeLeafNode;

class IBVHTreeVisitor
{
public:
    IBVHTreeVisitor();
    virtual ~IBVHTreeVisitor();
    virtual void visit(BVHTreeNode *node) const = 0;
    virtual void visit(BVHTreeLeafNode *node) const = 0;
};

#endif // IBVHTREEVISITOR_H
