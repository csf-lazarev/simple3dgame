#include "sceneobject.h"

SceneObject::SceneObject(TGContainer *bufferContainer, Transform3D *transform) :
    geometryBufferContainer(bufferContainer), transform(transform)
{

}

Transform *SceneObject::getTransform() const
{
    return transform;
}



GeomertyBufferContainer<TriangulatedGeometryBuffer> *SceneObject::getGeometryBufferContainer() const
{
    return geometryBufferContainer;
}
