#ifndef OBJECTMANAGER_H
#define OBJECTMANAGER_H
#include "inoselfrenderingobject.h"
#include "iselfrenderingobject.h"
#include <source/typeid.h>
#include <QVector>

class ObjectManager
{
public:
    ObjectManager();
    ~ObjectManager();

    template<class ObjectType>
    void getObjectList(QVector<ObjectType*> &list) const{
        if (SRTypes.contains(getTypeID<ObjectType>()))
            checkSRObjects<ObjectType>(list);
        else
            checkNoSRObjects<ObjectType>(list);
    }

    void registerObject(BaseRenderingObject* object);
    void registerObject(ISelfRenderingObject* object);
    void registerObject(INoSelfRenderingObject* object);

    const QVector<ISelfRenderingObject *> getSelfRenderingObjects() const;
    const QVector<INoSelfRenderingObject *> &getNoSelfRenderingObjects() const;

private:
    template<class ObjectType>
    void checkSRObjects(QVector<ObjectType*> &list) const{
        for (ISelfRenderingObject* object : selfRenderingObjects){
            ObjectType* casted = dynamic_cast<ObjectType*>(object);
            if (casted)
                list.append(casted);
        }
    }

    template<class ObjectType>
    void checkNoSRObjects(QVector<ObjectType*> &list) const{
        for (INoSelfRenderingObject* object : noSelfRenderingObjects){
            ObjectType* casted = dynamic_cast<ObjectType*>(object);
            if (casted)
                list.append(casted);
        }
    }

private:
    static QVector<TypeID> SRTypes;
    QVector<ISelfRenderingObject *> selfRenderingObjects;
    QVector<INoSelfRenderingObject *> noSelfRenderingObjects;
};

#endif // OBJECTMANAGER_H
