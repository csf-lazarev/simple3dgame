#ifndef PLANEGRID_H
#define PLANEGRID_H

#include "iselfrenderingobject.h"
#include <source/Scene/sceneenvironment.h>
#include <source/Render/RenderHelpers/simplerenderhelper.h>


class PlaneGrid : public ISelfRenderingObject
{
public:
    PlaneGrid(SimpleRenderHelper *renderHelper, const QVector3D& center,
              float width, float height, unsigned rows, unsigned columns);
    void render() override;

private:
    void drawGrid();
private:
    SimpleRenderHelper *renderHelper;

    QVector3D center;
    float width;
    float height;
    unsigned rows;
    unsigned columns;
};

#endif // PLANEGRID_H
