#ifndef MATERIALOBJECT_H
#define MATERIALOBJECT_H
#include <source/Objects/Material/material.h>
#include <source/Objects/sceneobject.h>

class MaterialObject : public SceneObject
{
public:
    MaterialObject(TGContainer * bufferContainer, Transform3D *transform, Material *material);

    Material *getMaterial() const;
    void setMaterial(Material *value);

private:
    Material *material;
};

#endif // MATERIALOBJECT_H
