#include "baserenderingobject.h"

BaseRenderingObject::BaseRenderingObject()
{

}

BaseRenderingObject::~BaseRenderingObject()
{

}

TypeID BaseRenderingObject::getObjectType()
{
    return getTypeID(this);
}

size_t BaseRenderingObject::getObjectTypeHash()
{
    return getTypeHash(this);
}
