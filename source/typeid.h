#ifndef TYPEID_H
#define TYPEID_H
#include <crtdefs.h>
#include <typeindex>

using TypeID = std::type_index;
template<typename T>
inline TypeID getTypeID(){
    return std::type_index(typeid (T));
}

template<typename T>
inline size_t getTypeHash(){
    return getTypeID<T>().hash_code();
}

template<typename T>
inline size_t getTypeHash(T *o){
    return getTypeID<T>(o).hash_code();
}

template<typename T>
inline TypeID getTypeID(T *o){
    return std::type_index(typeid (*o));
}
#endif // TYPEID_H
