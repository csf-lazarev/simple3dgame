#ifndef DIRECTEDLIGHT_H
#define DIRECTEDLIGHT_H
#include "baselight.h"

class DirectedLight : public BaseLight
{
public:
    DirectedLight(const QVector3D &color, const QVector3D &direction, float power);

    QVector3D getDirection() const;
    void setDirection(const QVector3D &value);

    float getPower() const;
    void setPower(float value);

private:
    QVector3D direction;
    float power;
};

#endif // DIRECTEDLIGHT_H
