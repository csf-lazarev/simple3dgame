#ifndef LIGHT_H
#define LIGHT_H

#include <types.h>
#include "baselight.h"
class PointLight : public BaseLight
{
public:
    PointLight(const vector3& color, const vector3 &position,
          float constant, float linear, float quadratic);

    vector3 getPosition() const;

    void setPosition(const vector3 &value);


    float getConstant() const;

    float getLinear() const;

    float getQuadratic() const;

private:
    vector3 position;

    float constant;
    float linear;
    float quadratic;
};

#endif // LIGHT_H
