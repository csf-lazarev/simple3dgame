#ifndef LIGHTINGMANAGER_H
#define LIGHTINGMANAGER_H

#include "pointlight.h"
#include "directedlight.h"
#include <QVector>



class LightingManager
{
public:
    LightingManager();
    void registerLight(PointLight *light);
    const QVector<PointLight*> getLights() const;

    DirectedLight *getDirLight() const;
    void setDirLight(DirectedLight *value);

private:
    QVector<PointLight*> pointLights;
    DirectedLight *dirLight;
};

#endif // LIGHTINGMANAGER_H
