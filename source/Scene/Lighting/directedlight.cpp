#include "directedlight.h"

DirectedLight::DirectedLight(const QVector3D &color, const QVector3D &direction, float power) :
    BaseLight(color), direction(direction), power(power)
{

}

QVector3D DirectedLight::getDirection() const
{
    return direction;
}

void DirectedLight::setDirection(const QVector3D &value)
{
    direction = value;
}

float DirectedLight::getPower() const
{
    return power;
}

void DirectedLight::setPower(float value)
{
    power = value;
}
