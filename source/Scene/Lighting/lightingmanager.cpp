#include "lightingmanager.h"

LightingManager::LightingManager() : dirLight(new DirectedLight({0,0,0}, {0,0,0}, 0))
{

}

void LightingManager::registerLight(PointLight *light)
{
    pointLights.append(light);
}

const QVector<PointLight *> LightingManager::getLights() const
{
    return pointLights;
}

DirectedLight *LightingManager::getDirLight() const
{
    return dirLight;
}

void LightingManager::setDirLight(DirectedLight *value)
{
    dirLight = value;
}
