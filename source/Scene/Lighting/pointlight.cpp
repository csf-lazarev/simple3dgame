#include "pointlight.h"

PointLight::PointLight(const vector3 &color, const vector3 &position, float constant, float linear, float quadratic) :
  BaseLight(color) ,position(position),
  constant(constant), linear(linear), quadratic(quadratic)
{

}


vector3 PointLight::getPosition() const
{
    return position;
}

void PointLight::setPosition(const vector3 &value)
{
    position = value;
}

float PointLight::getConstant() const
{
    return constant;
}

float PointLight::getLinear() const
{
    return linear;
}

float PointLight::getQuadratic() const
{
    return quadratic;
}

