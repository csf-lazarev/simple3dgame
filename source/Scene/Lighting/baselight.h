#ifndef BASELIGHT_H
#define BASELIGHT_H
#include <QVector3D>

class BaseLight
{
public:
    BaseLight(const QVector3D &color);
    virtual ~BaseLight();
    QVector3D getColor() const;
    void setColor(const QVector3D &value);

private:
    QVector3D color;
};

#endif // BASELIGHT_H
