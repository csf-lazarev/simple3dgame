#include "baselight.h"

BaseLight::BaseLight(const QVector3D &color) : color(color)
{

}

BaseLight::~BaseLight()
{

}

QVector3D BaseLight::getColor() const
{
    return color;
}

void BaseLight::setColor(const QVector3D &value)
{
    color = value;
}
