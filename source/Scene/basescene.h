#ifndef BASESCENE_H
#define BASESCENE_H


class BaseScene
{
public:
    BaseScene();
    virtual ~BaseScene();

    virtual void update() = 0;
    virtual void render() = 0;
};

#endif // BASESCENE_H
