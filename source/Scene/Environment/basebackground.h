#ifndef BASEBACKGROUND_H
#define BASEBACKGROUND_H
#include <source/Objects/iselfrenderingobject.h>

class BaseBackground : public ISelfRenderingObject
{
public:
    BaseBackground();
    virtual void render() override = 0;
};

#endif // BASEBACKGROUND_H
