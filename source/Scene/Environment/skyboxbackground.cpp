#include "skyboxbackground.h"
#include <source/utils/utilits.h>

SkyboxBackground::SkyboxBackground(const OpenGLTexture &skyboxTexture, QOpenGLBuffer *boxBuffer, QOpenGLShaderProgram *skyBoxShader, const ExtendedRenderHelper &rHelp) :
    skyboxTexture(skyboxTexture), boxBuffer(boxBuffer), skyBoxShader(skyBoxShader), rHelp(rHelp)
{

}

void SkyboxBackground::render()
{
    glDisable(GL_CULL_FACE);
    glDepthFunc(GL_LEQUAL);


    auto sEvr = rHelp.getSceneEnvironment();
    ICamera *cam = sEvr.getActiveCamera();
    QMatrix4x4 view = matrices::fromMatrix3x3(matrices::getLeftMinor(cam->getViewMatrix()));
    //QMatrix4x4 view = cam->getViewMatrix();
    QMatrix4x4 proj = cam->getProjectionMatrix();

    skyBoxShader->bind();
    skyBoxShader->setUniformValue("view", view);
    skyBoxShader->setUniformValue("projection", proj);
    skyBoxShader->enableAttributeArray("aPosition");
    boxBuffer->bind();
    skyBoxShader->setAttributeBuffer("aPosition", GL_FLOAT, 0, 3);

    auto glFunc = getOpenGLFunctions();
    skyBoxShader->setUniformValue("skybox", 0);
    glFunc.glActiveTexture(GL_TEXTURE0);
    skyboxTexture.bind();
    glDrawArrays(GL_TRIANGLES, 0, 36);
    boxBuffer->release();
    skyboxTexture.release();
    skyBoxShader->release();
    glDepthFunc(GL_LESS);
    glEnable(GL_CULL_FACE);
}
