#ifndef SKYBOXBACKGROUND_H
#define SKYBOXBACKGROUND_H
#include "basebackground.h"

#include <QOpenGLTexture>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>

#include <source/Render/RenderHelpers/extendedrenderhelper.h>
#include <source/OpenGL/opengltexture.h>
#include <source/utils/GL.h>

class SkyboxBackground : public BaseBackground
{
public:
    SkyboxBackground(const OpenGLTexture &skyboxTexture, QOpenGLBuffer *boxBuffer, QOpenGLShaderProgram *skyBoxShader, const ExtendedRenderHelper &rHelp);
    void render() override;

private:
    const OpenGLTexture &skyboxTexture;
    QOpenGLBuffer *boxBuffer;
    QOpenGLShaderProgram *skyBoxShader;
    const ExtendedRenderHelper &rHelp;
};

#endif // SKYBOXBACKGROUND_H
