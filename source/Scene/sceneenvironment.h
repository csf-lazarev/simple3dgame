#ifndef SCENECOMPONENT_H
#define SCENECOMPONENT_H

#include <source/Scene/scene.h>

class SceneEnvironment
{
public:
    explicit SceneEnvironment(Scene *scene);

    ICamera *getActiveCamera() const;
    ObjectManager *getObjectManager() const;
    const PointLight &getLight() const;
    const QVector<PointLight *> getLights() const;
    const LightingManager &getLightingManager() const;

private:
    Scene *scene;
};

#endif // SCENECOMPONENT_H
