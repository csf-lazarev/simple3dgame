#ifndef SCENE_H
#define SCENE_H

#include "basescene.h"
#include "debugscene.h"
#include <source/Render/Camera/icamera.h>
#include <source/Render/Camera/idrivencamera.h>
#include <source/Objects/objectmanager.h>
#include <source/Scene/Lighting/pointlight.h>
#include <source/Scene/Lighting/lightingmanager.h>
#include <source/Objects/Intersection/raytracer.h>
#include <source/InputSystem/keyeventregister.h>
#include <source/Scene/Environment/basebackground.h>


class Scene : public BaseScene
{
public:
    Scene(IDrivenCamera* camera, RayTracer *rayTracer,KeyEventRegister *keyRegister,  DebugScene *dScene = nullptr);

    void render() override;
    void update() override;

    IDrivenCamera *getActiveDrivenCamera() const;
    ICamera *getActiveCamera() const;
    ObjectManager *getObjectManager() const;
    LightingManager *getLightingManager() const;

    RayTracer *getRayTracer() const;

    DebugScene *getDScene() const;
    void setDScene(DebugScene *value);

    BaseBackground *getBackround() const;
    void setBackround(BaseBackground *value);

private:
    void initObjects();
    IDrivenCamera *activeCamera;
    ObjectManager *objectManager;
    LightingManager *lightingManager;
    RayTracer *rayTracer;
    KeyEventRegister *keyRegister;
    DebugScene *dScene;
    BaseBackground *backround;
};

#endif // SCENE_H
