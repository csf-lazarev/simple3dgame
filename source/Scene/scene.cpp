#include "scene.h"

Scene::Scene(IDrivenCamera *camera, RayTracer *rayTracer, KeyEventRegister *keyRegister, DebugScene *dScene) : BaseScene(),activeCamera(camera),
    objectManager(new ObjectManager()), lightingManager(new LightingManager()), rayTracer(rayTracer),keyRegister(keyRegister) ,dScene(dScene),
    backround(nullptr)
{

}

void Scene::render()
{
    for (ISelfRenderingObject *obj : objectManager->getSelfRenderingObjects())
        obj->render();
    if (backround)
        backround->render();

    if (dScene)
        dScene->render();
}

void Scene::update()
{

}

IDrivenCamera *Scene::getActiveDrivenCamera() const
{
    return activeCamera;
}

ICamera *Scene::getActiveCamera() const
{
    return activeCamera;
}

ObjectManager *Scene::getObjectManager() const
{
    return objectManager;
}


void Scene::initObjects()
{

}

BaseBackground *Scene::getBackround() const
{
    return backround;
}

void Scene::setBackround(BaseBackground *value)
{
    backround = value;
}

DebugScene *Scene::getDScene() const
{
    return dScene;
}

void Scene::setDScene(DebugScene *value)
{
    dScene = value;
}

RayTracer *Scene::getRayTracer() const
{
    return rayTracer;
}

LightingManager *Scene::getLightingManager() const
{
    return lightingManager;
}

