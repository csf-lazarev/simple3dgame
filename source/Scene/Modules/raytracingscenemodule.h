#ifndef RAYTRACINGSCENEMODULE_H
#define RAYTRACINGSCENEMODULE_H

#include "scenemodule.h"

#include <source/Objects/Intersection/raytracer.h>
#include <source/Render/RenderHelpers/raycastresultrenderer.h>
#include <QVector>


class RayTracingSceneModule : public SceneModule
{
public:
    RayTracingSceneModule(RayTracer *rayTracer, RaycastResultRenderer *raycastRenderer);

    void renderModule() const override;

private:
    RayTracer *rayTracer;
    RaycastResultRenderer *raycastRenderer;
};

#endif // RAYTRACINGSCENEMODULE_H
