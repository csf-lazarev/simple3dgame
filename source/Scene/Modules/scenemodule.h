#ifndef SCENEMODULE_H
#define SCENEMODULE_H


class SceneModule
{
public:
    SceneModule();
    virtual ~SceneModule();
    virtual void renderModule() const = 0;
};

#endif // SCENEMODULE_H
