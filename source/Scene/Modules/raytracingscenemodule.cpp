#include "raytracingscenemodule.h"

RayTracingSceneModule::RayTracingSceneModule(RayTracer *rayTracer, RaycastResultRenderer *raycastRenderer) :
    rayTracer(rayTracer) ,raycastRenderer(raycastRenderer)
{

}


void RayTracingSceneModule::renderModule() const
{
    const RaycastResultStore &store = rayTracer->getResultStore();
    foreach (RaycastResult *r, store.getStore())
        raycastRenderer->renderObject(*r);
}
