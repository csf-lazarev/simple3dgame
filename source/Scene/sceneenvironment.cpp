#include "sceneenvironment.h"


SceneEnvironment::SceneEnvironment(Scene *scene) : scene(scene)
{

}

ICamera *SceneEnvironment::getActiveCamera() const
{
    return scene->getActiveCamera();
}

ObjectManager *SceneEnvironment::getObjectManager() const
{
    return scene->getObjectManager();
}

const PointLight &SceneEnvironment::getLight() const
{
    return *scene->getLightingManager()->getLights().at(0);
}

const QVector<PointLight *> SceneEnvironment::getLights() const
{
    return scene->getLightingManager()->getLights();
}

const LightingManager &SceneEnvironment::getLightingManager() const
{
    return *scene->getLightingManager();
}
