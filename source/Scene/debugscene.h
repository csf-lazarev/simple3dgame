#ifndef DEBUGSCENE_H
#define DEBUGSCENE_H

#include "basescene.h"
#include <source/Scene/Modules/raytracingscenemodule.h>

class Scene;


class DebugScene : public BaseScene
{
public:
    DebugScene(RayTracingSceneModule *rayTracingModule);
    void render() override;
    void update() override;

    RayTracingSceneModule *getRayTracingModule() const;

private:
    Scene *linkedScene;
    RayTracingSceneModule *rayTracingModule;
};

#endif // DEBUGSCENE_H
