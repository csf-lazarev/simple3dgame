#include "debugscene.h"
#include "scene.h"
DebugScene::DebugScene(RayTracingSceneModule *rayTracingModule) : rayTracingModule(rayTracingModule)
{

}

void DebugScene::render()
{
    rayTracingModule->renderModule();
}

void DebugScene::update()
{
}

RayTracingSceneModule *DebugScene::getRayTracingModule() const
{
    return rayTracingModule;
}
