#include "geometryresourcemanager.h"

int GeometryResourceManager::indexGenerator = 1;

GeometryResourceManager::GeometryResourceManager()
{

}

GeomertyBufferContainer<TriangulatedGeometryBuffer> *GeometryResourceManager::registerBuffer(TriangulatedGeometryBuffer *buffer, QString bufferAlias)
{
    Q_ASSERT(!aliasesMap.contains(bufferAlias));

    GeomertyBufferContainer<TriangulatedGeometryBuffer> *tContainer
            = new GeomertyBufferContainer<TriangulatedGeometryBuffer>(buffer);
    int registrationIndex = genBufferIndex();
    tContainer->setBufferIndex(registrationIndex);
    aliasesMap.insert(bufferAlias, registrationIndex);
    tBuffersMap.insert(registrationIndex, tContainer);

    return tContainer;
}

GeomertyBufferContainer<GeometryBuffer> *GeometryResourceManager::registerBuffer(GeometryBuffer *buffer, QString bufferAlias)
{
    Q_ASSERT(!aliasesMap.contains(bufferAlias));

    GeomertyBufferContainer<GeometryBuffer> *gContainer
            = new GeomertyBufferContainer<GeometryBuffer>(buffer);
    int registrationIndex = genBufferIndex();
    gContainer->setBufferIndex(registrationIndex);
    aliasesMap.insert(bufferAlias, registrationIndex);
    geomBuffersMap.insert(registrationIndex, gContainer);

    return gContainer;
}

void GeometryResourceManager::loadOpenGLBufferFor(const GeomertyBufferContainer<TriangulatedGeometryBuffer> &bufContainer)
{
    int index = bufContainer.getBufferIndex();
    if (glBuffersMap.contains(index))
        return;
    OpenGLGeometryBuffer *glBuffer = OpenGLGeometryBufferCreator::createFor(*bufContainer.getBuffer());
    auto container = new GeomertyBufferContainer<OpenGLGeometryBuffer>(glBuffer);
    container->setBufferIndex(index);
    glBuffersMap.insert(index, container);
}

void GeometryResourceManager::loadOpenGLBufferFor(const QString &alias)
{
    loadOpenGLBufferFor(*getByAlias(alias));
}

GeomertyBufferContainer<OpenGLGeometryBuffer> *GeometryResourceManager::getOpenGLBuferFor(const GeomertyBufferContainer<TriangulatedGeometryBuffer> &bufContainer)
{
    int index = bufContainer.getBufferIndex();
    if (!glBuffersMap.contains(index))
        loadOpenGLBufferFor(bufContainer);
    return glBuffersMap.value(index);
}

GeomertyBufferContainer<OpenGLGeometryBuffer> *GeometryResourceManager::getOpenGLBuferFor(const QString &alias)
{
    return getOpenGLBuferFor(*getByAlias(alias));
}

GeomertyBufferContainer<TriangulatedGeometryBuffer> *GeometryResourceManager::getByAlias(const QString &alias)
{
    Q_ASSERT(aliasesMap.contains(alias));
    return tBuffersMap.value(aliasesMap.value(alias));
}

int GeometryResourceManager::genBufferIndex()
{
    return indexGenerator++;
}
