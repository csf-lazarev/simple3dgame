#ifndef GEOMERTYBUFFERCONTAINER_H
#define GEOMERTYBUFFERCONTAINER_H

template <class BufferType>
class GeomertyBufferContainer
{
public:
    explicit GeomertyBufferContainer<BufferType>(BufferType *buffer) :
    buffer(buffer)
    {

    }

    BufferType *operator->() const{
        return  buffer;
    }

    BufferType *getBuffer() const{
        return buffer;
    }

    int getBufferIndex() const {
        return bufferIndex;
    }

private:
    void setBufferIndex(int index){
        bufferIndex = index;
    }

private:
    friend class GeometryResourceManager;
    BufferType *buffer;
    int bufferIndex;
};

#endif // GEOMERTYBUFFERCONTAINER_H
