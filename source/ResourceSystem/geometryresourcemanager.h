#ifndef GEOMETRYRESOURCEMANAGER_H
#define GEOMETRYRESOURCEMANAGER_H
#include <QMap>

#include "geomertybuffercontainer.h"
#include <source/Buffer/geometrybuffer.h>
#include <source/Buffer/triangulatedgeometrybuffer.h>
#include <source/OpenGL/openglgeometrybuffer.h>
#include <source/Buffer/openglgeometrybuffercreator.h>

class GeometryResourceManager
{
public:
    GeometryResourceManager();

    GeomertyBufferContainer<TriangulatedGeometryBuffer> *registerBuffer(TriangulatedGeometryBuffer * buffer, QString bufferAlias);
    GeomertyBufferContainer<GeometryBuffer> *registerBuffer(GeometryBuffer * buffer, QString bufferAlias);

    void loadOpenGLBufferFor(const GeomertyBufferContainer<TriangulatedGeometryBuffer> &bufContainer);
    void loadOpenGLBufferFor(const QString &alias);
    GeomertyBufferContainer<OpenGLGeometryBuffer> *getOpenGLBuferFor(const GeomertyBufferContainer<TriangulatedGeometryBuffer> &bufContainer);
    GeomertyBufferContainer<OpenGLGeometryBuffer> *getOpenGLBuferFor(const QString &alias);

    GeomertyBufferContainer<TriangulatedGeometryBuffer> *getByAlias(const QString &alias);

private:
    int genBufferIndex();
private:
    QMap<int, GeomertyBufferContainer<TriangulatedGeometryBuffer> *> tBuffersMap;
    QMap<int, GeomertyBufferContainer<GeometryBuffer> *> geomBuffersMap;
    QMap<int, GeomertyBufferContainer<OpenGLGeometryBuffer> *> glBuffersMap;
    QMap<QString, int> aliasesMap;

    static int indexGenerator;
};

#endif // GEOMETRYRESOURCEMANAGER_H
