#include "utilits.h"

QMatrix3x3 matrices::getLeftMinor(const QMatrix4x4 &matrix)
{
   QVector3D data0 = matrix.column(0).toVector3D();
   QVector3D data1 = matrix.column(1).toVector3D();
   QVector3D data2 = matrix.column(2).toVector3D();

   float values[] = {data0[0], data1[0], data2[0],
                     data0[1], data1[1], data2[1],
                     data0[2], data1[2], data2[2]};

   return QMatrix3x3(values);
}

QMatrix3x3 matrices::inverse(const QMatrix3x3 &m)
{
    float det = m(0, 0) * (m(1, 1) * m(2, 2) - m(2, 1) * m(1, 2)) -
                 m(0, 1) * (m(1, 0) * m(2, 2) - m(1, 2) * m(2, 0)) +
                 m(0, 2) * (m(1, 0) * m(2, 1) - m(1, 1) * m(2, 0));

    float invdet = 1 / det;

    QMatrix3x3 minv; // inverse of matrix m
    minv(0, 0) = (m(1, 1) * m(2, 2) - m(2, 1) * m(1, 2)) * invdet;
    minv(0, 1) = (m(0, 2) * m(2, 1) - m(0, 1) * m(2, 2)) * invdet;
    minv(0, 2) = (m(0, 1) * m(1, 2) - m(0, 2) * m(1, 1)) * invdet;
    minv(1, 0) = (m(1, 2) * m(2, 0) - m(1, 0) * m(2, 2)) * invdet;
    minv(1, 1) = (m(0, 0) * m(2, 2) - m(0, 2) * m(2, 0)) * invdet;
    minv(1, 2) = (m(1, 0) * m(0, 2) - m(0, 0) * m(1, 2)) * invdet;
    minv(2, 0) = (m(1, 0) * m(2, 1) - m(2, 0) * m(1, 1)) * invdet;
    minv(2, 1) = (m(2, 0) * m(0, 1) - m(0, 0) * m(2, 1)) * invdet;
    minv(2, 2) = (m(0, 0) * m(1, 1) - m(1, 0) * m(0, 1)) * invdet;

    return minv;
}

QMatrix4x4 matrices::fromMatrix3x3(const QMatrix3x3 &m)
{
    return QMatrix4x4
            (m(0,0), m(0,1), m(0,2), 0,
             m(1,0), m(1,1), m(1,2), 0,
             m(2,0), m(2,1), m(2,2), 0,
             0,      0,      0,      1);
}
