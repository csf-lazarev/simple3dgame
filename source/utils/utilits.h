#ifndef UTILITS_H
#define UTILITS_H
#include <QMatrix4x4>

namespace matrices {
    QMatrix3x3 getLeftMinor(const QMatrix4x4 & matrix);
    QMatrix3x3 inverse(const QMatrix3x3 &matrix);
    QMatrix4x4 fromMatrix3x3(const QMatrix3x3 &m);
}
#endif // UTILITS_H
