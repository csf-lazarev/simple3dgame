#ifndef GL_H
#define GL_H
#include <QOpenGLContext>
#include <QOpenGLFunctions>
#include <QOpenGLExtraFunctions>

inline QOpenGLFunctions &getOpenGLFunctions(){
    return *QOpenGLContext::currentContext()->functions();
}

inline QOpenGLExtraFunctions &getOpenGLExtraFunctions(){
    return *QOpenGLContext::currentContext()->extraFunctions();
}


#endif // GL_H
