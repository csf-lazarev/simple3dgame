#ifndef GLBUFFER_H
#define GLBUFFER_H

#include <QOpenGLBuffer>

class GLBuffer
{
public:
    explicit GLBuffer(bool indexed);
    GLBuffer(QOpenGLBuffer *vbo,
             QOpenGLBuffer *ibo,
             size_t vertexCount,
             size_t indexCount);
    GLBuffer(const GLBuffer &buffer);
    ~GLBuffer();

    void createVBO();
    void createIBO();

    void allocateVBOMemory(const void *data, int size, size_t countVertices);
    void allocateIBOMemory(const void *data, int size, size_t countIndices);

    void bind();
    void release();
    void draw(GLenum mode);


    QOpenGLBuffer *getVbo() const;

    QOpenGLBuffer *getIbo() const;
    size_t getVertexCount() const;

    size_t getIndexCount() const;

private:
    void drawArray(GLenum mode);
    void drawIndexed(GLenum mode);
private:
    QOpenGLBuffer *vbo;
    QOpenGLBuffer *ibo;

    size_t vertexCount;
    size_t indexCount;

    bool isIndexed;
};

#endif // GLBUFFER_H
