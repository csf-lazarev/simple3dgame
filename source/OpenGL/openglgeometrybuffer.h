#ifndef OPENGLGEOMETRYBUFFER_H
#define OPENGLGEOMETRYBUFFER_H
#include <QOpenGLBuffer>

class OpenGLGeometryBuffer
{
public:
    OpenGLGeometryBuffer(QOpenGLBuffer * const vertexBuffer,
                         QOpenGLBuffer * const normalBuffer);

    OpenGLGeometryBuffer(QOpenGLBuffer * const vertexBuffer,
                         QOpenGLBuffer * const normalBuffer,
                         QOpenGLBuffer * const uvBuffer);


    QOpenGLBuffer *getVertexBuffer() const;

    QOpenGLBuffer *getNormalBuffer() const;

    QOpenGLBuffer *getUVBuffer() const;

    bool isUVAttached() const;

    void attachUVBuffer(QOpenGLBuffer *uvBuffer);

private:
    QOpenGLBuffer *vertexBuffer;
    QOpenGLBuffer *normalBuffer;
    QOpenGLBuffer *uvBuffer;

    bool uvAttached;
};

#endif // OPENGLGEOMETRYBUFFER_H
