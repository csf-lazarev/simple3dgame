#include "glbuffer.h"

GLBuffer::GLBuffer(bool indexed) : vbo(new QOpenGLBuffer(QOpenGLBuffer::Type::VertexBuffer)),
     ibo(indexed ? new QOpenGLBuffer(QOpenGLBuffer::Type::IndexBuffer) : nullptr), vertexCount(0), indexCount(0), isIndexed(indexed)
{

}


GLBuffer::GLBuffer(QOpenGLBuffer *vbo, QOpenGLBuffer *ibo, size_t vertexCount, size_t indexCount) : vbo(vbo), ibo(ibo),
    vertexCount(vertexCount), indexCount(indexCount), isIndexed(ibo != nullptr)
{

}

GLBuffer::GLBuffer(const GLBuffer &buffer) : GLBuffer(buffer.vbo, buffer.ibo, buffer.vertexCount, buffer.indexCount)
{

}

GLBuffer::~GLBuffer()
{
    delete vbo;
    delete ibo;
}

void GLBuffer::createVBO()
{
    vbo->create();
}

void GLBuffer::createIBO()
{
    if (isIndexed)
        ibo->create();
}

void GLBuffer::allocateVBOMemory(const void *data, int size, size_t countVertices)
{
    vbo->bind();
    vbo->allocate(data, size);
    vbo->release();
    vertexCount = countVertices;
}

void GLBuffer::allocateIBOMemory(const void *data, int size, size_t countIndices)
{
    Q_ASSERT(isIndexed);
    ibo->bind();
    ibo->allocate(data, size);
    ibo->release();
    indexCount = countIndices;
}

void GLBuffer::bind()
{
    vbo->bind();
    if (isIndexed)
        ibo->bind();
}

void GLBuffer::release()
{
    vbo->release();
    if (isIndexed)
        ibo->release();
}

QOpenGLBuffer *GLBuffer::getVbo() const
{
    return vbo;
}

QOpenGLBuffer *GLBuffer::getIbo() const
{
    return ibo;
}

void GLBuffer::drawArray(GLenum mode)
{
    glDrawArrays(mode, 0, vertexCount);
}

void GLBuffer::drawIndexed(GLenum mode)
{
    glDrawElements(mode, indexCount, GL_UNSIGNED_INT, nullptr);
}

size_t GLBuffer::getIndexCount() const
{
    return indexCount;
}

size_t GLBuffer::getVertexCount() const
{
    return vertexCount;
}
