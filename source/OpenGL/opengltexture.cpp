#include "opengltexture.h"

OpenGLTexture::OpenGLTexture(OpenGLTexture::Target target) :
    bindingTarget(target), textureID(0)
{

}

OpenGLTexture::~OpenGLTexture()
{
    glDeleteTextures(1, &textureID);
}

void OpenGLTexture::create()
{
    glGenTextures(1, &textureID);
}

bool OpenGLTexture::isCreated() const
{
    return textureID;
}

void OpenGLTexture::bind() const
{
    glBindTexture(static_cast<GLenum>(bindingTarget), textureID);
}

void OpenGLTexture::release() const
{
    glBindTexture(static_cast<GLenum>(bindingTarget), 0);
}

void OpenGLTexture::loadImage2DData(GLint mipMapLevel, OpenGLTexture::InternalFormat internalFormat, GLsizei width, GLsizei height, OpenGLTexture::PixelType pixelType, OpenGLTexture::DataType dataType, const void *data)
{
    glTexImage2D(static_cast<GLenum>(bindingTarget), mipMapLevel, static_cast<GLint>(internalFormat),
                 width, height, 0, static_cast<GLenum>(pixelType), static_cast<GLenum>(dataType), data);
}

void OpenGLTexture::loadImage2DDataCubeFace(OpenGLTexture::CubeFace cubeFace, GLint mipMapLevel, OpenGLTexture::InternalFormat internalFormat, GLsizei width, GLsizei height, OpenGLTexture::PixelType pixelType, OpenGLTexture::DataType dataType, const void *data)
{
    glTexImage2D(static_cast<GLenum>(cubeFace), mipMapLevel, static_cast<GLint>(internalFormat),
                 width, height, 0, static_cast<GLenum>(pixelType), static_cast<GLenum>(dataType), data);
}

void OpenGLTexture::setWrapModeS(OpenGLTexture::WrapMode mode)
{
    glTexParameteri(static_cast<GLenum>(bindingTarget), GL_TEXTURE_WRAP_S, static_cast<GLint>(mode));
}

void OpenGLTexture::setWrapModeT(OpenGLTexture::WrapMode mode)
{
    glTexParameteri(static_cast<GLenum>(bindingTarget), GL_TEXTURE_WRAP_T, static_cast<GLint>(mode));
}

void OpenGLTexture::setWrapModeR(OpenGLTexture::WrapMode mode)
{
    glTexParameteri(static_cast<GLenum>(bindingTarget), GL_TEXTURE_WRAP_R, static_cast<GLint>(mode));
}

void OpenGLTexture::setMinificationFilter(OpenGLTexture::FilterType filterType)
{
    glTexParameteri(static_cast<GLenum>(bindingTarget), GL_TEXTURE_MIN_FILTER, static_cast<GLint>(filterType));
}

void OpenGLTexture::setMagnificationFilter(OpenGLTexture::FilterType filterType)
{
    glTexParameteri(static_cast<GLenum>(bindingTarget), GL_TEXTURE_MAG_FILTER, static_cast<GLint>(filterType));
}

GLuint OpenGLTexture::getTextureID() const
{
    return textureID;
}

void OpenGLTexture::release(OpenGLTexture::Target target)
{
    glBindTexture(static_cast<GLenum>(target), 0);
}

