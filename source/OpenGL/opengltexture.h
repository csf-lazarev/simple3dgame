#ifndef OPENGLTEXTURE_H
#define OPENGLTEXTURE_H
#include <source/utils/GL.h>
#include <QMap>
class OpenGLTexture
{
public:
    enum class Target {
        Texture2D = GL_TEXTURE_2D, TextureCubeMap = GL_TEXTURE_CUBE_MAP,
        NoFormat = GL_NONE
    };

    enum class PixelType {
        RGB = GL_RGB, RGBA = GL_RGBA
    };

    enum class InternalFormat {
        RGB = GL_RGB, RGBA = GL_RGBA
    };

    enum class DataType{
        UNSIGNED_BYTE = GL_UNSIGNED_BYTE, BYTE = GL_BYTE,
        UNSIGNED_SHORT = GL_UNSIGNED_SHORT, SHORT = GL_SHORT,
        UNSIGNED_INT = GL_UNSIGNED_INT, INT = GL_INT,
        FLOAT = GL_FLOAT
    };

    enum CubeFace {
        POS_X = GL_TEXTURE_CUBE_MAP_POSITIVE_X, NEG_X = GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
        POS_Y = GL_TEXTURE_CUBE_MAP_POSITIVE_Y, NEG_Y = GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
        POS_Z = GL_TEXTURE_CUBE_MAP_POSITIVE_Z, NEG_Z = GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
    };

    enum class WrapMode {
        Repeat = GL_REPEAT, ClampToEdge = GL_CLAMP_TO_EDGE, ClampToBorder = GL_CLAMP_TO_BORDER
    };

    enum class FilterType {
        Linear = GL_LINEAR, Nearest = GL_NEAREST, NearestMipmapNearest = GL_NEAREST_MIPMAP_NEAREST,
        NearestMipmapLinear = GL_NEAREST_MIPMAP_LINEAR, LinearMimmapNearest = GL_LINEAR_MIPMAP_NEAREST,
        LinearMipmapLinear = GL_LINEAR_MIPMAP_LINEAR
    };


public:
    explicit OpenGLTexture(Target target = Target::NoFormat);
    ~OpenGLTexture();

    void create();
    bool isCreated() const;

    void bind() const ;
    void release() const;

    void loadImage2DData(GLint mipMapLevel, InternalFormat internalFormat, GLsizei width,
                         GLsizei height, PixelType pixelType, DataType dataType, const void *data);
    void loadImage2DDataCubeFace(CubeFace cubeFace, GLint mipMapLevel, InternalFormat internalFormat, GLsizei width,
                         GLsizei height, PixelType pixelType, DataType dataType, const void *data);

    void setWrapModeS(WrapMode mode);
    void setWrapModeT(WrapMode mode);
    void setWrapModeR(WrapMode mode);

    void setMinificationFilter(FilterType filterType);
    void setMagnificationFilter(FilterType filterType);

    GLuint getTextureID() const;
public:
    static void release(Target target);

private:
    Target bindingTarget;
    GLuint textureID;
};

#endif // OPENGLTEXTURE_H
