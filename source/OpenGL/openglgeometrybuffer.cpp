#include "openglgeometrybuffer.h"

OpenGLGeometryBuffer::OpenGLGeometryBuffer(QOpenGLBuffer * const vertexBuffer, QOpenGLBuffer * const normalBuffer) :
    vertexBuffer(vertexBuffer), normalBuffer(normalBuffer), uvAttached(false)
{

}

OpenGLGeometryBuffer::OpenGLGeometryBuffer(QOpenGLBuffer * const vertexBuffer, QOpenGLBuffer * const normalBuffer, QOpenGLBuffer * const uvBuffer) :
    vertexBuffer(vertexBuffer), normalBuffer(normalBuffer), uvBuffer(uvBuffer), uvAttached(true)
{

}

QOpenGLBuffer *OpenGLGeometryBuffer::getVertexBuffer() const
{
    return vertexBuffer;
}

QOpenGLBuffer *OpenGLGeometryBuffer::getNormalBuffer() const
{
    return normalBuffer;
}

QOpenGLBuffer *OpenGLGeometryBuffer::getUVBuffer() const
{
    return uvBuffer;
}

bool OpenGLGeometryBuffer::isUVAttached() const
{
    return uvAttached;
}

void OpenGLGeometryBuffer::attachUVBuffer(QOpenGLBuffer *uvBuffer)
{
    uvAttached = true;
    this->uvBuffer = uvBuffer;
}
