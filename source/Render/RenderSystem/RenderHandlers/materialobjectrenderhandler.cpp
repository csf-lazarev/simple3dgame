#include "materialobjectrenderhandler.h"

void MaterialObjectRenderHandler::renderObject(const MaterialObject &renderSource) const
{
    const Transform *objTransform = renderSource.getTransform();
    TriangulatedGeometryBuffer *tBuf = renderSource.getGeometryBufferContainer()->getBuffer();
    OpenGLGeometryBuffer *glBuffer = gMgr->getOpenGLBuferFor(*renderSource.getGeometryBufferContainer())->getBuffer();
    const Material *material = renderSource.getMaterial();

    QMatrix4x4 model = objTransform->getTransformMatrix();
    geomPass->setUniformValue("model", model);

    geomPass->setUniformValue("material.diffuse", material->getDiffuse());
    geomPass->setUniformValue("material.shininess", material->getShininess());

    QOpenGLBuffer *vertexVbo = glBuffer->getVertexBuffer();
    QOpenGLBuffer *normalVbo = glBuffer->getNormalBuffer();

    geomPass->enableAttributeArray("aPos");
    vertexVbo->bind();
    geomPass->setAttributeBuffer("aPos", GL_FLOAT, 0, 3);

    geomPass->enableAttributeArray("aNormal");
    normalVbo->bind();
    geomPass->setAttributeBuffer("aNormal", GL_FLOAT, 0, 3);

    glDrawArrays(GL_TRIANGLES, 0, tBuf->countTriangles() * 3);
}
