#ifndef MATERIALOBJECTRENDERHANDLER_H
#define MATERIALOBJECTRENDERHANDLER_H
#include <source/Render/RenderHelpers/objectrenderhandler.h>
#include <source/Objects/materialobject.h>
#include <source/ResourceSystem/geometryresourcemanager.h>
#include <QOpenGLShaderProgram>

class MaterialObjectRenderHandler : public ObjectRenderHandler<MaterialObject>
{
public:
    MaterialObjectRenderHandler(QOpenGLShaderProgram *geomPass, GeometryResourceManager *gMgr) :
        ObjectRenderHandler<MaterialObject>(), geomPass(geomPass), gMgr(gMgr)
    {

    }
    void renderObject(const MaterialObject &renderSource) const override;

private:
    QOpenGLShaderProgram *geomPass;
    GeometryResourceManager *gMgr;
};

#endif // MATERIALOBJECTRENDERHANDLER_H
