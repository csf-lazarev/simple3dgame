#ifndef RENDERSYSTEM_H
#define RENDERSYSTEM_H

#include "deferredrendersubsystem.h"

#include <QMap>
#include <QString>
#include <QOpenGLShaderProgram>
#include <QVector>
#include <source/Scene/sceneenvironment.h>
#include <source/Scene/scene.h>
class RenderSystem
{
public:
    RenderSystem(DeferredRenderSubSystem * defSubSystem);
    void renderScene(Scene *scene);
    void resize(int width, int height);

private:
    DeferredRenderSubSystem * defSubSystem;
};

#endif // RENDERSYSTEM_H
