#include "deferredrendersubsystem.h"
#include <source/Objects/materialobject.h>
#include <source/utils/utilits.h>


DeferredRenderSubSystem::DeferredRenderSubSystem(const QSize &contextSize, const QOpenGLFramebufferObjectFormat &format,
                                                 QOpenGLShaderProgram *geom, QOpenGLShaderProgram *light,
                                                 GeometryResourceManager *gMgr) :
    gBuffer(new QOpenGLFramebufferObject(contextSize, format)),
    contextSize(contextSize), geometryShader(geom), lightingShader(light), gMgr(gMgr)
{
    gBuffer->setAttachment(QOpenGLFramebufferObject::Attachment::Depth);
    initSystem();
    initQuad();
}

void DeferredRenderSubSystem::render(const SceneEnvironment &sceneEnvirontment)
{
    geomPass(sceneEnvirontment);
    lightingPass(sceneEnvirontment);

    auto glFunc = getOpenGLExtraFunctions();
    glFunc.glBindFramebuffer(GL_READ_FRAMEBUFFER, gBuffer->handle());
    glFunc.glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glFunc.glBlitFramebuffer(
                0, 0, gBuffer->width(), gBuffer->height(), 0, 0, gBuffer->width(), gBuffer->height(),
                GL_DEPTH_BUFFER_BIT, GL_NEAREST);
    glFunc.glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void DeferredRenderSubSystem::resize(int width, int height)
{
    QOpenGLFramebufferObjectFormat format = gBuffer->format();
    contextSize.setWidth(width);
    contextSize.setHeight(height);
    destoyFrameBuffer();
    gBuffer = new QOpenGLFramebufferObject(contextSize, format);
    initSystem();
}


void DeferredRenderSubSystem::initSystem()
{
    qDebug() << gBuffer->size();
    gBuffer->bind();
    // Position buffer
    gBuffer->addColorAttachment(contextSize, GL_RGBA16F);
    // Albedo and specular buffer
    gBuffer->addColorAttachment(contextSize, GL_RGBA16F);
    // Normal buffer
    gBuffer->addColorAttachment(contextSize, GL_RGBA16F);

    QVector<GLuint> textures = gBuffer->textures();
    gPosiion = textures[0];
    gAlbedoSpec = textures[1];
    gNormal = textures[2];
    GLenum attachments[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2};
    auto glFunc = getOpenGLExtraFunctions();
    glFunc.glDrawBuffers(3, attachments);
    gBuffer->release();
}

void DeferredRenderSubSystem::destoyFrameBuffer()
{
    gBuffer->release();
   delete gBuffer;
}

void DeferredRenderSubSystem::geomPass(const SceneEnvironment &sceneEnvirontment)
{
    auto *camera = sceneEnvirontment.getActiveCamera();
    matrix4 gridCorrect;
    matrix4 view = camera->getViewMatrix();
    matrix4 proj = camera->getProjectionMatrix();
    glPolygonMode(GL_FRONT, GL_FILL);
    gBuffer->bind();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    geometryShader->bind();

    geometryShader->setUniformValue("view", view);
    geometryShader->setUniformValue("projection", proj);

    ObjectManager *mgr = sceneEnvirontment.getObjectManager();

    const QVector<INoSelfRenderingObject *> &objs = mgr->getNoSelfRenderingObjects();

    for (INoSelfRenderingObject * obj : objs){
        auto t = obj->getObjectTypeHash();
        BaseObjectRenderHandler * handler = renderHandlersMap.value(obj->getObjectTypeHash());
        handler->render(obj);
    }
    geometryShader->release();
    gBuffer->release();
}

void DeferredRenderSubSystem::lightingPass(const SceneEnvironment &sceneEnvirontment)
{
    auto *camera = sceneEnvirontment.getActiveCamera();
    lightingShader->bind();
    auto glFunc = getOpenGLFunctions();
    glFunc.glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, gPosiion);
    lightingShader->setUniformValue("gPosition", 0);

    glFunc.glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, gAlbedoSpec);
    lightingShader->setUniformValue("gAlbedoSpec", 1);

    glFunc.glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, gNormal);
    lightingShader->setUniformValue("gNormal", 2);

    const QVector<PointLight *> lights = sceneEnvirontment.getLights();
    auto lMgr = sceneEnvirontment.getLightingManager();
    DirectedLight *dirLight = lMgr.getDirLight();
    lightingShader->setUniformValue("viewPos", camera->getPosition());
    lightingShader->setUniformValue("realLightCount", lights.size());

    for (int i = 0; i < lights.size(); ++i) {
        lightingShader->setUniformValue(("lights[" + QString::number(i) + "].position").toStdString().data(), lights[i]->getPosition());
        lightingShader->setUniformValue(("lights[" + QString::number(i) + "].color").toStdString().data(), lights[i]->getColor());
        lightingShader->setUniformValue(("lights[" + QString::number(i) + "].constant").toStdString().data(), lights[i]->getConstant());
        lightingShader->setUniformValue(("lights[" + QString::number(i) + "].linear").toStdString().data(), lights[i]->getLinear());
        lightingShader->setUniformValue(("lights[" + QString::number(i) + "].quadratic").toStdString().data(), lights[i]->getQuadratic());
    }

    lightingShader->setUniformValue("dirLight.direction",dirLight->getDirection());
    lightingShader->setUniformValue("dirLight.color", dirLight->getColor());
    lightingShader->setUniformValue("dirLight.power", dirLight->getPower());

    renderQuad();
    lightingShader->release();

}

void DeferredRenderSubSystem::initQuad()
{
    float quadVertices[] = {
        // positions        // texture Coords
        -1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
        -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
         1.0f,  1.0f, 0.0f, 1.0f, 1.0f,

        -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
         1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
         1.0f,  1.0f, 0.0f, 1.0f, 1.0f
    };
    quadBuffer = QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
    quadBuffer.create();
    quadBuffer.bind();
    quadBuffer.allocate(quadVertices, sizeof (quadVertices));
    quadBuffer.release();
}

void DeferredRenderSubSystem::renderQuad()
{

    quadBuffer.bind();
    lightingShader->enableAttributeArray("pos");
    lightingShader->setAttributeBuffer("pos", GL_FLOAT, 0, 3, 5 * sizeof(float));
    lightingShader->enableAttributeArray("texCoords");
    lightingShader->setAttributeBuffer("texCoords", GL_FLOAT, 3 * sizeof(float), 2, 5 * sizeof(float));

    glDrawArrays(GL_TRIANGLES, 0, 6);
    quadBuffer.release();
}
