#include "rendersystem.h"

RenderSystem::RenderSystem(DeferredRenderSubSystem * defSubSystem) :
   defSubSystem(defSubSystem)
{

}

void RenderSystem::renderScene(Scene *scene)
{
    SceneEnvironment environment(scene);
    defSubSystem->render(environment);
}

void RenderSystem::resize(int width, int height)
{
    defSubSystem->resize(width, height);
}
