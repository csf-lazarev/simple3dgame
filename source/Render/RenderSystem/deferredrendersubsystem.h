#ifndef DEFERREDRENDERSUBSYSTEM_H
#define DEFERREDRENDERSUBSYSTEM_H
#include <QOpenGLBuffer>
#include <QOpenGLFramebufferObject>
#include <QOpenGLFramebufferObjectFormat>
#include <QOpenGLShaderProgram>
#include <QVector>
#include <QMap>
#include <source/Scene/sceneenvironment.h>
#include <source/ResourceSystem/geometryresourcemanager.h>
#include <source/Render/RenderHelpers/objectrenderhandler.h>
#include <source/typeid.h>
#include <source/utils/GL.h>

class DeferredRenderSubSystem
{
public:
    DeferredRenderSubSystem(const QSize &contextSize, const QOpenGLFramebufferObjectFormat &format,
                            QOpenGLShaderProgram *geom, QOpenGLShaderProgram *light,
                            GeometryResourceManager *gMgr);
    void render(const SceneEnvironment &sceneEnvirontment);
    void resize(int width, int height);

    template<class ObjectType>
    void registerObjectRenderHandler(ObjectRenderHandler<ObjectType> *handler){
        renderHandlersMap.insert(getTypeHash<ObjectType>(), handler);
    }
private:
    void initSystem();
    void destoyFrameBuffer();
    void createFrameBuffer();
    void geomPass(const SceneEnvironment &sceneEnvirontment);
    void lightingPass(const SceneEnvironment &sceneEnvirontment);
    void initQuad();
    void renderQuad();
private:
    QOpenGLFramebufferObject *gBuffer;
    QOpenGLShaderProgram *geometryShader;
    QOpenGLShaderProgram *lightingShader;

    QSize contextSize;
    GLuint gPosiion;
    GLuint gAlbedoSpec;
    GLuint gNormal;

    QOpenGLBuffer quadBuffer;

    GeometryResourceManager *gMgr;

    QHash<size_t, BaseObjectRenderHandler *> renderHandlersMap;

};



#endif // DEFERREDRENDERSUBSYSTEM_H
