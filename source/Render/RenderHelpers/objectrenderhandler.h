#ifndef OBJECTRENDERHANDLER_H
#define OBJECTRENDERHANDLER_H
#include "baseobjectrenderhandler.h"

template<class ObjectType>
class ObjectRenderHandler : public BaseObjectRenderHandler
{
public:
    ObjectRenderHandler() : BaseObjectRenderHandler(){

    }

    virtual void renderObject(const ObjectType &renderSource) const = 0;
    virtual void render(BaseRenderingObject *object) const override{
        ObjectType *o = dynamic_cast<ObjectType *>(object);
        if (o)
            renderObject(*o);
    }
};

#endif // OBJECTRENDERHANDLER_H
