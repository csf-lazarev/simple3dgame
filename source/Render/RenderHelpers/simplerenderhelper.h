#ifndef GRIDRENDERHELPER_H
#define GRIDRENDERHELPER_H

#include <source/Scene/sceneenvironment.h>
#include <source/Render/RenderHelpers/irenderhelper.h>



class SimpleRenderHelper : public IRenderHelper
{
public:
    SimpleRenderHelper(SceneEnvironment *sceneEnvironment);
    SceneEnvironment *getSceneEnvironment() const;

private:
    SceneEnvironment *sceneEnvironment;
};

#endif // GRIDRENDERHELPER_H
