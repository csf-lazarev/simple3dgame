#ifndef EXTENDEDRENDERHELPER_H
#define EXTENDEDRENDERHELPER_H
#include "irenderhelper.h"

#include <source/Scene/sceneenvironment.h>
#include <source/ResourceSystem/geometryresourcemanager.h>

class ExtendedRenderHelper : public IRenderHelper
{
public:
    ExtendedRenderHelper(const SceneEnvironment &scEnvr,
                         const GeometryResourceManager &gMgr);

    const SceneEnvironment &getSceneEnvironment() const;

    const GeometryResourceManager &getGeometryManager() const;

private:
    const SceneEnvironment &scEnvr;
    const GeometryResourceManager &gMgr;
};

#endif // EXTENDEDRENDERHELPER_H
