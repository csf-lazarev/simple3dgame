#include "extendedrenderhelper.h"

ExtendedRenderHelper::ExtendedRenderHelper(const SceneEnvironment &scEnvr,
                                           const GeometryResourceManager &gMgr) :
    scEnvr(scEnvr), gMgr(gMgr)
{

}

const SceneEnvironment &ExtendedRenderHelper::getSceneEnvironment() const
{
    return scEnvr;
}

const GeometryResourceManager &ExtendedRenderHelper::getGeometryManager() const
{
    return gMgr;
}
