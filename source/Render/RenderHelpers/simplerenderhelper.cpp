#include "simplerenderhelper.h"

SimpleRenderHelper::SimpleRenderHelper(SceneEnvironment *sceneEnvironment) :
    sceneEnvironment(sceneEnvironment)
{

}

SceneEnvironment *SimpleRenderHelper::getSceneEnvironment() const
{
    return sceneEnvironment;
}
