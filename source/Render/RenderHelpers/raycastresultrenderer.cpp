#include "raycastresultrenderer.h"
#include <source/Scene/sceneenvironment.h>
RaycastResultRenderer::RaycastResultRenderer(SceneEnvironment *environment) :
    BaseRenderer<RaycastResult>(), environment(environment)
{

}

void RaycastResultRenderer::renderObject(const RaycastResult &renderSource) const
{
    QMatrix4x4 transform = renderSource.getItem()->getObjectTransform()->getTransformMatrix();
    ICamera *camera = environment->getActiveCamera();
    QMatrix4x4 projView = camera->getProjectionMatrix() * camera->getViewMatrix();

    glDepthFunc(GL_ALWAYS);

    glLoadMatrixf((projView * transform ).data());

    glColor3f(1,1,1);

    BaseTriangle *triangle = renderSource.getTriangle();
    QVector3D p1 = triangle->getA();
    QVector3D p2 = triangle->getB();
    QVector3D p3 = triangle->getC();
    glBegin(GL_TRIANGLES);
    glVertex3f(p1.x(), p1.y(), p1.z());
    glVertex3f(p2.x(), p2.y(), p2.z());
    glVertex3f(p3.x(), p3.y(), p3.z());
    glEnd();

    glColor3f(0,0,0);
    QVector3D point = renderSource.getPoint();
    glPointSize(4);
    glDepthFunc(GL_LEQUAL);
    glBegin(GL_POINTS);
    glVertex3f(point.x(), point.y(), point.z());
    glEnd();

    glLoadIdentity();
    glDepthFunc(GL_LESS);
}
