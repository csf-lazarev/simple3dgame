#ifndef RAYCASTRESULTRENDERER_H
#define RAYCASTRESULTRENDERER_H

#include "baserenderer.h"

class SceneEnvironment;
#include <source/Objects/Intersection/raycastresult.h>

class RaycastResultRenderer : public BaseRenderer<RaycastResult>
{
public:
    RaycastResultRenderer(SceneEnvironment *environment);
    void renderObject(const RaycastResult &renderSource) const override;

private:
    SceneEnvironment *environment;
};

#endif // RAYCASTRESULTRENDERER_H
