#ifndef BASEOBJECTRENDERHANDLER_H
#define BASEOBJECTRENDERHANDLER_H
#include <source/Objects/baserenderingobject.h>


class BaseObjectRenderHandler
{
public:
    BaseObjectRenderHandler();
    virtual ~BaseObjectRenderHandler();
    virtual void render(BaseRenderingObject *object) const = 0;
};

#endif // BASEOBJECTRENDERHANDLER_H
