#ifndef BASERENDERER_H
#define BASERENDERER_H

template<class RenderSource>
class BaseRenderer
{
public:
    BaseRenderer()
    {

    }
    virtual ~BaseRenderer()
    {

    }

    virtual void renderObject(const RenderSource &renderSource) const = 0;
    void renderObjectPtr(RenderSource *renderSource){
        renderObject(*renderSource);
    }

};

#endif // BASERENDERER_H
