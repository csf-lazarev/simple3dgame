#ifndef ICAMERA_H
#define ICAMERA_H

#include "types.h"

class ICamera
{
public:
    ICamera();
    virtual ~ICamera();
    virtual matrix4 &getViewMatrix() const  = 0;
    virtual matrix4 &getProjectionMatrix() const  = 0;

    virtual float getFOV() const  = 0;
    virtual float getAspectRatio() const = 0;
    virtual float getZoom() const = 0;
    virtual float getDistanceToNearPlane() const  = 0;
    virtual float getDistanceToFarPlane() const  = 0;
    virtual vector3 getPosition() const = 0;
    virtual vector3 getLookTarget() const = 0;
    virtual vector3 getLookDirection() const = 0;

    virtual void setFOV(float fovAngle) = 0;
    virtual void setAspectRatio(float aspectRadio) = 0;
    virtual void setZoom(float zoom) = 0;
    virtual void setDistanceToNearPlane(float distance) = 0;
    virtual void setDistanceToFarPlane(float distance) = 0;
    virtual void setPosition(const vector3 &position) = 0;

};

#endif // ICAMERA_H
