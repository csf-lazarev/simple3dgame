#ifndef FREECAMERA_H
#define FREECAMERA_H

#include "icamera.h"
#include "idrivencamera.h"

#include <types.h>
#include <qmath.h>

class FreeCamera : public IDrivenCamera
{
public:
    FreeCamera(const vector3 &position, const vector3 &lookTarget, const vector3 &wordUp,
               float zoom, float aspectRatio, float nearPlane, float farPlane);



    void onKeyPress(QKeyEvent *event) override;
    void mouseMoved(float xOffset, float yOffset) override;
    void wheelEvent(QWheelEvent *event) override;

    matrix4& getViewMatrix() const override;
    matrix4 & getProjectionMatrix() const override;
    float getFOV() const override;
    float getZoom() const override;
    float getAspectRatio() const override;
    float getDistanceToFarPlane() const override;
    float getDistanceToNearPlane() const override;

    vector3 getPosition() const override;
    vector3 getLookTarget() const override;
    vector3 getLookDirection() const override;

    void setFOV(float fovAngle) override;
    void setZoom(float zoom) override;
    void setAspectRatio(float aspectRadio) override;
    void setPosition(const vector3 &position) override;
    void setDistanceToFarPlane(float distance) override;
    void setDistanceToNearPlane(float distance) override;



public:
    constexpr static const float MOUSE_SENSETIVE = 0.25;
    constexpr static const float NORMAL_FOV = 45.f;

private:

    void updateCameraVectors();

    vector3 position;

    vector3 front;
    vector3 rightAxis;
    vector3 up;
    vector3 wordUp;

    float yawAngle;
    float pitchAngle;

    float zoom;
    float aspectRatio;
    float nearPlane;
    float farPlane;


    mutable matrix4 viewMatrix;
    mutable matrix4 projmatrix;
};

#endif // FREECAMERA_H
