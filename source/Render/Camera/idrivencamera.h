#ifndef IDRIVENCAMERA_H
#define IDRIVENCAMERA_H

#include "icamera.h"

#include <source/InputSystem/ikeyhandler.h>
#include <source/InputSystem/imousehandler.h>



class IDrivenCamera : public ICamera, public IKeyHandler, public IMouseHandler
{
public:
    IDrivenCamera();
    virtual ~IDrivenCamera() override;

    virtual matrix4 &getViewMatrix() const override  = 0;
    virtual matrix4 &getProjectionMatrix() const override  = 0;
    virtual void wheelEvent(QWheelEvent *event) override = 0;

    virtual float getFOV() const override  = 0;
    virtual float getAspectRatio() const override = 0;
    virtual float getZoom() const override = 0;
    virtual float getDistanceToNearPlane() const override = 0;
    virtual float getDistanceToFarPlane() const override = 0;
    virtual vector3 getPosition() const override = 0;
    virtual vector3 getLookTarget() const override = 0;
    virtual vector3 getLookDirection() const override = 0;

    virtual void setFOV(float fovAngle) override = 0;
    virtual void setAspectRatio(float aspectRadio) override = 0;
    virtual void setZoom(float zoom) override = 0;
    virtual void setDistanceToNearPlane(float distance) override = 0;
    virtual void setDistanceToFarPlane(float distance) override = 0;
    virtual void setPosition(const vector3 &position) override = 0;

    virtual void onKeyPress(QKeyEvent *event) override = 0;
    virtual void mouseMoved(float xOffset, float yOffset) override = 0;

};

#endif // IDRIVENCAMERA_H
