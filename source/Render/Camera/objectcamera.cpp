#include "objectcamera.h"

ObjectCamera::ObjectCamera(const QVector3D &objectPos, const QVector3D &worldUp, float zoom, float aspectRatio, float nearPlane, float farPlane) :
    lookObjectPosition(objectPos), distanceToObject(4), wordUp(worldUp), position(objectPos.x(), objectPos.y(), objectPos.z()-distanceToObject),
    xAngle(0), yAngle(0), zoom(zoom), aspectRatio(aspectRatio), nearPlane(nearPlane), farPlane(farPlane)
{
    updateCameraVectors();
}

matrix4 &ObjectCamera::getViewMatrix() const
{
    viewMatrix.setToIdentity();
    viewMatrix.lookAt(position, position + front, up);
    return viewMatrix;
}

matrix4 &ObjectCamera::getProjectionMatrix() const
{
    projmatrix.setToIdentity();
    projmatrix.perspective(1/zoom * NORMAL_FOV, aspectRatio, nearPlane, farPlane);
    return projmatrix;
}

float ObjectCamera::getFOV() const
{
    return 1/ zoom * NORMAL_FOV;
}

float ObjectCamera::getZoom() const
{
    return zoom;
}

float ObjectCamera::getAspectRatio() const
{
    return aspectRatio;
}

float ObjectCamera::getDistanceToFarPlane() const
{
    return farPlane;
}

float ObjectCamera::getDistanceToNearPlane() const
{
    return nearPlane;
}

vector3 ObjectCamera::getPosition() const
{
    return position;
}

vector3 ObjectCamera::getLookTarget() const
{
    return lookObjectPosition;
}

vector3 ObjectCamera::getLookDirection() const
{
    return front;
}

void ObjectCamera::setFOV(float fovAngle)
{
    zoom = NORMAL_FOV / fovAngle;
}

void ObjectCamera::setZoom(float zoom)
{
    this->zoom = zoom;
}

void ObjectCamera::setAspectRatio(float aspectRadio)
{
    this->aspectRatio = aspectRadio;
}

void ObjectCamera::setPosition(const vector3 &position)
{

}


void ObjectCamera::setDistanceToFarPlane(float distance)
{
    farPlane = distance;
}

void ObjectCamera::setDistanceToNearPlane(float distance)
{
    nearPlane = distance;
}

void ObjectCamera::onKeyPress(QKeyEvent *event)
{
    if (event->key() == Qt::Key::Key_W)
        yAngle += 5;
    if (event->key() == Qt::Key::Key_S)
        yAngle -= 5;
    if (event->key() == Qt::Key::Key_D)
        xAngle -= 5;
    if (event->key() == Qt::Key::Key_A)
        xAngle += 5;


    if (yAngle > 89.0f)
        yAngle = 89.0f;
    if (yAngle < -89.0f)
        yAngle = -89.0f;

    updateCameraVectors();
}

void ObjectCamera::mouseMoved(float xOffset, float yOffset)
{

    xOffset *= MOUSE_SENSETIVE;
    yOffset *= MOUSE_SENSETIVE;

    xAngle += xOffset;
    yAngle += yOffset;

    if (yAngle > 89.0f)
        yAngle = 89.0f;
    if (yAngle < -89.0f)
        yAngle = -89.0f;

    updateCameraVectors();
}

void ObjectCamera::wheelEvent(QWheelEvent *event)
{
    if (event->delta() > 0)
        distanceToObject += 0.1;
    else if (event->delta() < 0)
        distanceToObject -= 0.1;

    if (distanceToObject < 1e-2)
        distanceToObject = 0.1;
    updateCameraVectors();
}

void ObjectCamera::updateCameraVectors()
{
    float cosX = qCos(qDegreesToRadians(xAngle));
    float cosY = qCos(qDegreesToRadians(yAngle));
    float sinX = qSin(qDegreesToRadians(xAngle));
    float sinY = qSin(qDegreesToRadians(yAngle));

    position = vector3(cosX * cosY ,sinY, sinX * cosY);
    position.normalize();
    position *= distanceToObject;
    position += lookObjectPosition;

    front = lookObjectPosition - position;
    front.normalize();

    rightAxis = QVector3D::crossProduct(front, wordUp).normalized();
    up = QVector3D::crossProduct(rightAxis, front).normalized();
}
