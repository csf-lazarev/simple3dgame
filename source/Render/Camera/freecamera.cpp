#include "freecamera.h"



FreeCamera::FreeCamera(const vector3 &position, const vector3 &lookTarget, const vector3 &wordUp, float zoom, float aspectRatio, float nearPlane, float farPlane) :
    position(position), front((lookTarget - position).normalized()), wordUp(wordUp.normalized()), viewMatrix(),
    yawAngle(90.0), pitchAngle(0.0), zoom(zoom), aspectRatio(aspectRatio), nearPlane(nearPlane), farPlane(farPlane)
{
    Q_ASSERT(zoom > 1e-4f);
    updateCameraVectors();
}

matrix4 &FreeCamera::getViewMatrix() const
{
    viewMatrix.setToIdentity();
    viewMatrix.lookAt(position, position + front, up);
    return viewMatrix;
}

matrix4 &FreeCamera::getProjectionMatrix() const
{
    projmatrix.setToIdentity();
    projmatrix.perspective(1/zoom * NORMAL_FOV, aspectRatio, nearPlane, farPlane);
    return projmatrix;
}

float FreeCamera::getFOV() const
{
    return 1/ zoom * NORMAL_FOV;
}

float FreeCamera::getZoom() const
{
    return zoom;
}

float FreeCamera::getAspectRatio() const
{
    return aspectRatio;
}

float FreeCamera::getDistanceToFarPlane() const
{
    return farPlane;
}

float FreeCamera::getDistanceToNearPlane() const
{
    return nearPlane;
}

vector3 FreeCamera::getPosition() const
{
    return position;
}

vector3 FreeCamera::getLookTarget() const
{
    return position + front;
}

vector3 FreeCamera::getLookDirection() const
{
    return front;
}

void FreeCamera::setFOV(float fovAngle)
{
    zoom = NORMAL_FOV / fovAngle;
}

void FreeCamera::setZoom(float zoom)
{
    this->zoom = zoom;
}

void FreeCamera::setAspectRatio(float aspectRadio)
{
    this->aspectRatio = aspectRadio;
}

void FreeCamera::setPosition(const vector3 &position)
{
    this->position = position;
}

void FreeCamera::setDistanceToFarPlane(float distance)
{
    farPlane = distance;
}

void FreeCamera::setDistanceToNearPlane(float distance)
{
    nearPlane = distance;
}

void FreeCamera::onKeyPress(QKeyEvent *event)
{
    float speedFactor = event->modifiers() & Qt::Modifier::CTRL? 2.5 : 1;
    if (event->key() == Qt::Key::Key_W)
        position += 0.15 * speedFactor * front;
    if (event->key() == Qt::Key::Key_S)
        position -= 0.15 * speedFactor * front;
    if (event->key() == Qt::Key::Key_D)
        position += 0.15 * speedFactor * rightAxis;
    if (event->key() == Qt::Key::Key_A)
        position -= 0.15 * speedFactor * rightAxis;
}

void FreeCamera::mouseMoved(float xOffset, float yOffset)
{
    xOffset *= MOUSE_SENSETIVE;
    yOffset *= MOUSE_SENSETIVE;

    yawAngle += xOffset;
    pitchAngle += yOffset;

    // Make sure that when pitch is out of bounds, screen doesn't get flipped

        if (pitchAngle > 89.0f)
            pitchAngle = 89.0f;
        if (pitchAngle < -89.0f)
            pitchAngle = -89.0f;

        updateCameraVectors();
}

void FreeCamera::wheelEvent(QWheelEvent *event)
{
    if (event->delta() > 0)
        zoom += 0.1;
    else if (event->delta() < 0)
        zoom -= 0.1;
    if (zoom < 1e-2f)
        zoom = 0.1;
    if (zoom > 3.5f)
        zoom = 3.5f;
}

void FreeCamera::updateCameraVectors()
{

    vector3 newFront;
    float radYaw = qDegreesToRadians(yawAngle);
    float radPitch = qDegreesToRadians(pitchAngle);

    newFront.setX(qCos(radYaw) * qCos(radPitch));
    newFront.setY(qSin(radPitch));
    newFront.setZ(qSin(radYaw) * qCos(radPitch));

    front = newFront.normalized();
    rightAxis = QVector3D::crossProduct(front, wordUp).normalized();
    up = QVector3D::crossProduct(rightAxis, front).normalized();
}
