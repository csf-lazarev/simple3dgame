#include "viewport.h"

#include <source/Render/Camera/freecamera.h>

#include <objreader.h>
#include <QKeyEvent>
#include <source/Objects/sceneobject.h>

Viewport::Viewport(Scene *scene, RenderSystem *renderSystem, const QSize &viewportSize, QWidget *parent) : QOpenGLWidget(parent), scene(scene),
    renderSystem(renderSystem), keyRegister(), viewportSize(viewportSize)
{
    setMouseTracking(true);
}

QSize Viewport::sizeHint() const
{
    return viewportSize;
}



void Viewport::initializeGL()
{
    recalculateWidgetGlobalCenter();
    cursor().setPos(widgetGlobalCenter);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glClearColor(0.0f,0.0f,0.0f,0.0f);
}

void Viewport::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    for (QKeyEvent * event : keyRegister.getEvents())
        scene->getActiveDrivenCamera()->onKeyPress(event);
    scene->update();
    renderSystem->renderScene(scene);
    scene->render();
    drawHUD();
}

void Viewport::resizeGL(int w, int h)
{
    viewportSize.setWidth(w);
    viewportSize.setHeight(h);
    qDebug() << viewportSize;
    renderSystem->resize(w,h);
    scene->getActiveCamera()->setAspectRatio(float(w)/h);
}

void Viewport::mouseMoveEvent(QMouseEvent *event){
    QPoint cursorPos = cursor().pos();
    scene->getActiveDrivenCamera()->mouseMoved(cursorPos.x() - widgetGlobalCenter.x(),
                                               cursorPos.y() - widgetGlobalCenter.y());
    cursor().setPos(widgetGlobalCenter);
    update();

}

void Viewport::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::MouseButton::LeftButton){

        ICamera *cam = scene->getActiveCamera();
        Ray ray(cam->getPosition(), cam->getLookDirection());
        RayTracer *tracer = scene->getRayTracer();
        bool status = false;
        tracer->trace(ray, &status);
        update();
    }
}

void Viewport::wheelEvent(QWheelEvent *event)
{
    scene->getActiveDrivenCamera()->wheelEvent(event);
    update();
}

void Viewport::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key::Key_Escape)
        close();
    keyRegister.registerKeyEvent(event);
    update();

    if(event->key() == Qt::Key::Key_P){
        QVector3D pos = scene->getActiveCamera()->getPosition();
        scene->getLightingManager()->registerLight(new PointLight({1,1,1}, pos, 1, 0.07f, 0.017f));
    }
}

void Viewport::keyReleaseEvent(QKeyEvent *event)
{
    keyRegister.registerKeyEvent(event);
    event->accept();
}

void Viewport::drawHUD()
{
    glDepthFunc(GL_ALWAYS);
    glColor3f(1.f, 0,0);
    glBegin(GL_LINES);
    glVertex3f(0,0.05f,0.05f);
    glVertex3f(0,-0.05f,0.05f);
    glVertex3f(-0.05f, 0,0.05f);
    glVertex3f(0.05f,0,0.05f);
    glEnd();
    glDepthFunc(GL_LESS);
}

void Viewport::recalculateWidgetGlobalCenter()
{
    QSize widgetSize = size();
    QPoint position = pos();
    widgetGlobalCenter = {position.x() + widgetSize.width()/2, position.y() + widgetSize.height()/2};
}
