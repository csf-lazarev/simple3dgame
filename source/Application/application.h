#ifndef APPLICATION_H
#define APPLICATION_H

#include "viewport.h"

#include <QOpenGLContext>


class Application
{
public:
    Application(QOpenGLContext *glContext, Viewport *viewport, Scene *scene);

private:
    QOpenGLContext *glContext;
    Viewport *viewport;
    Scene *scene;
};

#endif // APPLICATION_H
