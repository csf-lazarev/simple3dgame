#ifndef VIEWPORT_H
#define VIEWPORT_H

#include <QWidget>
#include <QOpenGLWidget>
#include <source/Scene/scene.h>
#include <QDebug>
#include <source/InputSystem/keyeventregister.h>

#include <source/Render/RenderSystem/rendersystem.h>

class Viewport : public QOpenGLWidget
{
    Q_OBJECT
public:
    explicit Viewport(Scene *scene, RenderSystem * renderSystem, const QSize &viewportSize, QWidget *parent = nullptr);
    QSize sizeHint() const override;

protected:
    void initializeGL() override;
    void paintGL() override;
    void resizeGL(int w, int h) override;

    void mouseMoveEvent(QMouseEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void wheelEvent(QWheelEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;
signals:

public slots:

private:
    void drawHUD();

private:
    QPoint widgetGlobalCenter;

    void recalculateWidgetGlobalCenter();
    QSize viewportSize;
    Scene *scene;
    RenderSystem *renderSystem;
    KeyEventRegister keyRegister;

};

#endif // VIEWPORT_H
