#include "objreader.h"

#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QMap>

#include <source/Buffer/geometrybuffer.h>
#include <source/Buffer/vertexbuffer.h>

ObjReader::ObjReader()
{

}

GeometryBuffer* ObjReader::parse(const QString &source) const
{
 QTextStream stream(new QString(source));
 return parse(stream);
}

GeometryBuffer *ObjReader::parseFromFile(const QString &filePath) const
{
    QFile file(filePath);
    file.open(QIODevice::ReadOnly);
    return parse(file);
}

GeometryBuffer* ObjReader::parse(QFile &file) const
{
    if(!file.isOpen())
        if(!file.open(QIODevice::ReadOnly))
            return nullptr;
    QTextStream stream(&file);
    return parse(stream);
}

GeometryBuffer *ObjReader::parse(QTextStream &input_stream) const
{

    VertexBuffer *vertexBuffer = new VertexBuffer();
    GeometryBuffer* geometryBuffer = new GeometryBuffer(vertexBuffer);
    bool status = true;
    while (!input_stream.atEnd()) {
        QString line = input_stream.readLine();

        if (line == "" || line.startsWith('#')){
           continue;
        }

       QTextStream line_stream(&line);
       QString key;
       line_stream >> key;

        if (key == "v"){
            QVector3D v = parseVector3D(line_stream.readLine().trimmed(), status);
            vertexBuffer->addVertex(v);
        }
        else if (key == "vn") {
            QVector3D vn = parseVector3D(line_stream.readLine().trimmed(), status);
            vertexBuffer->addNormal(vn);
        }
        else if (key == "vt"){
            QVector2D vt = parseVector2D(line_stream.readLine().trimmed(), status);
            vertexBuffer->addTextureVertex(vt);
        }
        else if (key == "f"){

            QVector<int> vertices, textures, normals;
            if (!parseFaceCondition(line_stream.readLine().trimmed(), vertices, textures, normals)){
                status = false;
                delete geometryBuffer;
                return nullptr;
            }

            geometryBuffer->addFace(&vertices, &textures, &normals);
        }

        if (!status){
            delete geometryBuffer;
            return nullptr;
        }

    }
    return geometryBuffer;
}

QVector4D ObjReader::parseVector4D(const QString &source, bool &status)
{
    QStringList stringValues = source.split(' ');
    bool ifWrongSize =  stringValues.size() != 4;
    if (ifWrongSize){
        status = false;
        return QVector4D();
    }
    QVector4D res;
    for (int i = 0; i < stringValues.size(); ++i){
        res[i] = stringValues[i].toFloat(&status);
        if (!status)
            break;
    }
    return res;
}


QVector3D ObjReader::parseVector3D(const QString &source, bool &status)
{
    QStringList stringValues = source.split(' ');
    bool ifWrongSize =  stringValues.size() !=3;
    if (ifWrongSize){
        status = false;
        return QVector3D();
    }
    QVector3D res;
    for (int i = 0; i < stringValues.size(); ++i){
        res[i] = stringValues[i].toFloat(&status);
        if (!status)
            break;
    }
    return res;
}

QVector2D ObjReader::parseVector2D(const QString &source, bool &status)
{

    QStringList stringValues = source.split(' ');
    bool ifWrongSize =  stringValues.size() < 2;
    if (ifWrongSize){
        status = false;
        return QVector2D();
    }
    QVector2D res;
    for (int i = 0; i < 2; ++i){
        res[i] = stringValues[i].toFloat(&status);
        if (!status)
            break;
    }
    return res;

}

bool ObjReader::parseFaceCondition(const QString &source, QVector<int> &vertexIndices, QVector<int> &textureIndices, QVector<int> &normalIndices)
{
    bool status;
    QStringList spaceGroups = source.split(' ');
    if (!spaceGroups.size())
        return false;
    bool skipTexture = false;
    int indexGroupSize = -1;

    QVector<QVector<int>*> indices{&vertexIndices, &textureIndices, &normalIndices};

    for (int i = 0; i < spaceGroups.size(); ++i){
        QStringList stringIndicesGroup = spaceGroups[i].split('/');

        if (!i){
            indexGroupSize = stringIndicesGroup.size();
            if (indexGroupSize > 3)
                return false;
            bool dontHaveTextureCoords = (stringIndicesGroup.size() == 3 && stringIndicesGroup[1] == "");
            if (dontHaveTextureCoords)
                skipTexture = true;
        }

        else if (stringIndicesGroup.size() != indexGroupSize)
            return false;

        for (int index = 0; index < indexGroupSize; ++index){

            if (skipTexture && index == 1){

                if (stringIndicesGroup[index] == "")
                    continue;

                else return false;
            } else {

                indices[index]->append(stringIndicesGroup[index].toInt(&status) - 1);

                if (!status)
                    return false;
            }
        }
    }
    return true;
}
