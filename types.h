#ifndef TYPES_H
#define TYPES_H
#include <QMatrix4x4>
#include <QVector3D>
#include <QVector4D>

using matrix4 = QMatrix4x4;
using vector4 = QVector4D;
using vector3 = QVector3D;

#endif // TYPES_H
