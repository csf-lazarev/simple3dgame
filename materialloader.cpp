#include "materialloader.h"

MaterialLoader::MaterialLoader()
{

}


QVector3D parseVector3D(const QString &source, bool &status){
    QStringList stringValues = source.split(' ');
    bool ifWrongSize =  stringValues.size() !=3;
    if (ifWrongSize){
        status = false;
        return QVector3D();
    }
    QVector3D res;
    for (int i = 0; i < stringValues.size(); ++i){
        res[i] = stringValues[i].toFloat(&status);
        if (!status)
            break;
    }
    return res;
}


