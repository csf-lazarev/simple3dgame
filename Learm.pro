QT       += core gui testlib opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    materialloader.cpp \
    source/Application/application.cpp \
    source/Application/viewport.cpp \
    source/Buffer/geometrybuffer.cpp \
    source/Buffer/gtriangle.cpp \
    source/Buffer/openglgeometrybuffercreator.cpp \
    source/Buffer/triangulatedgeometrybuffer.cpp \
    source/Buffer/vertexbuffer.cpp \
    source/InputSystem/ikeyhandler.cpp \
    source/InputSystem/imousehandler.cpp \
    source/InputSystem/inputmanager.cpp \
    source/InputSystem/keyeventregister.cpp \
    source/Objects/Component/basecomponentcontainer.cpp \
    source/Objects/Component/componentcontainer.cpp \
    source/Objects/Geometry/basegeometry.cpp \
    source/Objects/Geometry/glgeometry.cpp \
    source/Objects/Geometry/gridgeometry.cpp \
    source/Objects/Geometry/linegeometry.cpp \
    source/Objects/Geometry/triangulatedgeometry.cpp \
    source/Objects/Intersection/BVH/bvhtreecreator.cpp \
    source/Objects/Intersection/ObjectBVH/iobjectbvhtreenode.cpp \
    source/Objects/Intersection/ObjectBVH/nodebuilder.cpp \
    source/Objects/Intersection/ObjectBVH/objectbvhtree.cpp \
    source/Objects/Intersection/ObjectBVH/objectbvhtreeleafnode.cpp \
    source/Objects/Intersection/ObjectBVH/objectbvhtreenode.cpp \
    source/Objects/Intersection/Primitives/basetriangle.cpp \
    source/Objects/Intersection/Primitives/indexedtriangle.cpp \
    source/Objects/Intersection/Primitives/triangle.cpp \
    source/Objects/Intersection/RayTracer/raycastresultstore.cpp \
    source/Objects/Intersection/RayTracer/raytracingmode.cpp \
    source/Objects/Intersection/aabb.cpp \
    source/Objects/Intersection/aabbcreator.cpp \
    source/Objects/Intersection/bvhtree.cpp \
    source/Objects/Intersection/bvhtreevisualizer.cpp \
    source/Objects/Intersection/globalbvhtreeitem.cpp \
    source/Objects/Intersection/globalbvhtreeleafnode.cpp \
    source/Objects/Intersection/globalbvhtreenode.cpp \
    source/Objects/Intersection/ibvhtreevisitor.cpp \
    source/Objects/Intersection/iglobalbvhtreenode.cpp \
    source/Objects/Intersection/intersections.cpp \
    source/Objects/Intersection/plane.cpp \
    source/Objects/Intersection/ray.cpp \
    source/Objects/Intersection/raycastresult.cpp \
    source/Objects/Intersection/raytracer.cpp \
    source/Objects/Material/material.cpp \
    source/Objects/Transform/transform.cpp \
    source/Objects/Transform/transform3d.cpp \
    source/Objects/baserenderingobject.cpp \
    source/Objects/geometry.cpp \
    source/Objects/inoselfrenderingobject.cpp \
    source/Objects/iselfrenderingobject.cpp \
    source/Objects/materialobject.cpp \
    source/Objects/objectmanager.cpp \
    source/Objects/planegrid.cpp \
    source/Objects/sceneobject.cpp \
    source/Objects/transformableobject.cpp \
    source/OpenGL/glbuffer.cpp \
    source/OpenGL/openglgeometrybuffer.cpp \
    source/OpenGL/opengltexture.cpp \
    source/Render/Camera/freecamera.cpp \
    source/Render/Camera/icamera.cpp \
    source/Render/Camera/idrivencamera.cpp \
    source/Render/Camera/objectcamera.cpp \
    source/Render/RenderHelpers/baseobjectrenderhandler.cpp \
    source/Render/RenderHelpers/baserenderer.cpp \
    source/Render/RenderHelpers/extendedrenderhelper.cpp \
    source/Render/RenderHelpers/irenderhelper.cpp \
    source/Render/RenderHelpers/objectrenderhandler.cpp \
    source/Render/RenderHelpers/raycastresultrenderer.cpp \
    source/Render/RenderHelpers/simplerenderhelper.cpp \
    source/Render/RenderSystem/RenderHandlers/materialobjectrenderhandler.cpp \
    source/Render/RenderSystem/deferredrendersubsystem.cpp \
    source/Render/RenderSystem/rendersystem.cpp \
    main.cpp \
    mainwindow.cpp \
    objreader.cpp \
    source/ResourceSystem/geomertybuffercontainer.cpp \
    source/ResourceSystem/geometryresourcemanager.cpp \
    source/Scene/Environment/basebackground.cpp \
    source/Scene/Environment/skyboxbackground.cpp \
    source/Scene/Lighting/baselight.cpp \
    source/Scene/Lighting/directedlight.cpp \
    source/Scene/Lighting/lightingmanager.cpp \
    source/Scene/Lighting/pointlight.cpp \
    source/Scene/Modules/objectselectionmodule.cpp \
    source/Scene/Modules/raytracingscenemodule.cpp \
    source/Scene/Modules/scenemodule.cpp \
    source/Scene/basescene.cpp \
    source/Scene/debugscene.cpp \
    source/Scene/scene.cpp \
    source/Scene/sceneenvironment.cpp \
    source/utils/qhashextention.cpp \
    source/utils/utilits.cpp\
    tests.cpp \
    tests/geometrybuffertest.cpp \
    tests/intersectiontests.cpp \
    tests/raytracertest.cpp

HEADERS += \
    materialloader.h \
    source/Application/application.h \
    source/Application/viewport.h \
    source/Buffer/geometrybuffer.h \
    source/Buffer/gtriangle.h \
    source/Buffer/openglgeometrybuffercreator.h \
    source/Buffer/triangulatedgeometrybuffer.h \
    source/Buffer/vertexbuffer.h \
    source/InputSystem/ikeyhandler.h \
    source/InputSystem/imousehandler.h \
    source/InputSystem/inputmanager.h \
    source/InputSystem/keyeventregister.h \
    source/Objects/Component/basecomponentcontainer.h \
    source/Objects/Component/componentcontainer.h \
    source/Objects/Geometry/basegeometry.h \
    source/Objects/Geometry/glgeometry.h \
    source/Objects/Geometry/gridgeometry.h \
    source/Objects/Geometry/linegeometry.h \
    source/Objects/Geometry/triangulatedgeometry.h \
    source/Objects/Intersection/BVH/bvhtreecreator.h \
    source/Objects/Intersection/ObjectBVH/iobjectbvhtreenode.h \
    source/Objects/Intersection/ObjectBVH/nodebuilder.h \
    source/Objects/Intersection/ObjectBVH/objectbvhtree.h \
    source/Objects/Intersection/ObjectBVH/objectbvhtreeleafnode.h \
    source/Objects/Intersection/ObjectBVH/objectbvhtreenode.h \
    source/Objects/Intersection/Primitives/basetriangle.h \
    source/Objects/Intersection/Primitives/indexedtriangle.h \
    source/Objects/Intersection/Primitives/triangle.h \
    source/Objects/Intersection/RayTracer/raycastresultstore.h \
    source/Objects/Intersection/RayTracer/raytracingmode.h \
    source/Objects/Intersection/aabb.h \
    source/Objects/Intersection/aabbcreator.h \
    source/Objects/Intersection/bvhtree.h \
    source/Objects/Intersection/bvhtreevisualizer.h \
    source/Objects/Intersection/globalbvhtreeitem.h \
    source/Objects/Intersection/globalbvhtreeleafnode.h \
    source/Objects/Intersection/globalbvhtreenode.h \
    source/Objects/Intersection/ibvhtreevisitor.h \
    source/Objects/Intersection/iglobalbvhtreenode.h \
    source/Objects/Intersection/intersections.h \
    source/Objects/Intersection/plane.h \
    source/Objects/Intersection/ray.h \
    source/Objects/Intersection/raycastresult.h \
    source/Objects/Intersection/raytracer.h \
    source/Objects/Material/material.h \
    source/Objects/Transform/transform.h \
    source/Objects/Transform/transform3d.h \\
    source/Objects/baserenderingobject.h \
    source/Objects/geometry.h \
    source/Objects/inoselfrenderingobject.h \
    source/Objects/iselfrenderingobject.h \
    source/Objects/materialobject.h \
    source/Objects/objectmanager.h \
    source/Objects/planegrid.h \
    source/Objects/sceneobject.h \
    source/Objects/transformableobject.h \
    source/OpenGL/glbuffer.h \
    source/OpenGL/openglgeometrybuffer.h \
    source/OpenGL/opengltexture.h \
    source/Render/Camera/freecamera.h \
    source/Render/Camera/icamera.h \
    source/Render/Camera/idrivencamera.h \
    source/Render/Camera/objectcamera.h \
    source/Render/RenderHelpers/baseobjectrenderhandler.h \
    source/Render/RenderHelpers/baserenderer.h \
    source/Render/RenderHelpers/extendedrenderhelper.h \
    source/Render/RenderHelpers/irenderhelper.h \
    source/Render/RenderHelpers/objectrenderhandler.h \
    source/Render/RenderHelpers/raycastresultrenderer.h \
    source/Render/RenderHelpers/simplerenderhelper.h \
    source/Render/RenderSystem/RenderHandlers/materialobjectrenderhandler.h \
    source/Render/RenderSystem/deferredrendersubsystem.h \
    source/Render/RenderSystem/rendersystem.h \
    mainwindow.h \
    objreader.h \
    source/ResourceSystem/geomertybuffercontainer.h \
    source/ResourceSystem/geometryresourcemanager.h \
    source/Scene/Environment/basebackground.h \
    source/Scene/Environment/skyboxbackground.h \
    source/Scene/Lighting/baselight.h \
    source/Scene/Lighting/directedlight.h \
    source/Scene/Lighting/lightingmanager.h \
    source/Scene/Lighting/pointlight.h \
    source/Scene/Modules/objectselectionmodule.h \
    source/Scene/Modules/raytracingscenemodule.h \
    source/Scene/Modules/scenemodule.h \
    source/Scene/debugscene.h \
    source/Scene/sceneenvironment.h \
    source/utils/GL.h \
    source/utils/qhashextention.h \
    source/utils/utilits.h\
    source/typeid.h \
    source/Scene/basescene.h \
    source/Scene/scene.h \
    source/utils/utilits.h \
    tests.h \
    tests/geometrybuffertest.h \
    tests/intersectiontests.h \
    tests/raytracertest.h \
    types.h

FORMS += \
    mainwindow.ui


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    resources/shaders/222.frag \
    resources/shaders/baseLightingShader.fsh \
    resources/shaders/baseLightingShader.vsh \
    resources/shaders/lightingPass.fsh \
    resources/shaders/lightingPass.vsh \
    resources/shaders/materialObjectWriter.fsh \
    resources/shaders/materialObjectsWriter.vsh \
    resources/shaders/meshShader.fsh \
    resources/shaders/meshShader.vsh \
    resources/shaders/simplefragmentshader.fsh \
    resources/shaders/simplevertexshader.vsh \
    resources/shaders/skybox.fsh \
    resources/shaders/skybox.vsh

LIBS += -lopengl32
