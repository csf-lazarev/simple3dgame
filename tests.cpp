#include "tests.h"
#include "objreader.h"
#include "objreader.h"

#include <source/Buffer/geometrybuffer.h>
#include <source/utils/utilits.h>
Tests::Tests(QObject *parent) : QObject(parent)
{

}

void Tests::parseVertex01()
{
    bool status = true;
    QCOMPARE(ObjReader::parseVector3D(QString("1 2 3"), status), QVector3D(1, 2, 3));
    QCOMPARE(ObjReader::parseVector3D(QString("1 2 3e-3"), status), QVector3D(1, 2, 0.003f));
    QCOMPARE(ObjReader::parseVector3D(QString(".1 2 3"), status), QVector3D(0.1f, 2, 3));
    QCOMPARE(ObjReader::parseVector3D(QString("1 2 3 4"), status), QVector3D());
    QVERIFY(!status);
    QCOMPARE(ObjReader::parseVector3D(QString("1 2"), status), QVector3D());
    QVERIFY(!status);
    QCOMPARE(ObjReader::parseVector3D(QString("a 1 2"), status), QVector3D());
    QVERIFY(!status);
    QCOMPARE(ObjReader::parseVector3D(QString(""), status), QVector3D());
    QVERIFY(!status);
}

void Tests::parseFile01()
{
    ObjReader reader;
    QVERIFY(reader.parse(QString("v 1 2 3\n v 2 3 4\n vn 1 2 3\n")));
    QFile input("resources/shpereChanged.obj");
    QVERIFY(!reader.parse(input));
}

void Tests::parseFaces()
{
    QVector<int> vertices, textures, normals;

    QVector<int> expected_vertices = {1, 2, 3}, expected_textures, expected_normals;
    QVERIFY(ObjReader::parseFaceCondition("1 2 3",vertices, textures, normals));
    QCOMPARE(expected_vertices, vertices);
    QCOMPARE(expected_textures, textures);
    QCOMPARE(expected_normals, normals);

    vertices.clear();
    textures.clear();
    normals.clear();

    expected_vertices = {1, 3, 5};
    expected_textures = {2, 4, 6};
    QVERIFY(ObjReader::parseFaceCondition("1/2 3/4 5/6", vertices, textures, normals));

    QCOMPARE(expected_vertices, vertices);
    QCOMPARE(expected_textures, textures);
    QCOMPARE(expected_normals, normals);

    vertices.clear();
    textures.clear();
    normals.clear();

    expected_vertices = {1, 4, 7};
    expected_textures = {2, 5, 8};
    expected_normals =  {3, 6, 9};
    QVERIFY(ObjReader::parseFaceCondition("1/2/3 4/5/6 7/8/9", vertices, textures, normals));
    QCOMPARE(expected_vertices, vertices);
    QCOMPARE(expected_textures, textures);
    QCOMPARE(expected_normals, normals);

    vertices.clear();
    textures.clear();
    normals.clear();

    expected_vertices = {1, 3, 5};
    expected_textures = {};
    expected_normals =  {2, 4, 6};
    QVERIFY(ObjReader::parseFaceCondition("1//2 3//4 5//6", vertices, textures, normals));

    QCOMPARE(expected_vertices, vertices);
    QCOMPARE(expected_textures, textures);
    QCOMPARE(expected_normals, normals);

    vertices.clear();
    textures.clear();
    normals.clear();

    // Примеры неправильных строк для парсинга

    QVERIFY(!ObjReader::parseFaceCondition("1// /3/ 5//", vertices, textures, normals));

    QVERIFY(!ObjReader::parseFaceCondition("1/2 3/4/5 6", vertices, textures, normals));

    QVERIFY(!ObjReader::parseFaceCondition("1/2/3 3//4 5/6/7", vertices, textures, normals));

    QVERIFY(!ObjReader::parseFaceCondition("dfd/2/3 4/5/6 7/8/9", vertices, textures, normals));

    QVERIFY(!ObjReader::parseFaceCondition("1/2/3 /5/6 7/8/9", vertices, textures, normals));

    QVERIFY(!ObjReader::parseFaceCondition("1/2/3/4 1/3", vertices, textures, normals));

    QVERIFY(!ObjReader::parseFaceCondition("1/2 3/ 5/6", vertices, textures, normals));

    QVERIFY(!ObjReader::parseFaceCondition("", vertices, textures, normals));

    QVERIFY(!ObjReader::parseFaceCondition("1wqd 23r 676877x", vertices, textures, normals));
}

void Tests::trinagulateTest()
{
    QVector<int> expected;
    QVector<int> indicesStarts, sourceBuffer, result;


    expected = {1, 2, 3,    4, 5, 6,    4, 6, 7,    8, 9, 10,   8, 10, 11,
               8, 11, 12};
    indicesStarts = {0, 3, 7, 12};
    sourceBuffer = {1, 2, 3,    4, 5, 6, 7,     8, 9, 10, 11, 12};
    GeometryBuffer::trinangulateBuffer(indicesStarts, sourceBuffer, result);
    QCOMPARE(expected, result);


    expected = {1, 2, 3,    4, 5, 6,    7, 8, 9};
    indicesStarts = {0, 3, 6, 9};
    sourceBuffer = {1, 2, 3,    4, 5, 6,    7, 8, 9};
    result.clear();
    GeometryBuffer::trinangulateBuffer(indicesStarts, sourceBuffer, result);
    QCOMPARE(expected, result);
}

void Tests::matrixTest()
{
    QMatrix4x4 m(1,2,3,4,
                 5,6,7,8,
                 9,1,2,3,
                 4,5,6,7);
    float values[] = {1,2,3,
                      5,6,7,
                      9,1,2};
    QMatrix3x3 mm(values);

    qDebug() << mm;
    qDebug() << matrices::getLeftMinor(m);

    QCOMPARE(mm, matrices::getLeftMinor(m));



}
