#ifndef TESTS_H
#define TESTS_H

#include <QObject>
#include <QTest>

class Tests : public QObject
{
    Q_OBJECT
public:
    explicit Tests(QObject *parent = nullptr);
private slots:
    void parseVertex01();
    void parseFile01();
    void parseFaces();
    void trinagulateTest();
    void matrixTest();
};

#endif // TESTS_H
