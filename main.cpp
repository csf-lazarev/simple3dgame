#include "mainwindow.h"

#include <QApplication>
#include <QDebug>
#include <QOpenGLContext>
#include <QOpenGLShaderProgram>
#include <QOffscreenSurface>
#include <qopenglbuffer.h>
#include <source/utils/qhashextention.h>

#include <objreader.h>
#include "tests.h"
#include <source/Application/viewport.h>
#include <source/Objects/Geometry/glgeometry.h>
#include <source/Objects/Intersection/BVH/bvhtreecreator.h>
#include <source/Objects/Intersection/ObjectBVH/objectbvhtree.h>
#include <source/Objects/Intersection/bvhtree.h>
#include <source/Objects/Intersection/raytracer.h>
#include <source/Objects/planegrid.h>
#include <source/Objects/materialobject.h>
#include <source/Render/Camera/freecamera.h>
#include <source/Render/Camera/objectcamera.h>
#include <source/Render/RenderHelpers/simplerenderhelper.h>
#include <source/Render/RenderHelpers/extendedrenderhelper.h>
#include <source/Render/RenderSystem/RenderHandlers/materialobjectrenderhandler.h>
#include <source/OpenGL/opengltexture.h>
#include <source/ResourceSystem/geometryresourcemanager.h>
#include <source/Scene/debugscene.h>
#include <source/Scene/Environment/skyboxbackground.h>
#include <tests/intersectiontests.h>
#include <tests/raytracertest.h>
#include <source/utils/utilits.h>

#define STB_IMAGE_IMPLEMENTATION
#include <../libs/STB/stb_image.h>

void runTests()
{
    Tests tests;
    IntersectionTests intersTest;
    RayTracerTest rayTracerTest;
    QTest::qExec(&tests);
    QTest::qExec(&intersTest);
    QTest::qExec(&rayTracerTest);

}

int appStart(int argc, char *argv[]){
    QApplication::setAttribute(Qt::AA_ShareOpenGLContexts); // требуем, чтобы все виджеты в приложении шарили один глобальный контекст

    QApplication a(argc, argv);

    a.setOverrideCursor(Qt::BlankCursor);

    QOpenGLContext *globalContext = QOpenGLContext::globalShareContext(); // получаем глобальный контекст
    Q_ASSERT(globalContext);

    QOffscreenSurface *surface = new QOffscreenSurface(); // создаем поверхность. Она необходима для вызова QOpenGLContext::makeCurrent()
    surface->create();

    QOpenGLContext *glContext = new QOpenGLContext(); // создаем OpenGL контекст
    glContext->setShareContext(globalContext); // шарим его с глобальным контекстом
    const bool ok = glContext->create();
    Q_ASSERT(ok);

    glContext->makeCurrent(surface); // теперь мы можем вызывать makeCurrent у контекста и вызывать OpenGL команды


    // INIT OBJECTS, SCENES, SHADERS, ECT...

    QOpenGLShaderProgram *meshShader = new QOpenGLShaderProgram();
    meshShader->addShaderFromSourceFile(QOpenGLShader::Vertex,
                                         "../resources/shaders/meshShader.vsh");

    meshShader->addShaderFromSourceFile(QOpenGLShader::Fragment,
                                        "../resources/shaders/meshShader.fsh");
    meshShader->link();



    QSize size(1280, 960);

    IDrivenCamera * camera = new FreeCamera(vector3(0,0, -2), vector3(0,0,0), vector3(0,1,0),
                                            1.f, float(size.width())/ size.height(), 0.1f, 100.f);

    IDrivenCamera *objCamera = new ObjectCamera({-5,0,0}, {0,1,0},
                                                1.f, float(size.width())/ size.height(), 0.1f, 100.f);

    BVHTreeCreator treeCreator;
    BVHTree *tree = treeCreator.createTree(new AABB({0,0,0}, 15,15,15), 4);
    RayTracer *tracer = new RayTracer(tree, RayTracingMode(RayTracingMode::DetectMode::NEAREST, RayTracingMode::FaceMode::FRONT_AND_BACK));

    KeyEventRegister keyRegister;
    GeometryResourceManager gMgr;

    QOpenGLShaderProgram skyboxShader;
    skyboxShader.addShaderFromSourceFile(QOpenGLShader::Vertex,
                                         "../resources/shaders/skybox.vsh");

    skyboxShader.addShaderFromSourceFile(QOpenGLShader::Fragment,
                                         "../resources/shaders/skybox.fsh");
    skyboxShader.link();

    int skyWidth, skyHeight, channels;
    uchar *sTop = stbi_load("../resources/textures/mp_drakeq/top.tga", &skyWidth, &skyHeight, &channels, 0);
    uchar *sBottom = stbi_load("../resources/textures/mp_drakeq/bottom.tga", &skyWidth, &skyHeight, &channels, 0);
    uchar *sLeft = stbi_load("../resources/textures/mp_drakeq/left.tga", &skyWidth, &skyHeight, &channels, 0);
    uchar *sRight = stbi_load("../resources/textures/mp_drakeq/right.tga", &skyWidth, &skyHeight, &channels, 0);
    uchar *sFront = stbi_load("../resources/textures/mp_drakeq/front.tga", &skyWidth, &skyHeight, &channels, 0);
    uchar *sBack = stbi_load("../resources/textures/mp_drakeq/back.tga", &skyWidth, &skyHeight, &channels, 0);



    qDebug() << skyWidth << skyHeight << channels;

    OpenGLTexture skyboxTexture(OpenGLTexture::Target::TextureCubeMap);

    skyboxTexture.create();
    skyboxTexture.bind();

    skyboxTexture.loadImage2DDataCubeFace(OpenGLTexture::CubeFace::POS_X, 0,
                                          OpenGLTexture::InternalFormat::RGB, skyWidth, skyHeight,
                                          OpenGLTexture::PixelType::RGB, OpenGLTexture::DataType::UNSIGNED_BYTE, sRight);

    skyboxTexture.loadImage2DDataCubeFace(OpenGLTexture::CubeFace::NEG_X, 0,
                                          OpenGLTexture::InternalFormat::RGB, skyWidth, skyHeight,
                                          OpenGLTexture::PixelType::RGB, OpenGLTexture::DataType::UNSIGNED_BYTE, sLeft);

    skyboxTexture.loadImage2DDataCubeFace(OpenGLTexture::CubeFace::POS_Y, 0,
                                          OpenGLTexture::InternalFormat::RGB, skyWidth, skyHeight,
                                          OpenGLTexture::PixelType::RGB, OpenGLTexture::DataType::UNSIGNED_BYTE, sTop);

    skyboxTexture.loadImage2DDataCubeFace(OpenGLTexture::CubeFace::NEG_Y, 0,
                                          OpenGLTexture::InternalFormat::RGB, skyWidth, skyHeight,
                                          OpenGLTexture::PixelType::RGB, OpenGLTexture::DataType::UNSIGNED_BYTE, sBottom);

    skyboxTexture.loadImage2DDataCubeFace(OpenGLTexture::CubeFace::POS_Z, 0,
                                          OpenGLTexture::InternalFormat::RGB, skyWidth, skyHeight,
                                          OpenGLTexture::PixelType::RGB, OpenGLTexture::DataType::UNSIGNED_BYTE, sFront);

    skyboxTexture.loadImage2DDataCubeFace(OpenGLTexture::CubeFace::NEG_Z, 0,
                                          OpenGLTexture::InternalFormat::RGB, skyWidth, skyHeight,
                                          OpenGLTexture::PixelType::RGB, OpenGLTexture::DataType::UNSIGNED_BYTE, sBack);


    stbi_image_free(sTop);
    stbi_image_free(sBottom);
    stbi_image_free(sLeft);
    stbi_image_free(sRight);
    stbi_image_free(sFront);
    stbi_image_free(sBack);

    skyboxTexture.setMinificationFilter(OpenGLTexture::FilterType::Linear);
    skyboxTexture.setMagnificationFilter(OpenGLTexture::FilterType::Linear);

    skyboxTexture.setWrapModeR(OpenGLTexture::WrapMode::ClampToEdge);
    skyboxTexture.setWrapModeS(OpenGLTexture::WrapMode::ClampToEdge);
    skyboxTexture.setWrapModeT(OpenGLTexture::WrapMode::ClampToEdge);

    qDebug() << glGetError();



    float skyboxVertices[] = {
        // positions
        -1.0f,  1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
         1.0f, -1.0f, -1.0f,
         1.0f, -1.0f, -1.0f,
         1.0f,  1.0f, -1.0f,
        -1.0f,  1.0f, -1.0f,

        -1.0f, -1.0f,  1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f,  1.0f, -1.0f,
        -1.0f,  1.0f, -1.0f,
        -1.0f,  1.0f,  1.0f,
        -1.0f, -1.0f,  1.0f,

         1.0f, -1.0f, -1.0f,
         1.0f, -1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
         1.0f,  1.0f, -1.0f,
         1.0f, -1.0f, -1.0f,

        -1.0f, -1.0f,  1.0f,
        -1.0f,  1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
         1.0f, -1.0f,  1.0f,
        -1.0f, -1.0f,  1.0f,

        -1.0f,  1.0f, -1.0f,
         1.0f,  1.0f, -1.0f,
         1.0f,  1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
        -1.0f,  1.0f,  1.0f,
        -1.0f,  1.0f, -1.0f,

        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f,  1.0f,
         1.0f, -1.0f, -1.0f,
         1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f,  1.0f,
         1.0f, -1.0f,  1.0f
    };

    QOpenGLBuffer skyboxBuffer(QOpenGLBuffer::VertexBuffer);
    skyboxBuffer.create();
    skyboxBuffer.bind();
    skyboxBuffer.allocate(skyboxVertices, sizeof(skyboxVertices));
    skyboxBuffer.release();


    Scene *scene = new Scene(camera, tracer, &keyRegister);

    SceneEnvironment *sceneEnvironment = new SceneEnvironment(scene);

    ExtendedRenderHelper rHelp(*sceneEnvironment, gMgr);

    SkyboxBackground *background = new SkyboxBackground(skyboxTexture, &skyboxBuffer, &skyboxShader,
                                                        rHelp);

    RaycastResultRenderer rcRenderer(sceneEnvironment);
    RayTracingSceneModule rayModule(tracer, &rcRenderer);
    DebugScene dScene(&rayModule);
    scene->setDScene(&dScene);
    scene->setBackround(background);

    ObjReader reader;

    GeometryBuffer *modelGeometryBuffer = reader.parseFromFile("../resources/objects/manMesh.obj");




    TriangulatedGeometryBuffer *modelTriangulatedBuffer = modelGeometryBuffer->toTriangulatedGeometryBuffer();

    TGContainer *modelBufferContainer = gMgr.registerBuffer(modelTriangulatedBuffer, "sphere");

    Material *emerandMaterial = new Material({0.07568f,0.61424f,0.07568f}, 0.6f);
    Material *pearlMaterial = new Material({0.829f,0.829f,0.829f},0.088f);
    Material *silverMaterial = new Material({0.50754f,0.50754f,0.50754f}, 0.4f);
    Material *obsidialMaterial = new Material({0.18275f,0.17f,0.22525f}, 0.3f);
    Material *chromeMaterial = new Material({0.35f, 0.35f, 0.35f}, 1.0f);


    SimpleRenderHelper *gridRenderHelper = new SimpleRenderHelper(sceneEnvironment);

    //MeshObject *modelGrid1 = new MeshObject(gridRenderHelper, modelMeshGeometry, new Transform3D({0,0,8}, {1,1,1}));

    SceneObject* obj1  = new MaterialObject(modelBufferContainer,
                                         new Transform3D({4,0,0}, {0.1,0.1,0.1}, QVector3D(0, 1, 0), 0),
                                            emerandMaterial);
    SceneObject* obj2  = new MaterialObject(modelBufferContainer,
                                           new Transform3D({-4,0,0}, {0.1,0.1,0.1}, QVector3D(0, 1, 0), 1),
                                            pearlMaterial);
    SceneObject* obj3  = new MaterialObject(modelBufferContainer,
                                           new Transform3D({0,0,4}, {0.1,0.1,0.1}, QVector3D(0, 1, 0), 1),
                                            silverMaterial);
    SceneObject* obj4  = new MaterialObject(modelBufferContainer,
                                          new Transform3D({0,0,-4}, {0.1,0.1,0.1}, QVector3D(0, 1, 0), 1),
                                            chromeMaterial);
    SceneObject* obj5  = new MaterialObject(modelBufferContainer,
                                           new Transform3D({0,0,0}, {0.1,0.1,0.1}, QVector3D(0, 1, 0), 1),
                                            obsidialMaterial);

    PlaneGrid *grid = new PlaneGrid(gridRenderHelper, {0, 0, 0},
                                    20.0f, 20.0f, 50, 50);

    ObjectManager *objManager = scene->getObjectManager();

    objManager->registerObject(obj1);
    objManager->registerObject(obj2);
    objManager->registerObject(obj3);
    objManager->registerObject(obj4);
    objManager->registerObject(obj5);
    objManager->registerObject(grid);

    PointLight *light1 = new PointLight({0.2f, 0.2f, 0.6f}, {0,0,0}, 1, 0.07f, 0.017f);
    PointLight *light2 = new PointLight({0.3f, 0.3f, 0.7f}, {-7,2,0}, 1, 0.07f, 0.017f);
    PointLight *light3 = new PointLight({0.0f, 0.0f, 0.3f}, {8,0,0}, 1, 0.07f, 0.017f);
    DirectedLight *dirLight = new DirectedLight({1,1,1}, QVector3D(0, -4,-1).normalized(), 0.85f);

    LightingManager *lmgr = scene->getLightingManager();
    lmgr->registerLight(light1);
    lmgr->registerLight(light2);
    lmgr->registerLight(light3);
    lmgr->setDirLight(dirLight);


    QOpenGLShaderProgram *objectShader = new QOpenGLShaderProgram();
    objectShader->addShaderFromSourceFile(QOpenGLShader::Vertex,
                                          "../resources/shaders/baseLightingShader.vsh");

    objectShader->addShaderFromSourceFile(QOpenGLShader::Fragment,
                                         "../resources/shaders/baseLightingShader.fsh");
    objectShader->link();


    // init deferred rendering susbsystem


    QOpenGLFramebufferObjectFormat frameBufferFormat;
    frameBufferFormat.setTextureTarget(GL_TEXTURE_2D);
    frameBufferFormat.setInternalTextureFormat(GL_RGBA16F);

    QOpenGLShaderProgram *geomPass = new QOpenGLShaderProgram();
    geomPass->addShaderFromSourceFile(QOpenGLShader::Vertex,
                                      "../resources/shaders/materialObjectsWriter.vsh");
    geomPass->addShaderFromSourceFile(QOpenGLShader::Fragment,
                                      "../resources/shaders/materialObjectWriter.fsh");

    QOpenGLShaderProgram *lightPass = new QOpenGLShaderProgram();
    lightPass->addShaderFromSourceFile(QOpenGLShader::Vertex,
                                       "../resources/shaders/lightingPass.vsh");
    lightPass->addShaderFromSourceFile(QOpenGLShader::Fragment,
                                       "../resources/shaders/lightingPass.fsh");

    DeferredRenderSubSystem *defRenderSubSystem = new DeferredRenderSubSystem(size, frameBufferFormat,
                                                                              geomPass, lightPass, &gMgr);

    MaterialObjectRenderHandler *materialHandler =
            new MaterialObjectRenderHandler(geomPass, &gMgr);

    defRenderSubSystem->registerObjectRenderHandler<MaterialObject>(materialHandler);

    RenderSystem *renderSystem = new RenderSystem(defRenderSubSystem);

    TriangulatedGeometryBuffer *tBuffer = modelGeometryBuffer->toTriangulatedGeometryBuffer();

    ObjectBVHTree *model1Tree = new ObjectBVHTree(tBuffer, 20);
    model1Tree->compute();
    tree->addItem(new GlobalBVHTreeItem(obj1->getTransform(), model1Tree));
    tree->addItem(new GlobalBVHTreeItem(obj2->getTransform(), model1Tree));
    tree->addItem(new GlobalBVHTreeItem(obj3->getTransform(), model1Tree));
    tree->addItem(new GlobalBVHTreeItem(obj4->getTransform(), model1Tree));
    tree->addItem(new GlobalBVHTreeItem(obj5->getTransform(), model1Tree));


    Viewport viewport(scene, renderSystem, size);
    viewport.show();
    int code = a.exec();

    //skyBox.destroy();
    return code;
}

int main(int argc, char *argv[]){


    //runTests();
    return appStart(argc, argv);


}
